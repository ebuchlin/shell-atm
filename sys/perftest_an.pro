; Analysis of result of perftest.pl

file = "perftest_bere.txt"
n = 45

openr, u, file, /get_lun, err=err
if err ne 0 then message, "Couldn't open file"
readarray, file, n * 3, d
d = reform (d, 3, n)
nz = round (d[0, *])
np = round (d[1, *])
pe = d[2, *]
nzs = nz[uniq (nz)]
nps = np[uniq (np)]

nnzs = n_elements(nzs)


psyms = [1, 2, 4, 5, 6, 7]
plot, [min (nps, max=tmp), tmp], [min (pe, max=tmp), tmp], /nodata, $
  /xlog, /ylog, xstyle=3, ystyle=3, xtitle="np", ytitle="s/nt/nz*np"
for inz=0, nnzs - 1 do begin
    w = where (nz eq nzs[inz], count)
    if count eq 0 then continue
    oplot, np[w], pe[w], psym=psyms[inz]
    oplot, np[w], pe[w], linestyle=1

endfor

noticks = replicate (' ', 30)
plot, [0, 1], [0, 1], /nodata, /noerase, position=[.8, .8, .972, .96], $
  xtickname=noticks, ytickname=noticks, xticklen=1e-10, yticklen=1e-10
for inz=0, nnzs - 1 do begin
    y = (inz + 1.) / (nnzs + 1.)
    oplot, [.1, .5], replicate (y, 2), psym=psyms[inz]
    oplot, [.1, .5], replicate (y, 2), linestyle=1
    xyouts, .6, y, string_ns (nzs[inz])
endfor

end

