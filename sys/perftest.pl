#!/usr/bin/perl

# creates, runs and analyzes set of runs to test performance of parallel runs

#my @nps = (1, 2, 3, 4, 6, 8, 12, 16, 24);
#my @nzs = (100, 300, 1000, 3000, 10000);
my @nps = (4, 6);
my @nzs = (3000, 10000);

my $nt = 1000;

my $prepare = 1;
my $run     = 1;
my $analyze = 1;

if ($analyze) {
    open (RESULT, "> perftest.txt") || die $!;
}

foreach $nz (@nzs) {
    foreach $np (@nps) {
	print "nz: $nz   np: $np\n";

	if ($prepare) { 
	    print "  Preparing run... ";
	    my $dz = 1. / $nz;
	    $subdir = "perftest-$nz-$np";
	    mkdir $subdir;
	    system "cp", "shell-atm", $subdir;
	    chdir $subdir;
	    open (PARAMS, "> params.txt") || die $!;
	    $nt = int (300000 / $nz) + 1;
	    print PARAMS << "EOF";
# nmin
0
# nmax
15
# nz
$nz
# k0
62.831853
# lambda
2
# nu
1.e-9
# eta
1.e-9
# eps
1.25
# epsm
-.3333333333333333333
# b0 Alfven speed
1.
# dz distance between planes
$dz
# nt
$nt
# initial delta_t
0.00001
# continue from saved state
0
# initial amplitude
1e-3
# initial slope
-.3333333333333
# forcing amplitude
1e-3
# tstar correlation time of forcing
200.31234
# rho0 density at base
1.
# h scale height
1.
EOF
        close (PARAMS);
	open (PARAMS, "> param_o.txt") || die $!;
	print PARAMS << "EOF";
# t1 : period for main output
10
# t2 : t1 * t2 is the period for spectrum output
1000
# Number of planes to output in out_simstate_nnn (all if 0)
0
# Number of structure functions
10
# Time
1
# Non-linear time scale
1
# Total energy in modes 02
1
# Total energy in modes 08
1
# Total energy in modes 14
1
# Total energy in modes 20
0
# Total kinetic energy
1
# Total magnetic energy
1
# Total kinetic energy dissipation
1
# Total magnetic energy dissipation
1
# Power of the forcing at boundaries
1
# Cross helicity
1
# "Magnetic helicity"
0
# Kinetic energy spectrum (time average, longitudinal sum)
1
# Magnetic energy spectrum (time average, longitudinal sum)
1
# Velocity field structure function
0
# Magnetic field structure function
0
# Raw fields (output of out_simstate_nnn.dat)
1
# Raw zm field (not used)
1
# Profile of Alfv&n velocity
1
# Profile of Alfv&n velocity gradient
1
# Profile of mass density
1
# Position as a function of z index
1
# Profile of over-expansion factor
1
# Total magnetic energy of each plane (not used)
0
EOF
            close (PARAMS);
	    open (PARAMS, "> mpi_processors.txt") || die $!;
	    chomp(my $cwd = `pwd`);
	    for ($i=0; $i < $np; $i++) {
		my $node = sprintf "%02d", ($i >> 1) + 1;
		my $newprocess = ($i == 0 ? 0 : 1);
		print PARAMS "linux$node $newprocess $cwd/shell-atm\n";
	    }
	    print "done\n";
	}

	if ($run) {
	    print "  Running...\n";
	    # need to use a shell because of |tee
	    system "mpirun -p4pg mpi_processors.txt ./shell-atm " . 
		" | tee output.log";
	    print "  done\n";
	}

	if ($analyze) {
	    print "  Analyzing... ";
	    
	    open (OUTPUT, "output.log") || die $!;
	    my $eff = undef;
	    while (<OUTPUT>) {
		$eff = $1 if /\(\s*([\d\.E\-]+)\s*s\/nt\/nz\*np \)/;
	    }
	    close (OUTPUT);
	    
	    print " ${eff} ";
	    print RESULT "$nz\n$np\n$eff\n";
	    print "done\n";
	}
	

	chdir "..";
    }
}

close (RESULT) if ($analyze);

