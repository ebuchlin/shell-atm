#!/usr/bin/perl

# Put table of parameters of shell-loop simulations in an HTML table

use strict;

print << "EOF";
<html>
<head>
  <title>List of shell-atm model parameters</title>
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <meta name="Generator" content="showparams Perl script">
</head>
<body>
<h1>Shell-atm model parameters</h1>

<p>[<a href="index.html">Back to index</a>]</p>

<table>
  <tr>
    <th>run</th>
    <th>nmin</th>
    <th>nmax</th>
    <th>nz</th>
    <th>k0</th>
    <th>lambda</th>
    <th>nu</th>
    <th>eta</th>
    <th>eps</th>
    <th>epsm</th>
    <th>b0</th>
    <th>dz</th>
    <th>nt</th>
    <th>cont</th>
    <th>initamp</th>
    <th>initslope</th>
    <th>forcamp</th>
    <th>tstar</th>
    <th>rho0</th>
    <th>h</th>
 <tr>
EOF

my $thisdir = shift @ARGV;
$thisdir = './' unless defined ($thisdir);
opendir (DH, $thisdir) || die "Can't opendir $thisdir: $!"; 
my @dirs = readdir DH;
my @params, my @oldparams;
foreach $_ (@dirs) {
    my $paramsfile = "$thisdir/$_/params.txt";
    if (-d "$thisdir/$_" && -f $paramsfile) {
	open PF, $paramsfile || die "Can't open $paramsfile: $!";
	@params = <PF>;
	close PF;
	print " <tr>\n";
	print "    <td><a href=\"$_/\">$_</a></td>\n";
	for (my $i=1; $i <= $#params; $i += 2) {
	    next if ($i == 25);
	    my $bold = ($params[$i] != $oldparams[$i]);
	    print "    <td>", ($bold?"<b>":""),
	      $params[$i], ($bold?"</b>":""), "</td>\n";
	}
	print "</tr>\n";
	@oldparams = @params
    }
}

closedir DH;

print << "EOF"

</table>
</body>
</html>

EOF
