#!/bin/bash

# To be called in main code directory by Makefile

DIR="$DIR"
if [[ -z "$DIR" ]]; then
    {
	echo "Variable DIR should be set to the subdirectory of runs" \
              "where you want the program to be installed, e.g." \
              "make install DIR=foobar"
	exit 10
    }
fi

mkdir runs/$DIR
cp src/Makefile src/*.txt src/*.f90 src/*.c runs/$DIR

# PLATFORM is determined automatically but can be overriden on the command line
PLATFORM="$PLATFORM"
if [[ -z "$PLATFORM" ]]; then
    {
	SYSNAME=`uname -s`
	HOSTNAME=`hostname -s` || HOSTNAME=`hostname`
	test $HOSTNAME = localhost && HOSTNAME=`hostname`
	PLATFORM=$SYSNAME/$HOSTNAME
    }
fi
echo "PLATFORM: $PLATFORM"
if [[ ! -d "config/platform/$PLATFORM" ]]; then
    echo "PLATFORM not found. Please check name, or" \
	 "choose another platform by hand, or create the configuration" \
	 "directory config/platform/PLATFORM for your platform."
    exit 11
fi
cp config/platform/"$PLATFORM"/Makefile runs/$DIR/Makefile.platform

# MODEL is either default or given on command line
MODEL="$MODEL"
[ -z "$MODEL" ] && MODEL="default"
echo "MODEL: $MODEL"
if [[ ! -d "config/model/$MODEL" ]]; then
    echo "MODEL not found. Please check name, or" \
	 "choose another model, or create the configuration" \
	 "directory config/model/MODEL for the model you want to run."
    exit 12
fi

cp config/model/"$MODEL"/Makefile runs/$DIR/Makefile.model
[ -f config/model/"MODEL"/params.txt ] && \
    cp config/model/"MODEL"/params.txt runs/$DIR/
[ -f config/model/"MODEL"/param_o.txt ] && \
    cp config/model/"MODEL"/param_o.txt runs/$DIR/
[ -f config/model/"MODEL"/paramexp.txt ] && \
    cp config/model/"MODEL"/paramexp.txt runs/$DIR/

echo "Files copied to runs/$DIR"
