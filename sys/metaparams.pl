#!/usr/bin/perl

# Transforms meta-parameters in the file mparams.txt to parameters in
# the file params.txt (the old one will be deleted)


# Read mparams.txt
open MP, "mparams.txt" || die "Couldn't open mparams.txt: $!";

$junk = <MP>; chomp ($contin = <MP>);
$junk = <MP>; chomp ($aspect = <MP>);
$junk = <MP>; chomp ($nperp  = <MP>);
$junk = <MP>; chomp ($npar   = <MP>);
$junk = <MP>; chomp ($nt     = <MP>);
$junk = <MP>; chomp ($uk0    = <MP>);
$junk = <MP>; chomp ($forca  = <MP>);

close MP;

$nmin = 0;
$nmax = $nmin + $nperp - 1;
$k0 = 6.2831853 * $aspect;
$lambda = 2;
$nz = $uk0 * $lambda ** (($nperp - 1) * 2. / 3.);
$dz = 1. / $npar;
$nu = $lambda ** (-2. * ($nperp - 1)) * $npar / $k0 ** 2 * 10.;
$initdt = $dz / 100.;

if ( abs (log ($nz) - log ($npar)) > log (3)) {
    print STDERR "Warning: nz is chosen to be $npar but ideal value" .
	" is closer to $nz\n";
}

open PA, " > params.txt" || die "Couldn't open params.txt: $!";

print PA << "EOF";
# nmin
$nmin
# nmax
$nmax
# nz
$npar
# k0
$k0
# lambda
$lambda
# nu
$nu
# eta
$nu
# eps
1.25
# epsm
-.3333333333333333333
# b0 Alfven speed
1.
# dz distance between planes
$dz
# nt
$nt
# initial delta_t
$initdt
# continue from saved state
$contin
# initial amplitude
1e-7
# initial slope
-.3333333333333
# forcing amplitude
$forca
EOF
close PA;

