.PHONY: all shell-atm clean cleanout outclean doc install

all:
	@echo "Please select Makefile action"

shell-atm: 
	make -C src shell-atm

clean:
	make -C src clean
	make -C doc clean
	make -C sys clean

doc:
	make -C doc all

install:
	@sys/installscript

export: doc exportclean
	mkdir shell-atm
	cd shell-atm && mkdir runs src && cd ..
	cp -R Makefile doc/ sys/ config/ shell-atm/
	cp -RL idl/ shell-atm/
	cp src/Makefile* src/*param*.txt src/mpi_* src/*.f90 \
		src/unlimitstack.c shell-atm/src/
	rm -rf shell-atm/*/.svn shell-atm/*/*/.svn shell-atm/*/*/*/.svn
	tar zcvf shell-atm.tgz shell-atm/

exportclean: 
	rm -rf shell-atm.tgz shell-atm/
