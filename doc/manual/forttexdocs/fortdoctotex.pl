#!/usr/bin/perl

# fortdoctotex file.f90 [file2.f90 ...]
# Auto-documentation of Fortran 90 source code
#   !+ at the beginning of a line: begin of documentation
#   !- at the end of a line: end of documetiontation
#   In documentation: rows of at least 16 *'s indicate the presence of
#   section's title
# Output: latex on stdout, to be input in a LaTeX document
# Eric Buchlin, Oct 2005
# License: GPL

$isdoc = 0;
$istitle = 0;
$i = 0;


print << "EOF";
\\begin{alltt}
EOF

while (<ARGV>) {
    $i++;
    chomp;
    if (/^!\+/) {
	if ($isdoc == 0) {
	    $isdoc = 1;
	} else {
	    print STDERR "Warning (line $i): two consecutive !+'s ?\n";
	}
    } elsif (/^!-/) {
	if ($isdoc == 1) {
	    $isdoc = 0;
	    print "\n";
	} else {
	    print STDERR "Warning (line $i): two consecutive !-'s ?\n";
	}
    } elsif ($isdoc == 1) {
	chomp;
	if (/^\s*!/) {
	    # comment in code
	    $style = 0;
	} else {
	    # code
	    $style = 1;
	    $isroutine = (/(subroutine|function|module|program)/i) ? 1: 0;
	}

	# suppress leading !'s indicating comments
	s/^[!\s]*//g;

	if (!/^\s*$/) { # if not a blank line
	    if (/\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*/) {
                # at least 16 stars: beginning or end of title
		if ($istitle == 0) { 
		    print "\\textbf{";
		    $istitle = 1;
		} else {
		    print "}\n";
		    $istitle = 0;
		}
	    } else {
		if ($style) {
		    # code
		    print "\\textbf{" if ($isroutine);
		    if ($style && /use (.*)/) {
			# for a module (except mpi) do hyperlink
			$_ = $1;
			if (/^mpi/) { print "use $_";}
			else {print "use \\hyperlink{module:$_}{$_}";}
		    } else {
			print;
		    }
		    print "}" if ($isroutine);;
		} else {
		    # comment in code
		    print;
		}
		print "\n" if (!$istitle);
	    }
	} else {
	    # blank line
#	    print "<br>\n"
	}
    }
}

print << "EOF";
\\end{alltt}
EOF
