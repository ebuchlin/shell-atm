; to cut and paste into an idl session
; use do_ps = 1, and then use Makefile to produce files to input in
; manual.tex
; current example: berenice:~eric/shella/runs/005shellloop
if n_elements(do_ps) eq 0 then do_ps = 0
if do_ps then set_plot, 'PS'
if do_ps then device, filename='idltutorial.ps', $
  xoffset=1, yoffset=1, encapsulated=0, xsize=15, ysize=12
p = sa_params_get ()
help, p, /struct
sa_plot_dt, /tau, minmax=1000
sa_plot_energy, minmax=1000
sa_plot_energy, /tot, /xlog, /ylog, xrange=[10, 100], minmax=500
sa_plot_de, /tot, minmax=1000
sa_plot_de, xrange=[150,151]
sa_plot_cutk, 5, /uu, /noi
sa_plot_cutt, 150, /zp
sa_plot_cutz, 400, /zm, time=100+indgen(50), /log,range=[-10,-5]
sa_plot_enflux, time=2*indgen(100)
sa_plot_heatfunc
sa_plot_spectrum2d, /zp
sa_plot_spectrumpp, /zm
if do_ps then device, /close
if do_ps then set_plot, 'X'

