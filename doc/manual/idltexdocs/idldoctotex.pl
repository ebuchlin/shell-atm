#!/usr/bin/perl

# idldoctotex file.pro [file2.pro ...]
# Auto-documentation of IDL source code
#   !+ at the beginning of a line: begin of documentation
#   !- at the end of a line: end of documetiontation
# Output: latex on stdout, to be included in a latex document
# Eric Buchlin, Oct 2005
# License: GPL

$isdoc = 0;
$i = 0;


print << "EOF";
\\begin{alltt}
EOF
while (<ARGV>) {
    $i++;
    if (/^\;\+/) {
	if ($isdoc == 0) {
	    $isdoc = 1;
	} else {
	    print STDERR "Warning (line $i): two consecutive !+'s ?\n";
	}
    } elsif (/^\;-/) {
	if ($isdoc == 1) {
	    $isdoc = 0;
	} else {
	    print STDERR "Warning (line $i): two consecutive !-'s ?\n";
	}
    } elsif ($isdoc == 1) {
	chomp;
	s/^[\s\;]*//g;
	print;
	print "\n";
    }
}

print << "EOF";
\\end{alltt}
EOF
