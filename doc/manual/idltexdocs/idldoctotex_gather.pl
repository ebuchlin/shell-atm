#!/usr/bin/perl

# idldoctotex_gather file.tex [file2.tex ...]
# Gathers files produced by idldoctotex.pl,
# putting each file in a subsubsection
# Output: latex on stdout, to be included in a latex document
# Eric Buchlin, Oct 2005
# License: GPL

while ($_ = shift @ARGV) {
    chomp;
    s/\.tex$//;
    print "\\subsubsection\{$_\}\n";
    s/_/\\string_/g;
    print "\\input\{idltexdocs/$_\}\n\n";
}

