# Shell-atm

## Introduction

Shell-atm is a numerical simulation code for reduced MHD turbulence, including wave propagation and heating, in closed or open magnetic field lines (typically coronal loops and coronal holes respectively).

## Documentation

A PDF manual can be generated from the scripts in `doc/manual` (a compiled version can also be found there).

Low-level code HTML documentation can be generated using the scripts in `doc/forthmldoc`.

## References

Shell-Atm has been originally developed for [Buchlin and Velli (2007)](https://doi.org/10.1086/512765).
It has also been enhanced for and used in [other papers](https://ui.adsabs.harvard.edu/#/public-libraries/QDvKkNCLSuuetMkRPffAZQ).
