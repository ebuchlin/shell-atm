;interpolate fields to get a higher resolution

if n_elements (factor) eq 0 then factor = 2L
p = sa_params_get ('simstate.dat_00', shells=z, time=time, forc=forc)
nk = p.nmax - p.nmin + 1L
newz = dcomplexarr (nk, p.nz * factor, 2)
for ip=0, 1 do begin
    for ik=p.nmin, p.nmax do begin
        ; interpol doesn't take into account the imaginary part!
        newz[ik, *, ip] = dcomplex ($
           interpol (real_part (z[ik, *, ip]), p.nz * factor, /spline), $
           interpol (imaginary (z[ik, *, ip]), p.nz * factor, /spline))
    endfor
endfor
p.nz = p.nz * factor
p.dz = p.dz / factor
sa_params_write, p, 'simstate.dat_0', shells=newz, time=time, forc=forc
end

