!+
!***********************************************************
! Module fexpand-none
!***********************************************************
! Routines for no expansion factor
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  19 Aug 05: AV Created
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module fexpand
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_fexpand = "none"

contains

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Expansion factor as a function of r
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function fexpansion(r, pexp)
!-
    implicit none
    type(paramexp), intent(in)      :: pexp
    real(kind=dfloat), intent(in)   :: r
    real(kind=dfloat)               :: fexpansion

    fexpansion = 1._dfloat 

  end function fexpansion



!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Logarithmic derivative of the expansion factor as a function of r
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function fdlexpansion(r, pexp)
!-
    implicit none
    type(paramexp), intent(in)      :: pexp
    real(kind=dfloat), intent(in)   :: r
    real(kind=dfloat)               :: fdlexpansion


    fdlexpansion = 0._dfloat 

  end function fdlexpansion

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Print parameters specific to expansion 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine expansion_print_parameters
!-
    implicit none

    character (len=*), parameter :: form1 = "(A, I12,    A, I12   )"
    character (len=*), parameter :: form2 = "(A, ES12.5, A, ES12.5)"
    character (len=*), parameter :: form3 = "(A, I12   , A, ES12.5)"
    character (len=*), parameter :: form4 = "(A, I12   )"
    character (len=*), parameter :: form5 = "(A, ES12.5)"

    print*, '  Flux tube expansion: no over-expansion'

  end subroutine expansion_print_parameters


end module fexpand

