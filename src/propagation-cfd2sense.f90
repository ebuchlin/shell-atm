!+
!***********************************************************
! Module propagation-cfd   
!***********************************************************
! Variables and routines for Alfv�n wave propagation
! With Compact-Finite-Difference numerical scheme (inside the domaine)
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  28 Nov 05 AV Created 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module propagation
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_propagation = "cfd"

contains

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Alfv�n wave propagation, Fromm numerical scheme inside the box,
! Compact Finite Difference scheme inside the box o(h^6)
! Lower at the boundaries o(h^4)
! zp on first plane and zm on last plane are _not_ computed (this is the
! job of the force routine)
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine propagate (zp, zm, k, p, t)
    use mpi
    use atmosphere
!-
    implicit none
    type(params),         intent(inout)                         :: p
    complex(kind=dfloat), intent(inout), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                     :: zp, zm
    ! not used
    complex(kind=dfloat), intent(in),  dimension(p%nmin:p%nmax) :: k
    ! not used
    real(kind=dfloat) :: t

    ! are the local fields already allocated?
    ! Is it the first time this procedure is called?
    logical, save :: isallocated = .FALSE., firsttime = .TRUE.
    integer :: iz

    ! temporary arrays for numerical scheme
    complex(kind=dfloat), dimension(:,:), allocatable, save :: vp, vm, &
         gp, gm, tp, tm


    if (.NOT. isallocated) then
       allocate (vp(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (gp(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (tp(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (vm(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (gm(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (tm(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       isallocated = .TRUE.
    end if

    ! normalization by unorm
    do iz = 0, p%nz-1
       zp(:,iz) = zp(:,iz) / unorm(iz)
       zm(:,iz) = zm(:,iz) / unorm(iz)
    end do

    if (firsttime .and. p%mpi%me .eq. p%mpi%root)  then
       print*, 'Using cfd numerical scheme for propagation'
       print*, '  (lower-order schemes at boundaries)'
    end if

!
    ! update zp for propagation
    vp = zp
    vm = zm
    call subprop_f(vp, vm, gp, k, p, t, +1)
    call subprop_b(vm, vp, gm, k, p, t, -1)
    vp = vp + (p%delta_t / 3._dfloat) * gp
    vm = vm + (p%delta_t / 3._dfloat) * gm
    call subprop_f(vp, vm, tp, k, p, t, +1)
    call subprop_b(vm, vp, tm, k, p, t, -1)
    gp = tp - (5._dfloat / 9._dfloat) * gp
    vp = vp + (15._dfloat / 16._dfloat * p%delta_t) * gp
    gm = tm - (5._dfloat / 9._dfloat) * gm
    vm = vm + (15._dfloat / 16._dfloat * p%delta_t) * gm
    call subprop_f(vp, vm, tp, k, p, t, +1)
    call subprop_b(vm, vp, tm, k, p, t, -1)
    gp = tp - (153._dfloat / 128._dfloat) * gp
    zp = vp + (8._dfloat / 15._dfloat * p%delta_t) * gp
    gm = tm - (153._dfloat / 128._dfloat) * gm
    zm = vm + (8._dfloat / 15._dfloat * p%delta_t) * gm
    ! want to filter the solution?
    call filter(zp, p)
    call filter(zm, p)


    ! de-normalization
    do iz = 0, p%nz-1
       zp(:,iz) = zp(:,iz) * unorm(iz)
       zm(:,iz) = zm(:,iz) * unorm(iz)
    end do

    firsttime = .FALSE.

  end subroutine propagate



  subroutine subprop_f (z1d, z2d, dz1d, k, p, t, sig)
    use mpi
    use atmosphere
!-
    implicit none
    type(params),         intent(in)                         :: p
    complex(kind=dfloat), intent(in), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                 :: z1d, z2d
    ! sign to compute time derivative: +1 for zp, -1 for zm
    integer, intent(in)                                     :: sig          
    complex(kind=dfloat), intent(out), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                 :: dz1d
    ! not used
    complex(kind=dfloat), intent(in),  dimension(p%nmin:p%nmax) :: k
    ! not used
    real(kind=dfloat) :: t

    ! temporary variable for communications
    complex(kind=dfloat), &
         dimension(p%nmin:p%nmax, -2:p%nz+1)                 :: oz1d
    real(kind=dfloat), dimension(p%nmin:p%nmax)             :: tmpl
    complex(kind=dfloat), dimension(p%nmin:p%nmax)          :: tmpm
    ! coefficients (termini noti)
    complex(kind=dfloat), dimension(p%nmin:p%nmax,0:p%nz-1) :: ss, dm 
    real(kind=dfloat),    dimension(p%nmin:p%nmax,0:p%nz-1) :: dl 
    ! coefficients of the scheme (to compute derivative combination)
    real(kind=dfloat), parameter ::  ca = 1._dfloat/3._dfloat , &
                                     cb = 1._dfloat/4._dfloat  , &
                                     cd = 3._dfloat          
    ! coefficients of the scheme (to compute function combination)
    real(kind=dfloat), parameter ::  c1 = 7._dfloat/9._dfloat , &
                                     c2 = 1._dfloat/36._dfloat  , &
                                     ca1 = 17._dfloat/6._dfloat , &
                                     ca2 = 3._dfloat/2._dfloat  , &
                                     ca3 = 1._dfloat/6._dfloat  , & 
                                     ca4 = 3._dfloat/4._dfloat  
  
    ! are the local fields already allocated?
    ! Is it the first time this procedure is called?
    integer :: iz
    ! MPI variables
    integer           :: status(mpi_status_size)
    integer           :: zlen,  &
                         ireqs1, ireqs2, ireqs3, ireqs4, ireqs5,&
                         ireqr1, ireqr2, ireqr3, ireqr4, ireqr5
    integer           :: istart, iend
    integer, save     :: left, right  


    if (p%mpi%me .eq. p%mpi%root) then
       left = mpi_proc_null
    else
       left = p%mpi%me - 1
    end if
    if (p%mpi%me .eq. p%mpi%np-1) then
       right = mpi_proc_null
    else
       right = p%mpi%me + 1
    end if

    oz1d(:, 0:p%nz-1) = z1d(:, 0:p%nz-1)
    ! comunicate oz1d borders to compute ss
    zlen = (p%nmax - p%nmin + 1) * 2
    ! send oz1d to the right 
    call mpi_isend (z1d(0,p%nz-2), zlen, mpi_double_complex, &
         right, 12, mpi_comm_world, ireqs1, ierr)
    ! receive oz1d from the left
    call mpi_irecv (oz1d(0,-2),     zlen, mpi_double_complex, &
         left , 12, mpi_comm_world, ireqr1, ierr) 
    ! receive oz1d from the right
    call mpi_irecv (oz1d(0,p%nz),   zlen, mpi_double_complex, &
         right, 13, mpi_comm_world, ireqr2, ierr)
    ! send oz1d to the left 
    call mpi_isend (z1d(0,0),      zlen, mpi_double_complex, &
         left , 13, mpi_comm_world, ireqs2, ierr)

    ! compute coefficients for oz1d 
    if (p%mpi%me .eq. p%mpi%root) then 
       ! For processor 0, start at iz=2: the new ss(:,0:1) is computed here 
       istart = 2
       ss(:, 0) = (- ca1 * oz1d(:, 0) &
                   + ca2 * oz1d(:, 1) &
                   + ca2 * oz1d(:, 2) &
                   - ca3 * oz1d(:, 3)) / p%dz 
       ss(:, 1) = ca4 * (oz1d(:,2) - oz1d(:, 0)) / p%dz
    else 
       ! otherwise, start at iz=0, and use communicated data (iz=-2,-1)
       istart = 0
    endif
    if (p%mpi%me .eq. p%mpi%np-1) then 
       iend = p%nz - 3
       ss(:, p%nz-2) = ca4 * (oz1d(:,p%nz-1) - oz1d(:, p%nz-3)) / p%dz
       ss(:, p%nz-1) =  (+ ca1 * oz1d(:, p%nz-1) &
                         - ca2 * oz1d(:, p%nz-2) &
                         - ca2 * oz1d(:, p%nz-3) &
                         + ca3 * oz1d(:, p%nz-4)) / p%dz 
    else
       iend = p%nz - 1
    endif
    ! wait until completion of non-blocking communications for oz1d
    call mpi_wait (ireqs1, status, ierr)
    call mpi_wait (ireqr2, status, ierr)
    call mpi_wait (ireqr1, status, ierr)
    call mpi_wait (ireqs2, status, ierr)

    do iz = istart, iend
       ss(:, iz) = (c1 * (oz1d(:, iz+1) - oz1d(:, iz-1)) &
                   +c2 * (oz1d(:, iz+2) - oz1d(:, iz-2))) / p%dz 
    enddo

    !if (p%mpi%me .ne. p%mpi%root) then
    !   do iz = 0, istart-1
    !      ss(:, iz) = (c1 * (oz1d(:, iz+1) - oz1d(:, iz-1)) &
    !                   +c2 * (oz1d(:, iz+2) - oz1d(:, iz-2))) / p%dz 
    !   enddo
    !endif
    !if (p%mpi%me .ne. p%mpi%np-1) then
    !   do iz = iend+1, p%nz-1
    !      ss(:, iz) = (c1 * (oz1d(:, iz+1) - oz1d(:, iz-1)) &
    !                   +c2 * (oz1d(:, iz+2) - oz1d(:, iz-2))) / p%dz 
    !   enddo
    !endif
    
    ! compute dl and dm using communications
    ! start for proc 0, else wait for communications
    ! at the and of the loop in the previous proc
    if (p%mpi%me .eq. p%mpi%root) then 
       dl(:, 0) = -cd
       dm(:, 0) = ss(:, 0)
       dl(:, 1) = -cb / (1._dfloat  + cb * dl(:, 0))
       dm(:, 1) = (ss(:, 1) - cb * dm(:, 0)) / &
                   (1._dfloat  + cb * dl(:, 0))
       istart = 2
       iend = p%nz-3
    else
       zlen = (p%nmax - p%nmin + 1)
       call mpi_recv(tmpl(0)     , zlen, mpi_double_precision, left , 14, & 
                     mpi_comm_world, status, ierr)
       call mpi_recv(tmpm(0)     , zlen, mpi_double_complex, left , 15, & 
                     mpi_comm_world, status, ierr)
       dl(:, 0) = -ca / (1._dfloat + ca * tmpl(:))
       dm(:, 0) = (ss(:, 0) - ca * tmpm(:)) / &
                   (1._dfloat + ca * tmpl(:))
       istart = 1
       iend = p%nz-3
    endif
    do iz = istart, iend
       dl(:, iz) = -ca / (1._dfloat  + ca * dl(:, iz-1))
       dm(:, iz) = (ss(:, iz) - ca * dm(:, iz-1)) / &
                   (1._dfloat  + ca * dl(:, iz -1))
    enddo
    if (p%mpi%me .eq. p%mpi%np-1) then 
       dl(:, p%nz-2) = -cb / (1._dfloat  + cb * dl(:, p%nz-3))
       dm(:, p%nz-2) = (ss(:, p%nz-2) - cb * dm(:, p%nz-3)) / &
                   (1._dfloat  + cb * dl(:, p%nz-3))
       dz1d(:, p%nz-1) = (ss(:, p%nz-1) - cd * dm(:, p%nz-2)) / &
                        (1._dfloat + cd * dl(:, p%nz-2))
       dl(:, p%nz-1) = 0._dfloat
       dm(:, p%nz-1) = (ss(:, p%nz-1) - cd * dm(:, p%nz-2)) / &
                   (1._dfloat  + cd * dl(:, p%nz-2))
    else 
       do iz = iend+1, p%nz-1
          dl(:, iz) = -ca / (1._dfloat  + ca * dl(:, iz-1))
          dm(:, iz) = (ss(:, iz) - ca * dm(:, iz-1)) / &
                   (1._dfloat  + ca * dl(:, iz -1))
       enddo
       zlen = (p%nmax - p%nmin + 1)
       call mpi_send(dl(0,p%nz-1), zlen, mpi_double_precision, right, 14, &
                         mpi_comm_world, ierr)
       call mpi_send(dm(0,p%nz-1), zlen, mpi_double_complex, right, 15, &
                         mpi_comm_world, ierr)
    endif
   

    ! compute the derivatives
    zlen = (p%nmax - p%nmin + 1) 
    istart = 0
    iend = p%nz-2
    ! start for proc p%mpi%np-1, else wait for communications
    ! at the and of the loop in the next proc
    if (p%mpi%me .ne. p%mpi%np-1) then 
       call mpi_recv (tmpm(0),  zlen, mpi_double_complex  , &
                      right, 16, mpi_comm_world, status, ierr) 
       dz1d(:, p%nz-1) = dm(:, p%nz-1) + dl(:, p%nz-1) * tmpm(:)
    endif
    do iz =  iend, istart, -1
       dz1d(:, iz) = dm(:, iz) + dl(:, iz) * dz1d(:, iz+1)
    enddo
    if (p%mpi%me .ne. p%mpi%root) then
       call mpi_send (dz1d(0,0), zlen, mpi_double_complex  , &
                   left, 16, mpi_comm_world, ierr)
    endif
    
    ! correct boundary condition for zm
    ! only for open geometry, not good here!
    if (p%mpi%me .eq. p%mpi%np-1 .and. sig .eq. -1) &
       dz1d(:, p%nz-1) = 0

    ! update for reflection and propagation
    do iz = 0, p%nz-1
       dz1d(:, iz) = - sig * dz1d(:, iz) * va(iz) &
                      - sig * 0.5_dfloat * vad(iz) * z2d(:, iz)
    enddo

  end subroutine subprop_f


  subroutine subprop_b (z1d, z2d, dz1d, k, p, t, sig)
    use mpi
    use atmosphere
!-
    implicit none
    type(params),         intent(in)                         :: p
    complex(kind=dfloat), intent(in), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                 :: z1d, z2d
    ! sign to compute time derivative: +1 for zp, -1 for zm
    integer, intent(in)                                     :: sig          
    complex(kind=dfloat), intent(out), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                 :: dz1d
    ! not used
    complex(kind=dfloat), intent(in),  dimension(p%nmin:p%nmax) :: k
    ! not used
    real(kind=dfloat) :: t

    ! temporary variable for communications
    complex(kind=dfloat), &
         dimension(p%nmin:p%nmax, -2:p%nz+1)                 :: oz1d
    real(kind=dfloat), dimension(p%nmin:p%nmax)             :: tmpl
    complex(kind=dfloat), dimension(p%nmin:p%nmax)          :: tmpm
    ! coefficients (termini noti)
    complex(kind=dfloat), dimension(p%nmin:p%nmax,0:p%nz-1) :: ss, dm 
    real(kind=dfloat),    dimension(p%nmin:p%nmax,0:p%nz-1) :: dl 
    ! coefficients of the scheme (to compute derivative combination)
    real(kind=dfloat), parameter ::  ca = 1._dfloat/3._dfloat , &
                                     cb = 1._dfloat/4._dfloat  , &
                                     cd = 3._dfloat          
    ! coefficients of the scheme (to compute function combination)
    real(kind=dfloat), parameter ::  c1 = 7._dfloat/9._dfloat , &
                                     c2 = 1._dfloat/36._dfloat  , &
                                     ca1 = 17._dfloat/6._dfloat , &
                                     ca2 = 3._dfloat/2._dfloat  , &
                                     ca3 = 1._dfloat/6._dfloat  , & 
                                     ca4 = 3._dfloat/4._dfloat  
  
    ! are the local fields already allocated?
    ! Is it the first time this procedure is called?
    integer :: iz
    ! MPI variables
    integer           :: status(mpi_status_size)
    integer           :: zlen,  &
                         ireqs1, ireqs2, ireqs3, ireqs4, ireqs5,&
                         ireqr1, ireqr2, ireqr3, ireqr4, ireqr5
    integer           :: istart, iend
    integer, save     :: left, right  


    if (p%mpi%me .eq. p%mpi%root) then
       left = mpi_proc_null
    else
       left = p%mpi%me - 1
    end if
    if (p%mpi%me .eq. p%mpi%np-1) then
       right = mpi_proc_null
    else
       right = p%mpi%me + 1
    end if

    oz1d(:, 0:p%nz-1) = z1d(:, 0:p%nz-1)
    ! comunicate oz1d borders to compute ss
    zlen = (p%nmax - p%nmin + 1) * 2
    ! send oz1d to the right 
    call mpi_isend (z1d(0,p%nz-2), zlen, mpi_double_complex, &
         right, 12, mpi_comm_world, ireqs1, ierr)
    ! receive oz1d from the left
    call mpi_irecv (oz1d(0,-2),     zlen, mpi_double_complex, &
         left , 12, mpi_comm_world, ireqr1, ierr) 
    ! receive oz1d from the right
    call mpi_irecv (oz1d(0,p%nz),   zlen, mpi_double_complex, &
         right, 13, mpi_comm_world, ireqr2, ierr)
    ! send oz1d to the left 
    call mpi_isend (z1d(0,0),      zlen, mpi_double_complex, &
         left , 13, mpi_comm_world, ireqs2, ierr)

    ! compute coefficients for oz1d 
    if (p%mpi%me .eq. p%mpi%root) then 
       ! For processor 0, start at iz=2: the new ss(:,0:1) is computed here 
       istart = 2
       ss(:, 0) = (- ca1 * oz1d(:, 0) &
                   + ca2 * oz1d(:, 1) &
                   + ca2 * oz1d(:, 2) &
                   - ca3 * oz1d(:, 3)) / p%dz 
       ss(:, 1) = ca4 * (oz1d(:,2) - oz1d(:, 0)) / p%dz
    else 
       ! otherwise, start at iz=0, and use communicated data (iz=-2,-1)
       istart = 0
    endif
    if (p%mpi%me .eq. p%mpi%np-1) then 
       iend = p%nz - 3
       ss(:, p%nz-2) = ca4 * (oz1d(:,p%nz-1) - oz1d(:, p%nz-3)) / p%dz
       ss(:, p%nz-1) =  (+ ca1 * oz1d(:, p%nz-1) &
                         - ca2 * oz1d(:, p%nz-2) &
                         - ca2 * oz1d(:, p%nz-3) &
                         + ca3 * oz1d(:, p%nz-4)) / p%dz 
    else
       iend = p%nz - 1
    endif
    ! wait until completion of non-blocking communications for oz1d
    call mpi_wait (ireqs1, status, ierr)
    call mpi_wait (ireqr2, status, ierr)
    call mpi_wait (ireqr1, status, ierr)
    call mpi_wait (ireqs2, status, ierr)

    do iz = istart, iend
       ss(:, iz) = (c1 * (oz1d(:, iz+1) - oz1d(:, iz-1)) &
                   +c2 * (oz1d(:, iz+2) - oz1d(:, iz-2))) / p%dz 
    enddo
    !if (p%mpi%me .ne. p%mpi%root) then
    !   do iz = 0, istart-1
    !      ss(:, iz) = (c1 * (oz1d(:, iz+1) - oz1d(:, iz-1)) &
    !               +c2 * (oz1d(:, iz+2) - oz1d(:, iz-2))) / p%dz 
    !   enddo
    !endif
    !if (p%mpi%me .ne. p%mpi%np-1) then
    !   do iz = iend+1, p%nz-1
    !      ss(:, iz) = (c1 * (oz1d(:, iz+1) - oz1d(:, iz-1)) &
    !               +c2 * (oz1d(:, iz+2) - oz1d(:, iz-2))) / p%dz 
    !   enddo
    !endif
    
    ! compute dl and dm using communications
    ! start for proc p%mpi%mp-1, else wait for communications
    ! at the and of the loop in the next proc
    if (p%mpi%me .eq. p%mpi%np-1) then 
       dl(:, p%nz-1) = -cd
       dm(:, p%nz-1) = ss(:, p%nz-1)
       dl(:, p%nz-2) = -cb / (1._dfloat  + cb * dl(:, p%nz-1))
       dm(:, p%nz-2) = (ss(:, p%nz-2) - cb * dm(:, p%nz-1)) / &
                   (1._dfloat  + cb * dl(:, p%nz-1))
       istart = 0
       iend = p%nz-3
    else
       zlen = (p%nmax - p%nmin + 1)
       call mpi_recv(tmpl(0)     , zlen, mpi_double_precision, right , 14, & 
                     mpi_comm_world, status, ierr)
       call mpi_recv(tmpm(0)     , zlen, mpi_double_complex, right , 15, & 
                     mpi_comm_world, status, ierr)
       dl(:, p%nz-1) = -ca / (1._dfloat + ca * tmpl(:))
       dm(:, p%nz-1) = (ss(:, p%nz-1) - ca * tmpm(:)) / &
                   (1._dfloat + ca * tmpl(:))
       istart = 0
       iend = p%nz-2
    endif
    do iz = iend, istart, -1
       dl(:, iz) = -ca / (1._dfloat  + ca * dl(:, iz+1))
       dm(:, iz) = (ss(:, iz) - ca * dm(:, iz+1)) / &
                   (1._dfloat  + ca * dl(:, iz+1))
    enddo
    if (p%mpi%me .ne. p%mpi%root) then 
       zlen = (p%nmax - p%nmin + 1)
       call mpi_send(dl(0,0), zlen, mpi_double_precision, left, 14, &
                         mpi_comm_world, ierr)
       call mpi_send(dm(0,0), zlen, mpi_double_complex, left, 15, &
                         mpi_comm_world, ierr)
    else 
       dl(:, 1) = -cb / (1._dfloat  + cb * dl(:, 2))
       dm(:, 1) = (ss(:, 1) - cb * dm(:, 2)) / &
                   (1._dfloat  + cb * dl(:, 2))
       dl(:, 0) = 0._dfloat
       dm(:, 0) = (ss(:, 0) - cd * dm(:, 1)) / &
                   (1._dfloat  + cd * dl(:, 1))
       dz1d(:, 0) = (ss(:, 0) - cd * dm(:, 1)) / &
                        (1._dfloat + cd * dl(:, 1))
    endif
   

    ! compute the derivatives
    zlen = (p%nmax - p%nmin + 1) 
    istart = 1
    iend = p%nz-1
    ! start for proc 0, else wait for communications
    ! at the and of the loop in the previous proc
    if (p%mpi%me .ne. p%mpi%root) then 
       call mpi_recv (tmpm(0),  zlen, mpi_double_complex  , &
                      left, 16, mpi_comm_world, status, ierr) 
       dz1d(:, 0) = dm(:, 0) + dl(:, 0) * tmpm(:)
    endif
    do iz =  istart, iend
       dz1d(:, iz) = dm(:, iz) + dl(:, iz) * dz1d(:, iz-1)
    enddo
    if (p%mpi%me .ne. p%mpi%np-1) then
       call mpi_send (dz1d(0,p%nz-1), zlen, mpi_double_complex  , &
                   right, 16, mpi_comm_world, ierr)
    endif
    

    ! update for reflection and propagation
    iend=p%nz-1
    do iz = 0, p%nz-1
       dz1d(:, iz) = - sig * dz1d(:, iz) * va(iz) &
                      - sig * 0.5_dfloat * vad(iz) * z2d(:, iz)
    enddo

    ! correct boundary condition for zm
    ! only for open geometry, not good here!
    if (p%mpi%me .eq. p%mpi%np-1 .and. sig .eq. -1) &
       dz1d(:, p%nz-1) = 0

  end subroutine subprop_b

!Filtraggio dei campi in direzione x, per inversione del sistema:
!aiYi-1+Yi + biYi+1 = Si = al*(fi+2 + fi-2) + be*(fi+1 + fi-1)
!schema del quarto ordine
  subroutine filter(zd, p)

    use mpi

    implicit none
    type(params),         intent(in)                         :: p
    complex(kind=dfloat), intent(inout), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                  :: zd

    ! temporary variable for communication
    complex(kind=dfloat), dimension(p%nmin:p%nmax)          :: tmpm
    ! temporary variable for communication
    real(kind=dfloat), dimension(p%nmin:p%nmax)             :: tmpl
    ! temporary variables to compute derivative
    complex(kind=dfloat), &
          dimension(p%nmin:p%nmax,-2:p%nz+1) :: zd1
    complex(kind=dfloat), &
          dimension(p%nmin:p%nmax,-1:p%nz-1) :: dm
    real(kind=dfloat), &
         dimension(p%nmin:p%nmax, -1:p%nz-1) :: dl, endl
    ! parameters for the filtering
    real(kind=dfloat) :: aaj, aj, bj, cj
  
    integer :: iz
    ! MPI variables
    integer           :: status(mpi_status_size)
    integer           :: zlen, zlen2, &
                         ireqs1, ireqs2, &
                         ireqr1, ireqr2 
    integer           :: istart, iend
    integer, save     :: left, right  


    if (p%mpi%me .eq. p%mpi%root) then
       left = mpi_proc_null
    else
       left = p%mpi%me - 1
    end if
    if (p%mpi%me .eq. p%mpi%np-1) then
       right = mpi_proc_null
    else
       right = p%mpi%me + 1
    end if


    aaj = 0.475_dfloat
    aj=(5._dfloat+6._dfloat*aaj)/8._dfloat
    bj=(1._dfloat+2._dfloat*aaj)/4._dfloat
    cj=-(1._dfloat-2._dfloat*aaj)/16._dfloat
    zd1(:,0:p%nz-1) = zd(:,:)

    if (p%mpi%me .eq. p%mpi%root) then 
       dl(:, -1:2) = 0._dfloat
       dl(:, 3) = -aaj
       istart = 4
       iend = p%nz-1
    else
       zlen = (p%nmax - p%nmin + 1)
       call mpi_recv(dl(0, -1)   , zlen, mpi_double_precision, left , 14, & 
                     mpi_comm_world, status, ierr)
       istart=0
       iend = p%nz-1
    endif
    if (p%mpi%me .eq. p%mpi%np-1) iend = p%nz-4

    do iz = istart, iend
       dl(:, iz) = -aaj / (1._dfloat + aaj * dl(:, iz-1))
    enddo

    if (p%mpi%me .ne. p%mpi%np-1) then 
       zlen = (p%nmax - p%nmin + 1)
       call mpi_send(dl(0,p%nz-1), zlen, mpi_double_precision, right, 14, &
                         mpi_comm_world, ierr)
    endif

    do iz = istart-1, iend
       endl(:, iz) = 1._dfloat / (1._dfloat + aaj * dl(:, iz))
    enddo
   
    ! comunicate dl, endl, dm at the border 
    ! sent to right only
    !zlen = (p%nmax - p%nmin + 1)
    !call mpi_sendrecv(dl(0, p%nz-1), zlen, mpi_double_precision, right, 16, &
    !                  dl(0, -1)    , zlen, mpi_double_precision, left , 16, &
    !                  mpi_comm_world, status, ierr)
    !call mpi_sendrecv(endl(0, p%nz-1), zlen, mpi_double_precision, right, 17, &
    !                  endl(0, -1)    , zlen, mpi_double_precision, left , 17, &
    !                  mpi_comm_world, status, ierr)

    ! comunicate zd1 borders to compute dm or zd
    zlen = (p%nmax - p%nmin + 1) * 2
    ! send zd1 to the right 
    call mpi_isend (zd1(0,p%nz-2), zlen, mpi_double_complex, &
         right, 12, mpi_comm_world, ireqs1, ierr)
    ! receive zd1 from the left
    call mpi_irecv (zd1(0,-2),     zlen, mpi_double_complex, &
         left , 12, mpi_comm_world, ireqr1, ierr) 
    ! receive zd1 from the right
    call mpi_irecv (zd1(0,p%nz),   zlen, mpi_double_complex, &
         right, 13, mpi_comm_world, ireqr2, ierr)
    ! send zd1 to the left 
    call mpi_isend (zd1(0,0),      zlen, mpi_double_complex, &
         left , 13, mpi_comm_world, ireqs2, ierr)

    ! to compute dm start for proc 0, else wait for communications
    ! at the and of the loop in the previous proc
    zlen2 = (p%nmax - p%nmin + 1) 
    if (p%mpi%me .eq. p%mpi%np-1) then 
       iend = p%nz - 4
    else
       iend = p%nz - 1
    endif
    if (p%mpi%me .eq. p%mpi%root) then 
       ! For processor 0, start at iz=4: the new dm(:,0:3) is computed here 
       dm(:, 0) = 1._dfloat / 16._dfloat * &
                   (15._dfloat * zd1(:, 0) + 4._dfloat * zd1(:, 1) &
                    -6._dfloat * zd1(:, 2) + 4._dfloat * zd1(:,3) - zd1(:, 4))
       dm(:, 1) = 1._dfloat / 16._dfloat * (12._dfloat * zd1(:, 1) &
                    +zd1(:, 0) + 6._dfloat * zd1(:,2)  &
                    -4._dfloat * zd1(:, 3) + zd1(:, 4))
       dm(:, 2) = 1._dfloat / 16._dfloat * (10._dfloat * zd1(:, 2) &
                    -zd1(:, 0) + 4._dfloat * zd1(:, 1)  &
                    +4._dfloat * zd1(:, 3) - zd1(:, 4))
       dm(:, 3) = aj * zd1(:, 3) + bj * (zd1(:, 4) + zd1(:, 2)) &
                  +cj * (zd1(:, 5) + zd1(:, 1)) - aaj * dm(:, 2)
       istart = 4
    else 
       ! otherwise, start at iz=0, and use communicated data (iz=-2,-1)
       call mpi_recv(dm(0, -1),zlen2, mpi_double_complex  , left , 18, &
                     mpi_comm_world, status, ierr)
       istart = 0
    endif
    ! wait until completion of non-blocking communications for zd1
    call mpi_wait (ireqs1, status, ierr)
    call mpi_wait (ireqr2, status, ierr)
    call mpi_wait (ireqr1, status, ierr)
    call mpi_wait (ireqs2, status, ierr)

    do iz = istart, iend
       dm(:, iz) = endl(:, iz-1) * &
                   (aj * zd1(:, iz) + bj * (zd1(:, iz+1) + zd1(:, iz-1)) &
                   + cj * (zd1(:, iz+2) + zd1(:, iz-2)) - aaj * dm(:, iz-1))
    enddo
     
    if (p%mpi%me .ne. p%mpi%np-1) then
       call mpi_send(dm(0, p%nz-1), zlen2, mpi_double_complex  , right, 18, &
                      mpi_comm_world, ierr)
    endif
    

    ! compute zd1(filtered) 
    ! start for proc p%mpi%np-1, else wait for communications
    ! at the and of the loop in the next proc
    zlen = (p%nmax - p%nmin + 1) 
    if (p%mpi%me .eq. p%mpi%np-1) then 
       zd(:, p%nz-1) = 1._dfloat / 16._dfloat * &
                   (15._dfloat * zd1(:, p%nz-1) + 4._dfloat * zd1(:, p%nz-2) &
                   -6._dfloat * zd1(:, p%nz-3) & 
                   +4._dfloat * zd1(:,p%nz-4) - zd1(:, p%nz-5))
       zd(:, p%nz-2) = 1._dfloat / 16._dfloat * (12._dfloat * zd1(:,p%nz-2) &
                   +zd1(:, p%nz-1) + 6._dfloat * zd1(:,p%nz-3) &
                   -4._dfloat * zd1(:, p%nz-4) + zd1(:, p%nz-5))
       zd(:, p%nz-3) = 1._dfloat / 16._dfloat * (10._dfloat * zd1(:, p%nz-3)&
                    -zd1(:, p%nz-1) + 4._dfloat * zd1(:, p%nz-2) &
                    +4._dfloat * zd1(:, p%nz-4) - zd1(:, p%nz-5))
       zd(:, p%nz-4) = dl(:, p%nz-4) * zd(:, p%nz-3) + dm(:, p%nz-4)
       istart = 0
       iend = p%nz-5
    else
       call mpi_recv (tmpm(0),  zlen, mpi_double_complex  , &
                      right, 19, mpi_comm_world, status, ierr) 
       zd(:, p%nz-1) = dl(:, p%nz-1) * tmpm(:) + dm(:, p%nz-1)
       istart = 0
       iend = p%nz-2
    endif
    if (p%mpi%me .eq. p%mpi%root) istart = 3

    do iz =  iend, istart, -1
       zd(:, iz) = dl(:, iz) * zd(:, iz+1) + dm(:, iz)
    enddo

    if (p%mpi%me .ne. p%mpi%root) then
       call mpi_send (zd(0,0), zlen, mpi_double_complex  , &
                   left, 19, mpi_comm_world, ierr)
    else
       !zd(:, 0) = 1._dfloat / 16._dfloat * &
       !            (15._dfloat * zd1(:, 0) + 4._dfloat * zd1(:, 1) &
       !             -6._dfloat * zd1(:, 2) + 4._dfloat * zd1(:,3) - zd1(:, 4))
       !zd(:, 1) = 1._dfloat / 16._dfloat * (12._dfloat * zd1(:, 1) &
       !             +zd1(:, 0) + 6._dfloat * zd1(:,2)  &
       !             -4._dfloat * zd1(:, 3) + zd1(:, 4))
       !zd(:, 2) = 1._dfloat / 16._dfloat * (10._dfloat * zd1(:, 2) &
       !             -zd1(:, 0) + 4._dfloat * zd1(:, 1)  &
       !             +4._dfloat * zd1(:, 3) - zd1(:, 4))
       zd(:, 0:2) = dm(:, 0:2)
    endif
 
  return
 end subroutine filter

end module propagation
