!+
!***********************************************************
! Module fexpand-MJ
!***********************************************************
! Routines for Munro-Jackson (1981) expansion factor
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  19 Aug 05: AV Created
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module fexpand
  use types
  use unit
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_fexpand = "MJ"

contains

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Expansion factor as a function of r
! non-dimensional unit
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function fexpansion(r, pexp)
!-
    implicit none
    type(paramexp), intent(in)      :: pexp
    real(kind=dfloat), intent(in)   :: r
    real(kind=dfloat)               :: y, f1
    real(kind=dfloat)               :: fexpansion
     
    y = (r/Rsun - pexp%x1n) / pexp%sigma
    f1 = 1._dfloat - (pexp%fmax - 1._dfloat) * &
         exp((pexp%x_0 - pexp%x1n) / pexp%sigma)

    fexpansion=(pexp%fmax*exp(y)+f1)/(exp(y)+1._dfloat)

  end function fexpansion



!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Logarithmic derivative of the expansion factor as a function of r
! non-dimensional unit
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function fdlexpansion(r, pexp)
!-
    implicit none
    type(paramexp), intent(in)      :: pexp
    real(kind=dfloat), intent(in)   :: r
    real(kind=dfloat)               :: y, f1
    real(kind=dfloat)               :: fdlexpansion

    y = (r/Rsun - pexp%x1n) / pexp%sigma
    f1 = 1._dfloat - (pexp%fmax - 1._dfloat) * &
         exp((pexp%x_0 - pexp%x1n) / pexp%sigma)

    fdlexpansion = (pexp%fmax - f1)/pexp%sigma*exp(y)/&
                   (exp(y)+1._dfloat)/&
                   (pexp%fmax*exp(y)+f1)

  end function fdlexpansion

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Print parameters specific to expansion 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine expansion_print_parameters
!-
    implicit none

    character (len=*), parameter :: form1 = "(A, I12,    A, I12   )"
    character (len=*), parameter :: form2 = "(A, ES12.5, A, ES12.5)"
    character (len=*), parameter :: form3 = "(A, I12   , A, ES12.5)"
    character (len=*), parameter :: form4 = "(A, I12   )"
    character (len=*), parameter :: form5 = "(A, ES12.5)"

    print*, '  Flux tube expansion: MJ (supra-spherical)'

  end subroutine expansion_print_parameters

end module fexpand
