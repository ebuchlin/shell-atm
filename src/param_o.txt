# Period for output of integrated quantities (time if <0, steps if >0)
-.001
# Period for output of non-integrated quantities (time if <0, steps if >0)
-.01
# Number of planes to output in out_simstate_nnn (all if 0)
0
# Number of structure functions
10
# Time
1
# Non-linear time scale
1
# Total energy in modes 02
1
# Total energy in modes 08
1
# Total energy in modes 14
1
# Total energy in modes 20
0
# Total kinetic energy
1
# Total magnetic energy
1
# Total kinetic energy dissipation
1
# Total magnetic energy dissipation
1
# Power of the forcing at boundaries
1
# Cross helicity
1
# "Magnetic helicity"
0
# Kinetic energy spectrum (time average, longitudinal sum)
1
# Magnetic energy spectrum (time average, longitudinal sum)
1
# Velocity field structure function
0
# Magnetic field structure function
0
# Raw fields (output of out_simstate_nnn.dat)
1
# Profile of average dissipation per unit length
1
# Profile of Alfven velocity
1
# Profile of Alfven velocity gradient
1
# Profile of mass density
1
# Position as a function of z index
1
# Profile of over-expansion factor
1
# Total magnetic energy of each plane (not used)
0
