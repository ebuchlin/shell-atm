!+
!***********************************************************
! Module atmosphere-spher
!***********************************************************
! Variables and routines for a spherical atmosphere
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  15 Jul 05 AV Created
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module atmosphere
  use types
  use fexpand
  use unit
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_atmosphere = "spher"

  ! Non-dimensional scale height of density variation
  ! (escape velocity / sound speed)^2 / 2
  ! become part of the type params (p%h) not needed anymore!
  real(kind=dfloat) :: alpha = 6._dfloat

  ! Atmosphere
  real(kind=dfloat), dimension(:), allocatable :: &
       va, va_m, vad, rho, r, aexp, kaexp

  ! normalisation value (rho^1/4), unorm = 1/rho^1/4
  real(kind=dfloat),    dimension(:),   allocatable :: unorm
  ! normalisation value: sound speed
  real(kind=dfloat)                            :: CsN

contains 

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Allocate variables for the atmosphere
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine atmosphere_allocate(p)
!-
    type(params), intent(in)      :: p

    allocate (va(0:p%nz-1))  
    allocate (va_m(0:p%nz-1))  
    allocate (vad(0:p%nz-1))  
    allocate (rho(0:p%nz-1))  
    allocate (r(0:p%nz-1))  
    allocate (unorm(0:p%nz-1))
    allocate (aexp(0:p%nz-1))
    allocate (kaexp(0:p%nz-1))

  end subroutine atmosphere_allocate


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Deallocate variables for the atmosphere
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine atmosphere_deallocate
!-
    deallocate (va)
    deallocate (va_m)
    deallocate (vad)
    deallocate (rho)
    deallocate (r)
    deallocate (unorm)
    deallocate (aexp)
    deallocate (kaexp)

  end subroutine atmosphere_deallocate

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Assign values for Alfv�n speed, Alfv�n speed gradient and 
! mass density to the grid points.
! These values are computed by the xxx_det routines
!    (different alternatives according to geometry)
! MPI: done on each processor separately (but need to check if OK)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine grid_assign (p)
!-
    implicit none
    type(params), intent(in)      :: p
    real(kind=dfloat)             :: NsC
    integer                       :: i
    real(kind=dfloat)             :: ir

    ! Assign grid values for r and va_m
    ! r: linear
    ! for atmosphere-dependent r, need to define r_det in atmosphere module
    do i = 0, p%nz-1
       ir = i + p%nz0
       ! r at mid-point (forward) for computing Va at mid-point
       ! will be overwritten just after
       r(i) = r_det (ir + 0.5_dfloat, p)
       va_m(i) = va_det (r(i), p)
       ! r at grid point (overwrites the value of r at mid-point)
       r(i) = r_det (ir, p)
    enddo

    ! Assign grid values for va, vad, rho, aexp, kaexp
    do i = 0, p%nz-1
       va(i)   =   va_det(r(i), p)
       vad(i)  =  vad_det(r(i), p) 
       rho(i)  =  rho_det(r(i), p)
       aexp(i) = aexp_det(r(i), p)
    enddo
    kaexp = 1._dfloat / sqrt(aexp)

    if (vad(p%nz-1) .gt. 1e-6 .and. p%mpi%me .eq. p%mpi%np-1) then
       print*, ' Warning: vad at the top boundary greater than 1e-6:', &
            vad(p%nz-1)
    endif

    ! normalisation factor for zp and zm
    unorm = rho ** (-.25_dfloat)
    ! normalisation factor for velocities
    CsN = cs_det(p)

    if (p%mpi%me .eq. p%mpi%root) then
       print*, '  Va(0) = ', va(0)
       print*, '    CsN = ', CsN
       print*, 'Beta(0) = ', 2._dfloat  * (CsN / Va(0)) ** 2
    end if
  end subroutine grid_assign


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Determination of position/altitude as a function of index of plane
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function r_det (i, p)
!-
    implicit none
    type(params), intent(in)      :: p
    real(kind=dfloat), intent(in) :: i
    real(kind=dfloat)  :: r_det
    
    r_det = Rsun + i * p%dz

  end function r_det


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Determination of Alfv�n velocity as a function of altitude
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function va_det (r, p)
!-
    implicit none
    type(params), intent(in)      :: p
    real(kind=dfloat), intent(in) :: r
    real(kind=dfloat)  :: va_det

    ! Spherical stratification induced by central object 
    ! alfa = (G * m) / (r * cs^2) with m = mass of central object
    ! G gravitational constant,        r = radius of central object
!     va_det = p%b0 / sqrt (4._dfloat * pi * p%rho0) / aexp_det(r, p) * &
!             exp (p%h * 0.5_dfloat * (1._dfloat - 1._dfloat / r))
    va_det = b_det (r, p) / sqrt (4._dfloat * pi * rho_det (r, p))

  end function va_det



!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Determination of Alfv�n velocity gradient as a function of altitude
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function vad_det (r, p)
!-
    implicit none
    type(params), intent(in)      :: p
    real(kind=dfloat), intent(in) :: r
    real(kind=dfloat)  :: vad_det

!     vad_det = va_det (r, p) * &
!          (p%h * 0.5_dfloat / r / r - aexpdl_det(r, p))
    vad_det = -va_det (r, p) * &
         (aexpdl_det (r, p) + .5_dfloat * rhodl_det (r, p))

  end function vad_det


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Determination of magnetic field as a function of altitude
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function b_det (r, p)
!-
    implicit none
    type(params), intent(in)      :: p
    real(kind=dfloat), intent(in) :: r
    real(kind=dfloat)  :: b_det

    b_det = p%b0 / aexp_det(r, p)
    
  end function b_det


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Determination of density as a function of altitude
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function rho_det (r, p)
!-
    implicit none
    type(params), intent(in)      :: p
    real(kind=dfloat), intent(in) :: r
    real(kind=dfloat)  :: rho_det

    rho_det = p%rho0 * &
         exp ( -Rsun / p%h * (1._dfloat - Rsun / r))

  end function rho_det

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Determination of logarithmic derivative of density 
! as a function of altitude
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function rhodl_det (r, p)
!-
    implicit none
    type(params), intent(in)      :: p
    real(kind=dfloat), intent(in) :: r
    real(kind=dfloat)  :: rhodl_det

    rhodl_det = -1._dfloat / p%h * (Rsun / r) ** 2

  end function rhodl_det


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Determination of flux tube expansion as a function of altitude: A = r^2*f
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function aexp_det(r, p) 
    use fexpand
!-
    implicit none
    type(params), intent(in)    :: p
    real(kind=dfloat)             :: aexp_det, r

    aexp_det =  (r / Rsun)**2 * fexpansion(r, p%pexp) !/ p%pexp%fmax

  end function aexp_det


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Determination of flux tube expansion logarithmic derivative A'/A
! as a function of altitude: A = r^2*f
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function aexpdl_det (r, p)
    use fexpand
!-
    implicit none
    type(params), intent(in)    :: p
    real(kind=dfloat)             :: aexpdl_det, r

    aexpdl_det = 2._dfloat / r + &
                 fdlexpansion(r, p%pexp) / Rsun

  end function aexpdl_det



!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Determination sound speed for normalisation (if needed)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function cs_det (p)
!-
    use unit
    implicit none
    type(params), intent(in)      :: p
    real(kind=dfloat)  :: cs_det

    cs_det = sqrt(gg / p%h)

  end function cs_det


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Print parameters specific to atmosphere
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine atmosphere_print_parameters(p)
!-
    implicit none
    type(params), intent(in)     :: p


    if (p%mpi%me .eq. p%mpi%root) then
       print*, '*** Parameters for the atmosphere ***'
       print*, '  Kind: spherical'
       call expansion_print_parameters
       print*, ''
      end if

  end subroutine atmosphere_print_parameters


end module atmosphere


