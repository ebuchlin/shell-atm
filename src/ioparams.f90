!+
!***********************************************************
! Module ioparams
!***********************************************************
! Input and output of parameters of simulation and of output
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  22 Oct 05: EB Created (moved from shell-atm)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module ioparams
  use mpi
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_ioparams = "default"

contains

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Read parameters from file params.txt
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine read_parameters (p)
!-
    implicit none
    type(params), intent(inout) :: p
    ! need inout because already contains parameters of the parallel run

    ! local variables
    character(len=80)         :: dummy
    integer                   :: tmpint
    real(kind=dfloat)         :: tmp
    integer                   :: excessplanes

    p%mpi%root = 0
    if (p%mpi%me .eq. p%mpi%root) then
       open (90, file='params.txt', status='old')
       read(90,*) dummy;   read(90,*) p%nmin
       read(90,*) dummy;   read(90,*) p%nmax
       read(90,*) dummy;   read(90,*) p%tnz
       read(90,*) dummy;   read(90,*) p%k0
       read(90,*) dummy;   read(90,*) p%lambda
       read(90,*) dummy;   read(90,*) p%nu
       read(90,*) dummy;   read(90,*) p%eta
       read(90,*) dummy;   read(90,*) p%eps
       read(90,*) dummy;   read(90,*) p%epsm
       read(90,*) dummy;   read(90,*) p%b0
       read(90,*) dummy;   read(90,*) p%dz
       read(90,*) dummy;   read(90,*) tmp
       if (tmp .lt. 0) then
          p%tmax = -tmp
          p%nt = huge (1)
       else
          p%tmax = huge (1._dfloat)
          p%nt = tmp
       end if
       read(90,*) dummy;   read(90,*) p%delta_t
       read(90,*) dummy;   read(90,*) tmpint
       if (tmpint == 0) then
          p%continue = .FALSE.
       else
          p%continue = .TRUE.
       end if
       read(90,*) dummy;   read(90,*) p%initamp
       read(90,*) dummy;   read(90,*) p%initslope
       read(90,*) dummy;   read(90,*) p%forcamp
       read(90,*) dummy;   read(90,*) p%tstar
       read(90,*) dummy;   read(90,*) p%rho0
       read(90,*) dummy;   read(90,*) p%h
       close (90)
       open (90, file='paramexp.txt', status='old')
       read(90,*) dummy;   read(90,*) p%pexp%x_0 
       read(90,*) dummy;   read(90,*) p%pexp%sigma
       read(90,*) dummy;   read(90,*) p%pexp%fmax
       read(90,*) dummy;   read(90,*) p%pexp%x1n
       close (90)
    end if
   
    ! broadcast the parameters we have read on root processor
    ! (try not to forget anything... Would be better to define a type for
    ! communicating the parameters, but it is not worth it as this
    ! broadcasting is done only once)
    call mpi_bcast (p%nmin, 1, mpi_integer, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%nmax, 1, mpi_integer, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%tnz, 1, mpi_integer, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%k0, 1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%lambda, 1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%nu, 1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%eta, 1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%eps, 1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%epsm, 1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%b0, 1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%dz, 1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%nt, 1, mpi_integer, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%tmax, 1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%delta_t, 1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%continue, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%initamp, 1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%initslope,1,mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%forcamp, 1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%tstar,     1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%rho0,      1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%h,         1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    ! parameters for the expansion function (if needed)
    call mpi_bcast (p%pexp%x_0,  1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%pexp%sigma,1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%pexp%fmax, 1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (p%pexp%x1n,  1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)

    ! other variables contained in p
    p%forcp = 0._dfloat

    ! deduce p%nz for current processor from p%tnz
    p%nz = p%tnz / p%mpi%np
    excessplanes = p%tnz - p%nz * p%mpi%np
    ! Warn if need of excessplanes, even if it works fine; 
    ! this is not very useful and can be suppressed
    if ((p%mpi%me .eq. p%mpi%root) .and. (excessplanes .ne. 0)) &
         print*, 'Warning: number of planes is not a multiple of number of processors'
    if (excessplanes .gt. p%mpi%me) then
       p%nz = p%nz + 1
       p%nz0 = p%nz * p%mpi%me
    else
       p%nz0 = p%nz * p%mpi%me + excessplanes
    end if

!!! Warning: p%nz for processor 0 must be the largest of all
!!! p%nz's

    ! allocate random coefficients of forcing
    !! (but these are not allocatable anymore, see types.f90)
!     allocate(p%fc%a1(p%nmin:p%nmax))
!     allocate(p%fc%b1(p%nmin:p%nmax))
!     allocate(p%fc%a2(p%nmin:p%nmax))
!     allocate(p%fc%b2(p%nmin:p%nmax))

    ! initialize random coefficients of forcing
    ! These will be overwritten by those read in simstate.dat
    ! if p%continue is true
    p%fc%a1 = 0;   p%fc%b1 = 0
    p%fc%a2 = 0;   p%fc%b2 = 0

  end subroutine read_parameters


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Print parameters on standard output
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine print_parameters (p)
    use atmosphere
    use boundary
    use diagnostics
    use fexpand
    use initialize
    use iodata
    use iosimstate
    use nonlin
    use propagation
    use types
    use unit
!-
    implicit none
    type(params), intent(in)     :: p

    character (len=*), parameter :: form1 = "(A, I12,    A, I12   )"
    character (len=*), parameter :: form2 = "(A, ES12.5, A, ES12.5)"
    character (len=*), parameter :: form3 = "(A, I12   , A, ES12.5)"
    character (len=*), parameter :: form4 = "(A, I12   )"
    character (len=*), parameter :: form5 = "(A, ES12.5)"

    if (p%mpi%me .eq. p%mpi%root) then
       print*, '*** Loaded modules ***'
       print*, 'atmosphere:  ', module_atmosphere
       print*, 'boundary:    ', module_boundary
       print*, 'diagnostics: ', module_diagnostics
       print*, 'fexpand:     ', module_fexpand
       print*, 'initialize:  ', module_initialize
       print*, 'iodata:      ', module_iodata
       print*, 'ioparams:    ', module_ioparams
       print*, 'iosimstate:  ', module_iosimstate
       print*, 'nonlin:      ', module_nonlin
       print*, 'propagation: ', module_propagation
       print*, 'types:       ', module_types
       print*, 'unit:        ', module_unit
       print*, ''
       print*, '*** Parameters for the model ***'
       print form1, "      N: ", p%nmin,    "         .. ", p%nmax
       print form2, "     k0: ", p%k0,      "    lambda: ", p%lambda
       print form4, "     Nz: ", p%tnz
       print form2, "    eps: ", p%eps,     "      epsm: ", p%epsm
       print form2, "     nu: ", p%nu,      "       eta: ", p%eta
       print form2, "     b0: ", p%b0,      "        dz: ", p%dz
       if (p%tmax .gt. 9.99999e99_dfloat) then
          print form3, "  Steps: ", p%nt,    "  dt_init: ", p%delta_t
       else
          print form2, "   Tmax: ", p%tmax,  "  dt_init: ", p%delta_t
       end if
       if (.not. p%continue) then
          print form2, "initamp: ", p%initamp, " initslope: ", p%initslope
       end if
       print form5, "forcamp: ", p%forcamp
       print form5, "tstar  : ", p%tstar
       print*, ''
       print*, '*** Parameters for parallel run ***'
       print form4, 'Number of processors: ', p%mpi%np
    end if
    call mpi_barrier (mpi_comm_world, ierr)
    print form4, '          I am number ', p%mpi%me
    call mpi_barrier (mpi_comm_world, ierr)
    if (p%mpi%me .eq. p%mpi%root) print*, ''

    call atmosphere_print_parameters(p)

  end subroutine print_parameters




!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Output data in files - Atmosphere profiles
! Should be done only once a the beginning
! MPI: OK
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine output_data_atm (p, outflags)
    use atmosphere
!-
    implicit none
    type(params),         intent(in)                       :: p
    type(saveflags),      intent(in)                       :: outflags

    ! variables used for communication of fields to write
    integer                       :: ip, zlen
    !! no need for save (and problems at compilation?)
    ! integer, save, dimension(0:p%mpi%np-1) :: allnz
    integer, dimension(0:p%mpi%np-1) :: allnz
        integer           :: status(mpi_status_size)
    real(kind=dfloat), allocatable, save, dimension(:) :: vtmp

    call mpi_gather (p%nz, 1, mpi_integer, allnz, 1, mpi_integer, &
         p%mpi%root, mpi_comm_world, ierr)
    if (p%mpi%me .eq. p%mpi%root) then
       allocate (vtmp(0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'

       if (outflags%profva) &
            open (50,  file='out_va',   status='replace', form='formatted')
       if (outflags%profvad) &
            open (51,  file='out_vad',  status='replace', form='formatted')
       if (outflags%profrho) &
            open (52,  file='out_rho',  status='replace', form='formatted')
       if (outflags%rad) &
            open (53,  file='out_rad',  status='replace', form='formatted')
       if (outflags%profaexp) &
            open (54, file='out_aexp', status='replace', form='formatted')
    end if

153 format (ES23.15E3)

!!! Atmosphere profiles

    ! Alfven velocity
    if (outflags%profva) then
       if (p%mpi%me .eq. p%mpi%root) then
          do ip=0, p%mpi%np-1
             if (ip .eq. p%mpi%root) then
                vtmp = va
             else
                zlen = allnz (ip)
                call mpi_recv (vtmp, zlen, mpi_double_precision, &
                     ip, 6, mpi_comm_world, status, ierr)
             end if
             write (50,153) vtmp(0:allnz(ip)-1)
          end do
       else
          zlen = p%nz
          call mpi_send (va, zlen, mpi_double_precision, &
               p%mpi%root,  6, mpi_comm_world, ierr)
       end if
    end if

    ! Alfven velocity gradient
    if (outflags%profvad) then
       if (p%mpi%me .eq. p%mpi%root) then
          do ip=0, p%mpi%np-1
             if (ip .eq. p%mpi%root) then
                vtmp = Vad
             else
                zlen = allnz (ip)
                call mpi_recv (vtmp, zlen, mpi_double_precision, &
                     ip, 7, mpi_comm_world, status, ierr)
             end if
             write (51,153) vtmp(0:allnz(ip)-1)
          end do
       else
          zlen = p%nz
          call mpi_send (vad, zlen, mpi_double_precision, &
               p%mpi%root,  7, mpi_comm_world, ierr)
       end if
    end if

    ! Density
    if (outflags%profrho) then
       if (p%mpi%me .eq. p%mpi%root) then
          do ip=0, p%mpi%np-1
             if (ip .eq. p%mpi%root) then
                vtmp = rho
             else
                zlen = allnz (ip)
                call mpi_recv (vtmp, zlen, mpi_double_precision, &
                     ip, 8, mpi_comm_world, status, ierr)
             end if
             write (52,153) vtmp(0:allnz(ip)-1)
          end do
       else
          zlen = p%nz
          call mpi_send (rho, zlen, mpi_double_precision, &
               p%mpi%root,  8, mpi_comm_world, ierr)
       end if
    end if

    ! distances from the base
    if (outflags%rad) then
       if (p%mpi%me .eq. p%mpi%root) then
          do ip=0, p%mpi%np-1
             if (ip .eq. p%mpi%root) then
                vtmp = r
             else
                zlen = allnz (ip)
                call mpi_recv (vtmp, zlen, mpi_double_precision, &
                     ip, 5, mpi_comm_world, status, ierr)
             end if
             write (53,153) vtmp(0:allnz(ip)-1)
          end do
       else
          zlen = p%nz
          call mpi_send (r, zlen, mpi_double_precision, &
               p%mpi%root,  5, mpi_comm_world, ierr)
       end if
    end if

    ! Expansion factor
    if (outflags%profaexp) then
       if (p%mpi%me .eq. p%mpi%root) then
          do ip=0, p%mpi%np-1
             if (ip .eq. p%mpi%root) then
                vtmp = aexp
             else
                zlen = allnz (ip)
                call mpi_recv (vtmp, zlen, mpi_double_precision, &
                     ip, 9, mpi_comm_world, status, ierr)
             end if
             write (54,153) vtmp(0:allnz(ip)-1) 
          end do
       else
          zlen = p%nz
          call mpi_send (aexp, zlen, mpi_double_precision, &
               p%mpi%root,  9, mpi_comm_world, ierr)
       end if
    end if

    if (p%mpi%me .eq. p%mpi%root) then
       deallocate (vtmp, stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'

       if (outflags%profva)   close (50)
       if (outflags%profvad)  close (51)
       if (outflags%profrho)  close (52)
       if (outflags%rad)      close (53)
       if (outflags%profaexp) close (54)
    end if

  end subroutine output_data_atm

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Get output options
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine read_output_options (p, outflags)
!-
    implicit none
    type(params),    intent(in)  :: p
    type(saveflags), intent(out) :: outflags
    character(len=80)            :: dummy
    integer                      :: tmpint
    real(kind=dfloat)            :: tmpdfl
    if (p%mpi%me .eq. p%mpi%root) then
       open (92, file='param_o.txt', status='old')
       read(92,*) dummy;   read(92,*) tmpdfl
       if (tmpdfl > 0._dfloat) then
          outflags%period  = int (tmpdfl)
          outflags%periodt = 1._dfloat
       else
          outflags%period  = -1
          outflags%periodt = -tmpdfl
       end if
       read(92,*) dummy;   read(92,*) tmpdfl
       if (tmpdfl > 0._dfloat) then
          outflags%specperiod  = int (tmpdfl)
          outflags%specperiodt = 1._dfloat
       else
          outflags%specperiod  = -1
          outflags%specperiodt = -tmpdfl
       end if
       ! by default, output all planes in out_simstate_nnn
       read(92,*) dummy;   read(92,*) outflags%outnz
       if (outflags%outnz .gt. p%tnz .or. outflags%outnz .le. 0) &
            outflags%outnz = p%tnz
       read(92,*) dummy;   read(92,*) outflags%sfunnb
       read(92,*) dummy;   read(92,*) tmpint
       outflags%time     = .TRUE.
       if (tmpint == 0) outflags%time     = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%taunl    = .TRUE.
       if (tmpint == 0) outflags%taunl    = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%enmode02 = .TRUE.
       if (tmpint == 0) outflags%enmode02 = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%enmode08 = .TRUE.
       if (tmpint == 0) outflags%enmode08 = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%enmode14 = .TRUE.
       if (tmpint == 0) outflags%enmode14 = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%enmode20 = .TRUE.
       if (tmpint == 0) outflags%enmode20 = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%entotu   = .TRUE.
       if (tmpint == 0) outflags%entotu   = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%entotb   = .TRUE.
       if (tmpint == 0) outflags%entotb   = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%dissu    = .TRUE.
       if (tmpint == 0) outflags%dissu    = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%dissb    = .TRUE.
       if (tmpint == 0) outflags%dissb    = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%forcp    = .TRUE.
       if (tmpint == 0) outflags%forcp    = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%crosshel = .TRUE.
       if (tmpint == 0) outflags%crosshel = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%maghel   = .TRUE.
       if (tmpint == 0) outflags%maghel   = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%specu    = .TRUE.
       if (tmpint == 0) outflags%specu    = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%specb    = .TRUE.
       if (tmpint == 0) outflags%specb    = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%sfunu    = .TRUE.
       if (tmpint == 0) outflags%sfunu    = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%sfunb    = .TRUE.
       if (tmpint == 0) outflags%sfunb    = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%rawzp    = .TRUE.
       if (tmpint == 0) outflags%rawzp    = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%profheat = .TRUE.
       if (tmpint == 0) outflags%profheat = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%profva    = .TRUE.
       if (tmpint == 0) outflags%profva   = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%profvad   = .TRUE.
       if (tmpint == 0) outflags%profvad  = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%profrho   = .TRUE.
       if (tmpint == 0) outflags%profrho  = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%rad   = .TRUE.
       if (tmpint == 0) outflags%rad  = .FALSE.
       read(92,*) dummy;   read(92,*) tmpint
       outflags%profaexp  = .TRUE.
       if (tmpint == 0) outflags%profaexp = .FALSE.
       close (92)
    end if

    ! broadcast the parameters we have read on root processor
    ! (try not to forget anything... Would be better to define a type for
    ! communicating the parameters, but it is not worth it as this
    ! broadcasting is done only once)
    call mpi_bcast (outflags%period, 1, mpi_integer, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%periodt, 1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%specperiod, 1, mpi_integer, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%specperiodt, 1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%outnz, 1, mpi_integer, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%sfunnb, 1, mpi_integer, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%time, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%taunl, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%enmode02, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%enmode08, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%enmode14, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%enmode20, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%entotu, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%entotb, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%dissu, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%dissb, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%forcp, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%crosshel, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%maghel, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%specu, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%specb, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%sfunu, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%sfunb, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%rawzp, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%profheat, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%profva, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%profvad, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%profrho, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%rad, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_bcast (outflags%profaexp, 1, mpi_logical, &
         p%mpi%root, mpi_comm_world, ierr)

  end subroutine read_output_options




!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Print output options on standard output
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine print_output_options (p, outflags)
!-
    implicit none
    type(params),    intent(in)  :: p
    type(saveflags), intent(in) :: outflags
    character (len=*), parameter :: form1 = "(a, i7, a)"
    character (len=*), parameter :: form2 = "(a, i5, a, i5)"
    character (len=*), parameter :: form3 = "(a, i3, a, i9)"
    character (len=*), parameter :: form4 = "(a, es12.5e2, a)"

    if (p%mpi%me .eq. p%mpi%root) then
       print*, '*** Output options ***'
       if (outflags%period > 0) then
          print form1, " Output every ", outflags%period,  " timesteps"
       else
          print form4, " Output every ", outflags%periodt, " "
       end if
       if (outflags%specperiod > 0) then
          print form1, "    and every ", outflags%specperiod,  &
               " timesteps (spectra, fields...)"
       else
          print form4, "    and every ", outflags%specperiodt, &
               " (spectra, fields...)"
       end if
       if (outflags%time)        print*, "Time"
       if (outflags%taunl)       print*, "TauNL"
       if (outflags%enmode02 .or. outflags%enmode08 &
            .or. outflags%enmode14 .or. outflags%enmode20) then
          print*, "Energy in modes:"
          if (outflags%enmode02) print*, "  02"
          if (outflags%enmode08) print*, "  08"
          if (outflags%enmode14) print*, "  14"
          if (outflags%enmode20) print*, "  20"
       end if
       if (outflags%entotu)      print*, "Total kinetic energy"
       if (outflags%entotb)      print*, "Total magnetic energy"
       if (outflags%dissu)       print*, "Kinetic energy dissipation"
       if (outflags%dissb)       print*, "Magnetic energy dissipation"
       if (outflags%forcp)       print*, "Power of forcing"
       if (outflags%crosshel)    print*, "Cross-helicity"
       if (outflags%maghel)      print*, "Magnetic helicity"
       if (outflags%specu)       print*, &
            "Kinetic  energy averaged perpendicular spectrum"
       if (outflags%specb)       print*, &
            "Magnetic energy averaged perpendicular spectrum"
       if (outflags%sfunu .or. outflags%sfunb) then
          print form3, " Structure functions (", outflags%sfunnb, ")"
          if (outflags%sfunu)    print*, "  Velocity field"
          if (outflags%sfunb)    print*, "  Magnetic field"
       end if
       if (outflags%rawzp)       print*, &
            "Raw zp and zm fields (simulation states)"
       if (outflags%profheat)    print*, "Heating profile"
       if (outflags%profva)      print*, "Va profile"
       if (outflags%profvad)     print*, "Va derivative profile"
       if (outflags%profrho)     print*, "Rho profile"
       if (outflags%rad)         print*, "distance from sun surface"
       if (outflags%profaexp)    print*, "Flux tube expansion"
       print*, ''
    end if
  end subroutine print_output_options

end module ioparams
