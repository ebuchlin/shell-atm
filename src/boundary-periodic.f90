!+
!***********************************************************
! Module boundary-periodic
!***********************************************************
! Variables and routines for boundary conditions and forcing
! For periodic boundary conditions
!
! Velocity forcing is imposed in the volume of the box.
! It is the same everywhere in the box seen by one processor,
! but it differs from one processor to the next!
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  04 Oct 2012 EB Created (forked from boundary-photospheres)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module boundary
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_boundary = "periodic"

contains

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Periodic boundary conditions: do nothing at boundaries,
! but force in volume.
! (periodicity will be implemented in propagation scheme)
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine force (zp, zm, k, p, t)
!-
    use mpi
    use atmosphere
    use diagnostics
    implicit none
    type(params),         intent(inout)                           :: p
    complex(kind=dfloat), intent(inout), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                     :: zp, zm
    complex(kind=dfloat), intent(in),    dimension(p%nmin:p%nmax) :: k
    real(kind=dfloat), intent(in) :: t

    ! forcing fields
    complex(kind=dfloat), dimension(:), save, allocatable :: dzp0, dzm0, dzp, dzm

    ! (temporary) coefficients of forcing velocity for the nkforce forced modes
    real(kind=dfloat), dimension(0:nkforce-1)               :: tmp1
    real(kind=dfloat)                             :: tmp
    ! relative position in flux tube (set to 0 if p%tnz=1 (monoshell))
    real(kind=dfloat)                             :: rs
    ! are these saved arrays already allocated?
    logical, save :: isallocated = .FALSE.
    integer :: iz
    ! MPI variables
    integer           :: status(mpi_status_size)

    ! p%tstar is the time after which the forcing should be changed
    ! In this version, zm and zp are forced independently (v + B forcing)

    if (.NOT. isallocated) then
      allocate (dzp(0:nkforce-1), stat=aerr)
      if (aerr .ne. 0) stop 'Allocation error'
      allocate (dzm(0:nkforce-1), stat=aerr)
      if (aerr .ne. 0) stop 'Allocation error'
      allocate (dzp0(0:nkforce-1), stat=aerr)
      if (aerr .ne. 0) stop 'Allocation error'
      allocate (dzm0(0:nkforce-1), stat=aerr)
      if (aerr .ne. 0) stop 'Allocation error'
      isallocated = .TRUE.
    end if

    if (modulo (t, p%tstar) .lt. p%delta_t) then
       if (p%mpi%me .eq. p%mpi%root) then
          call random_number (harvest=tmp1)
          p%fc%a1 = exp (cmplx (0._dfloat, tmp1 * pi * 2._dfloat))
          call random_number (harvest=tmp1)
          p%fc%a2 = exp (cmplx (0._dfloat, tmp1 * pi * 2._dfloat))
       end if
       call mpi_bcast (p%fc%a1, nkforce, mpi_double_complex, p%mpi%root, mpi_comm_world, ierr)
       call mpi_bcast (p%fc%a2, nkforce, mpi_double_complex, p%mpi%root, mpi_comm_world, ierr)
    end if

    ! apply forcing (only in forcing modes) and compute power of forcing
    dzp0 = p%forcamp * pi / p%tstar * sin (2._dfloat * pi * t / p%tstar)
    dzm0 = dzp0 * p%fc%a2
    dzp0 = dzp0 * p%fc%a1

    tmp = 0._dfloat   ! will be p%forcp after mpi_reduce
    do iz=0, p%nz - 1
      if (p%tnz .eq. 0) then
         rs = 0._dfloat
      else
         rs = r(iz) / (p%tnz * p%dz)
      end if

      dzp = dzp0 * exp (cmplx (0._dfloat, 2._dfloat * pi * rs)) * p%delta_t
      dzm = dzm0 * exp (cmplx (0._dfloat, 2._dfloat * pi * rs)) * p%delta_t
      ! (quadruple) energy in forcing modes: before when forcing is applied
      tmp = tmp - real (dot_product (zp(0:nkforce-1, iz), zp(0:nkforce-1, iz)), kind=dfloat)
      tmp = tmp - real (dot_product (zm(0:nkforce-1, iz), zm(0:nkforce-1, iz)), kind=dfloat)
      ! apply forcing
      zp(0:nkforce-1, iz) = zp(0:nkforce-1, iz) + dzp
      zm(0:nkforce-1, iz) = zm(0:nkforce-1, iz) + dzm
      ! (quadruple) energy in forcing modes: after when forcing is applied
      tmp = tmp + real (dot_product (zp(0:nkforce-1, iz), zp(0:nkforce-1, iz)), kind=dfloat)
      tmp = tmp + real (dot_product (zm(0:nkforce-1, iz), zm(0:nkforce-1, iz)), kind=dfloat)
    end do
    tmp = tmp * rho(0) * aexp(0) * p%dz / 4._dfloat / p%delta_t ! assumes unifom rho, aexp, and grid cell size
    tmp = tmp * pi ** 3 / p%k0 ** 2

    ! compute sum and send it to root
    call mpi_reduce (tmp, p%forcp, 1, mpi_double_precision, MPI_SUM, p%mpi%root, mpi_comm_world, ierr)

  end subroutine force

end module boundary
