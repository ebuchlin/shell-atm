!+
!***********************************************************
! Module propagation-periodicfromm
!***********************************************************
! Variables and routines for Alfven wave propagation
! With Fromm numerical scheme everywhere, assuming periodic
! boundary conditions
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  03 Oct 12 EB Created (forked from propagation-fromm)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module propagation
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_propagation = "periodicfromm"

contains

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Alfven wave propagation, Fromm numerical scheme everywhere,
! with periodic boundary conditions
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine propagate (zp, zm, k, p, t)
    use mpi
    use atmosphere
!-
    implicit none
    type(params),         intent(inout)                         :: p
    complex(kind=dfloat), intent(inout), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                     :: zp, zm
    ! not used
    complex(kind=dfloat), intent(in),  dimension(p%nmin:p%nmax) :: k
    ! not used
    real(kind=dfloat) :: t

    ! old values in whole box
    complex(kind=dfloat), dimension(:,:), save, allocatable :: ozp, ozm
    ! Extended arrays for Alfven velocities
    real(kind=dfloat), dimension(:), save, allocatable      :: eva, eva_m

    ! are the local fields already allocated?
    ! Is it the first time this procedure is called?
    logical, save :: isallocated = .FALSE., firsttime = .TRUE.
    integer :: iz
    real(kind=dfloat) :: lam
    ! MPI variables
    integer           :: status(mpi_status_size)
    integer           :: zlen
    integer, save     :: left, right  

    if (.NOT. isallocated) then
       ! Old fields, with needed extension
       allocate (ozp(p%nmin:p%nmax, -2:p%nz  ), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (ozm(p%nmin:p%nmax, -1:p%nz+1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'

       ! Get extended arrays for Alfven velocities
       allocate (eva  (-2:p%nz+1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (eva_m(-2:p%nz+1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       eva  (0:p%nz-1) = va
       eva_m(0:p%nz-1) = va_m
       if (p%mpi%me .eq. 0) then
          left = p%mpi%np-1  ! this implements the periodic boundary conditions if using MPI
       else
          left = p%mpi%me - 1
       end if
       if (p%mpi%me .eq. p%mpi%np-1) then
          right = 0          ! this implements the periodic boundary conditions if using MPI
       else
          right = p%mpi%me + 1
       end if

       ! send to right (and receive from left)
       call mpi_sendrecv(eva  (p%nz-2), 2, mpi_double_precision, right, 16, &
            eva  (-2)    , 2, mpi_double_precision, left , 16, &
            mpi_comm_world, status, ierr)
       call mpi_sendrecv(eva_m(p%nz-2), 2, mpi_double_precision, right, 16, &
            eva_m(-2)    , 2, mpi_double_precision, left , 16, &
            mpi_comm_world, status, ierr)
       ! send to left (and receive from right)
       call mpi_sendrecv(eva  (0)     , 2, mpi_double_precision, left , 17, &
            eva  (p%nz)  , 2, mpi_double_precision, right, 17, &
            mpi_comm_world, status, ierr)
       call mpi_sendrecv(eva_m(0)     , 2, mpi_double_precision, left , 17, &
            eva_m(p%nz)  , 2, mpi_double_precision, right, 17, &
            mpi_comm_world, status, ierr)

       isallocated = .TRUE.
    end if

    ! normalization by unorm
    do iz = 0, p%nz-1
       zp(:,iz) = zp(:,iz) / unorm(iz)
       zm(:,iz) = zm(:,iz) / unorm(iz)
    end do

!!! propagation by Fromm numerical scheme everywhere
    if (firsttime .and. p%mpi%me .eq. p%mpi%root)  then
       print*, 'Using Fromm numerical scheme for propagation'
    end if
    !! Need to save old values of fields (inefficient?)
    ozp(:,0:p%nz-1) = zp
    ozm(:,0:p%nz-1) = zm

    ! in same direction as wave: transmit 2 grid cells
    zlen = (p%nmax - p%nmin + 1) * 2
    ! send ozp to the right and receive it from the left
    call mpi_sendrecv (ozp(0,p%nz-2), zlen, mpi_double_complex, right, 18, &
         ozp(0,    -2), zlen, mpi_double_complex, left,  18, &
         mpi_comm_world, status, ierr)
    ! send ozm to the left and receive it from the right
    call mpi_sendrecv (ozm(0,0),      zlen, mpi_double_complex, left,  19, &
         ozm(0,p%nz),   zlen, mpi_double_complex, right, 19, &
         mpi_comm_world, status, ierr)
    ! in opposite direction as wave: transmit 1 grid cell
    zlen = (p%nmax - p%nmin + 1)
    ! send ozp to the left and receive it from the right
    call mpi_sendrecv (ozp(0,0),      zlen, mpi_double_complex, left,  20, &
         ozp(0,p%nz),   zlen, mpi_double_complex, right, 20, &
         mpi_comm_world, status, ierr)
    ! send ozm to the right and receive it from the left
    call mpi_sendrecv (ozm(0,p%nz-1), zlen, mpi_double_complex, right, 21, &
         ozm(0,    -1), zlen, mpi_double_complex, left,  21, &
         mpi_comm_world, status, ierr)

    lam  = p%delta_t / p%dz

    !! update zp with the Fromm numerical scheme
    do iz=0, p%nz - 1
       zp(:,iz) = ozp(:,iz) * (1._dfloat - lam * eva_m(iz-1) + &
            .25_dfloat * lam * eva_m(iz) * &
            (1._dfloat - lam * eva_m(iz))) &
            + ozp(:,iz-1) * lam * (eva_m(iz-1) + &
            .25_dfloat * eva_m(iz-2) * &
            (1._dfloat - lam * eva_m(iz-2))) &
            - ozp(:,iz+1) * .25_dfloat * lam * eva_m(iz) * &
            (1._dfloat - lam * eva_m(iz)) &
            - ozp(:,iz-2) * .25_dfloat * lam * eva_m(iz-2) * &
            (1._dfloat - lam * eva_m(iz-2)) &
            - .5_dfloat * vad(iz) * ozm(:,iz) * p%delta_t !reflection
    end do

    !! update zm with the Fromm numerical scheme
    do iz=0, p%nz - 1
       zm(:,iz) = ozm(:,iz) * (1._dfloat - lam * eva_m(iz) + &
            .25_dfloat * lam * eva_m(iz-1) * &
            (1._dfloat - lam * eva_m (iz-1))) &
            + ozm(:,iz+1) * lam * (eva_m(iz) + &
            .25_dfloat * eva_m(iz+1) * &
            (1._dfloat - lam * eva_m(iz+1))) &
            - ozm(:,iz-1) * .25_dfloat * lam * eva_m(iz-1) * &
            (1._dfloat - lam * eva_m(iz-1)) &
            - ozm(:,iz+2) * .25_dfloat * lam * eva_m(iz+1) * &
            (1._dfloat - lam * eva_m(iz+1)) &
            + .5_dfloat * vad(iz) * ozp(:,iz) * p%delta_t !reflection
    end do

    ! de-normalization
    do iz = 0, p%nz-1
       zp(:,iz) = zp(:,iz) * unorm(iz)
       zm(:,iz) = zm(:,iz) * unorm(iz)
    end do

    firsttime = .FALSE.

  end subroutine propagate

end module propagation
