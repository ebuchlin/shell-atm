!+
!***********************************************************
! Module types
!***********************************************************
! Definition of user-defined types
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  15 Jul 05: AV/EB: created
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!-
module types
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_types = "default"

  ! "Double precision" numbers precision
  integer, parameter :: dfloat = selected_real_kind(15,300)
  real(kind=dfloat), parameter :: pi = 3.1415926535897932_dfloat

  ! Additional parameters (not in params.txt)
  integer, parameter :: nkforce = 2   ! number of forcing wavenumbers
  integer, parameter :: visc = 2      ! viscous term exponent (must be even)
  real(kind=dfloat), parameter :: biskampa = 0.3162278_dfloat ! perpendicular Alfven term coefficient

  ! Parallel routine calls error code
  integer :: ierr
  ! Allocation error code
  integer :: aerr

!+
  ! Random coefficients of forcing
  type forcecoeffs
!-
     ! Both components on first boundary
      complex(kind=dfloat), dimension(0:nkforce-1) :: a1, b1
      !! allocatable members of types are not standard Fortran
!      complex(kind=dfloat), dimension(:), allocatable :: a1, b1
     ! Both components on second boundary
      complex(kind=dfloat), dimension(0:nkforce-1) :: a2, b2
      !! allocatable members of types are not standard Fortran
!      complex(kind=dfloat), dimension(:), allocatable :: a2, b2
   end type forcecoeffs

!+
  ! Parameters for the parallel run
  type mpiparams
!-
     ! processor rank
     integer :: me
     ! size (number of processors)
     integer :: np
     ! left and right processor of the current processor (for communications)
     integer :: left, right
     ! communicators
!     integer :: comm_grid
     ! root processor
     integer :: root
  end type mpiparams


!+
  ! Parameters for the expansion function 
  type paramexp
!-
     ! supraspherical expansion
     real(kind=dfloat) :: x_0
     real(kind=dfloat) :: sigma
     real(kind=dfloat) :: fmax 
     real(kind=dfloat) :: x1n 
  end type paramexp


!+
  ! Parameters of simulation (including parameters of the parallel run)
  ! and for the expansion function
  type params
!-
     !    logarithmic discretization of k_n: k_n = k0 * lambda ^ n,
     !      n = nmin..nmax
     integer           :: nmin, nmax
     !    Total number of planes
     integer           :: tnz
     !    Number of planes (for this processor)
     integer           :: nz
     !    Index of plane 0 of this processor in the whole simulation box
     !    (goes from 0 to tnz-1)
     integer           :: nz0
     ! Indices of extremes planes (for this processor), taking into
     ! account the planes from the neighboring processors that are needed for
     ! the numerical schemes
     !integer           :: iz1, iz2
     real(kind=dfloat) :: k0
     real(kind=dfloat) :: lambda
     !    Model viscosity and magnetic diffusivity
     real(kind=dfloat) :: nu, eta
     !    Model "eps" and "epsm" parameter
     real(kind=dfloat) :: eps, epsm
     !    Magnetic field at the base (Gauss)
     real(kind=dfloat) :: b0
     !    Density at the base        (cm-3)
     real(kind=dfloat) :: rho0
     !    Beta plasma at base
     !real(kind=dfloat) :: beta0
     !    Distance between planes
     real(kind=dfloat) :: dz
     !    Number of time steps
     integer           :: nt
     !    Maximum value of time
     real(kind=dfloat) :: tmax
     !    Initial time step (then current time step)
     real(kind=dfloat) :: delta_t
     !    Continue from saved state ?
     logical           :: continue
     ! Both next ones are not relevant if we continue an existing simulation
     !    Initial field amplitude
     real(kind=dfloat) :: initamp
     !    Initial field spectrum power-law index
     real(kind=dfloat) :: initslope
     !    Forcing amplitude
     real(kind=dfloat) :: forcamp
     !    Correlation time of forcing
     real(kind=dfloat) :: tstar
     ! h scale height
     real(kind=dfloat) :: h
     ! Parameters for the expansion function
     type(paramexp) :: pexp

     !!! some additional variables that are not really parameters
     ! non linear timescale (now defined in type "timescales"!!)
     !real(kind=dfloat) :: taunl
     ! Power of forcing at boundaries
     real(kind=dfloat) :: forcp
     ! Parameters for the parallel run
     type(mpiparams) :: mpi
     ! Random coefficients of forcing
     type(forcecoeffs) :: fc
  end type params

!+
  ! What do we output, and at which frequency
  type saveflags
!-
     ! Number of timesteps between each output of integrated quantities
     integer :: period
     ! or: Time between each output of integrated quantities
     real(kind=dfloat) :: periodt
     ! Number of timesteps between each output of non-integrated quantities
     integer :: specperiod
     ! or: Time between each output of non-integrated quantities
     real(kind=dfloat) :: specperiodt
     ! Number of planes to output in out_simstate_nnn
     integer :: outnz
     ! Time (filehandle 10)
     logical :: time
     ! Non-linear time scale (filehandle 101; done in check_time_scales)
     logical :: taunl
     ! Energy (kinetic + magnetic) in mode 02 (filehandle 11)
     logical :: enmode02
     ! Energy (kinetic + magnetic) in mode 08 (filehandle 12)
     logical :: enmode08
     ! Energy (kinetic + magnetic) in mode 14 (filehandle 13)
     logical :: enmode14
     ! Energy (kinetic + magnetic) in mode 20 (filehandle 14)
     logical :: enmode20
     ! Total kinetic energy (filehandle 20)
     logical :: entotu
     ! Total magnetic energy (filehandle 21)
     logical :: entotb
     ! Kinetic energy dissipation (filehandle 22)
     logical :: dissu
     ! Magnetic energy dissipation (filehandle 23)
     logical :: dissb
     ! Power of the forcing at boundaries (filehandle 24)
     logical :: forcp
     ! Cross helicity (filehandle 26)
     logical :: crosshel
     ! "Magnetic helicity" (filehandle 27)
     logical :: maghel
     !   Kinetic energy spectrum (filehandle 30)
     logical :: specu
     !   Magnetic energy spectrum (filehandle 31)
     logical :: specb
     !   Number of field structure functions (outputed at end of simulation)
     integer :: sfunnb
     !   Velocity field structure function (filehandle 35)
     logical :: sfunu
     !   Magnetic field structure function (filehandle 36)
     logical :: sfunb
     !   Raw zp field (filehandle 40)
     logical :: rawzp
     !   Raw zm field (not used, was filehandle 41)
     logical :: rawzm

     ! Radius (filehandle 53)
     logical :: rad
     !   Alfven speed profile (filehandle 50)
     logical :: profVa
     !   Alfven speed gradient profile (filehandle 51)
      logical :: profVad
     !   Mass density profile (filehandle 52)
     logical :: profRho
     !   Expansion factor (filehandle 49)
     logical :: profaexp
     !   Average profile of heating per unit lenght (unit 55)
     logical :: profheat
     !!! these are now computed by IDL from out_simstate_*
     !   Wave amplitude (Zp and Zm) profile (filehandle 60, 61)
     !logical :: amp 
     !   Field amplitude (U and B) profile (filehandle 65, 66)
     !logical :: famp 
     !   Cross Helicity (filehandle 54)
     !logical :: crosshelP
     !   Magnetic Helicity (filehandle 55)
     !logical :: maghelP
     !   Energy flux profile (Zp and Zm and total) (filehandle 62, 63, 64)
     !logical :: Flux
     !   Energy per unit mass for Zp and Zm (filehandle 75, 76)
     !logical :: ElsEn
     !   Energy per unit mass for u and b (filehandle 77, 78, 79)
     !logical :: EnUB
     !   Transmission coefficient (filehandle 80)
     ! logical :: Tras
     ! total energy inside the domain (filehandle 74)
     !logical :: Edom
  end type saveflags

!+
  ! Time scales as computed by the last call of check_time_scales  
  type timescales
!-
     ! Minimum non-linear time scale
     real(kind=dfloat) :: taunl
     ! Minimum Alfven time scale
     real(kind=dfloat) :: taual
     ! Minimum dissipation (perpendicular) time scale
     real(kind=dfloat) :: taunu
     ! Crossing time scale
     real(kind=dfloat) :: taucr
  end type timescales

!+
  ! Number of records saved for each type of output
  type saverecords
!-
     integer :: energy
     integer :: sfun
     integer :: spec
  end type saverecords

!+
  ! Units (CGS) of variables
  type units
!-
     real(kind=dfloat) :: length
     real(kind=dfloat) :: time
     real(kind=dfloat) :: mass
  end type units

end module types
