!+
!***********************************************************
! Module propagation-none
!***********************************************************
! Variables and routines for no Alfv�n wave propagation
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  24 Nov 14 EB Created (forked from propagation-periodicfromm)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module propagation
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_propagation = "none"

contains

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Alfv�n wave propagation, Fromm numerical scheme everywhere,
! with periodic boundary conditions
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine propagate (zp, zm, k, p, t)
    use usempi
!-
    implicit none
    type(params),         intent(inout)                         :: p
    complex(kind=dfloat), intent(inout), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                     :: zp, zm
    ! not used
    complex(kind=dfloat), intent(in),  dimension(p%nmin:p%nmax) :: k
    ! not used
    real(kind=dfloat) :: t

  end subroutine propagate

end module propagation
