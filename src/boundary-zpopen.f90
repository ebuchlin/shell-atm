!+
!***********************************************************
! Module boundary-zpopen
!***********************************************************
! Variables and routines for boundary conditions and forcing
! For imposed zp on one side and open boundary condition the other side
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  07 Nov 05 EB Created
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module boundary
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_boundary = "zpopen"

contains


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Boundary conditions (reflection and forcing)
! Imposed zp, open at top
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine force (zp, zm, k, p, t)
    use mpi
    use atmosphere
    use diagnostics
!-
    implicit none
    type(params),         intent(inout)                           :: p
    complex(kind=dfloat), intent(inout), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                     :: zp, zm
    complex(kind=dfloat), intent(in),    dimension(p%nmin:p%nmax) :: k
    real(kind=dfloat), intent(in) :: t

    real(kind=dfloat)                             :: tmp
    ! MPI variables
    integer           :: status(mpi_status_size)

    if (p%mpi%me .eq. 0 .or. p%mpi%me .eq. p%mpi%np-1) then

       ! Apply forcing by imposing the incoming field at boundaries
       ! and compute power of forcing (incoming - outgoing flux)
       if (p%mpi%me .eq. 0) then
          ! imposed z+ field
          !zp(2:2+nkforce-1,0) = cmplx(cos(2._dfloat * pi*t/p%tstar), &
          !     sin(2._dfloat * pi*t/p%tstar)) * p%forcamp
          zp(2:2+nkforce-1,0) = cmplx(sin(2._dfloat * pi*t/p%tstar), &
               0._dfloat) * p%forcamp
          p%forcp = energy_tot1d (zp(:,0), p) - &
               energy_tot1d (zm(:,0), p)
          p%forcp = p%forcp * rho(0) * va(0) * aexp(0)
          if (p%mpi%root .ne. 0) then
             call mpi_send (p%forcp, 1, mpi_double_precision, &
                  p%mpi%root, 10, mpi_comm_world, ierr)
          end if
       end if
       if (p%mpi%me .eq. p%mpi%np-1) then
          ! open boundary
          zm(:, p%nz-1) = 0
          tmp = - energy_tot1d (zp(:,p%nz-1), p)
          tmp = tmp * rho(p%nz-1) * va(p%nz-1) * aexp(p%nz-1)
          if (p%mpi%root .ne. p%mpi%np-1) then
             call mpi_send (tmp, 1, mpi_double_precision, &
                  p%mpi%root, 11, mpi_comm_world, ierr)
          end if
       end if
    end if
    if (p%mpi%me .eq. p%mpi%root) then ! does not assume that root is 0
       if (p%mpi%root .ne. 0) then
          call mpi_recv (p%forcp, 1, mpi_double_precision, &
               0,          10, mpi_comm_world, status, ierr)
       end if
       if (p%mpi%root .ne. p%mpi%np-1) then
          call mpi_recv (tmp,     1, mpi_double_precision, &
               p%mpi%np-1, 11, mpi_comm_world, status, ierr)
       end if
       p%forcp = (p%forcp + tmp) * pi ** 3 / p%k0 ** 2
    end if

  end subroutine force
  
end module boundary
