#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

int unlimitstack_ (void)
{
  const struct rlimit rlim = {.rlim_cur = RLIM_INFINITY,
			      .rlim_max = RLIM_INFINITY};
  return (setrlimit (RLIMIT_STACK, &rlim));
}

