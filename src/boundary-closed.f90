!+
!***********************************************************
! Module boundary-closed
!***********************************************************
! Variables and routines for boundary conditions and forcing
! For a closed box (total reflection on both boundaries)
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  10 Nov 05 EB Created
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module boundary
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_boundary = "closed"

contains


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Boundary conditions (reflection and forcing)
! Total reflection
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine force (zp, zm, k, p, t)
    use mpi
!-
    implicit none
    type(params),         intent(inout)                           :: p
    complex(kind=dfloat), intent(inout), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                       :: zp, zm
    complex(kind=dfloat), intent(in),    dimension(p%nmin:p%nmax) :: k
    real(kind=dfloat), intent(in) :: t

    ! boundary conditions on velocity (forcing)
    complex(kind=dfloat), dimension(:), save, allocatable :: u1, u2

    if (p%mpi%me .eq. 0 .or. p%mpi%me .eq. p%mpi%np-1) then
       ! Do total reflection and compute power of forcing
       if (p%mpi%me .eq. 0) then
          zp(:, 0)      = -zm(:, 0)
       end if
       if (p%mpi%me .eq. p%mpi%np-1) then
          zm(:, p%nz-1) = -zp(:, p%nz-1)
       end if
    end if
    p%forcp = 0._dfloat
    
  end subroutine force

end module boundary
