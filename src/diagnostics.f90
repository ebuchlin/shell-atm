!+
!***********************************************************
! Module diagnostics
!***********************************************************
! Computation of diagnostics from fields, integrated quantities, conversions
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  22 Oct 05: EB Created (moved from shell-atm)
!  05 Nov 05: EB Take density into account
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module diagnostics
  use mpi
  use types
  use atmosphere
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_diagnostics = "default"

contains

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Return the total energy (massic density) contained in shells
! of a loop cross-section
! As we don't know the density in this section or its position,
! we can't take it into account right here
! MPI: OK (no need)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function energy_tot1d (z, p)
!-
    implicit none
    type   (params),      intent(in)                  :: p
    real   (kind=dfloat)                              :: energy_tot1d
    complex(kind=dfloat), intent(in), dimension(:)    :: z

    energy_tot1d = real (dot_product (z, z), kind=dfloat) * 0.25_dfloat

  end function energy_tot1d


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Return the total energy contained in one shell on all planes
! Take into acount mass density, and separation between planes
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function energy_tot1dk (z, p)
!-
    implicit none
    type   (params),      intent(in)                  :: p
    real   (kind=dfloat)                              :: energy_tot1dk
    complex(kind=dfloat), intent(in), dimension(0:p%nz-1) :: z
    real   (kind=dfloat)                              :: tmp

    tmp = real (dot_product (z, z * rho * aexp), kind=dfloat) * 0.25_dfloat
    call mpi_reduce (tmp, energy_tot1dk, 1, mpi_double_precision, &
         mpi_sum, p%mpi%root, mpi_comm_world, ierr)

    ! convert this volumic energy to an energy
    energy_tot1dk = energy_tot1dk * p%dz * pi ** 3 / p%k0 ** 2
  end function energy_tot1dk


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Return the total energy contained in shells of a field (whole
! box, all processors)
! Take into acount mass density, and separation between planes
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function energy_tot (z, p)
!-
    implicit none
    type   (params),      intent(in)                   :: p
    real   (kind=dfloat)                               :: energy_tot, tmpe
    complex(kind=dfloat), intent(in), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)          :: z

    integer :: i

    ! compute total energy contained in shells of a field
    ! (all planes in one processor)
    tmpe = 0._dfloat
    do i = 0, p%nz - 1
       ! take into account effect of boundary conditions (reflection):
       ! the boundary planes should be counted half
        if (((i .eq. 0     ) .and. (p%mpi%me .eq. 0         )) .or. &
            &((i .eq. p%nz-1) .and. (p%mpi%me .eq. p%mpi%np-1))) then
          tmpe = tmpe + energy_tot1d (z(:, i), p) * rho(i) * .5_dfloat &
                        * aexp(i)
       else
          tmpe = tmpe + energy_tot1d (z(:, i), p) * rho(i) &
                        * aexp(i)
       end if
    end do

    ! mpi_allreduce is overkill if we need to know the total energy only
    ! in the root processor, but we don't know if it is the case
    ! (unless we write another procedure for this case)
    call mpi_allreduce (tmpe, energy_tot, 1, mpi_double_precision, &
         mpi_sum, mpi_comm_world, ierr)
    
    ! convert this volumic energy to an energy
    energy_tot = energy_tot * p%dz * pi ** 3 / p%k0 ** 2
  end function energy_tot


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Return the total energy contained in shells of both fields of a
!   loop section
! As we don't know the density in this section or its position,
!   we can't take it into account right here
! MPI: OK (no need; however, when used [out of specifications, and deprecated]
!          to compute enmodexx, needs to be considered as not parallel)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function energy_2_tot1d (z1, z2, p)
!-
    implicit none
    type   (params),      intent(in)                  :: p
    real   (kind=dfloat)                              :: energy_2_tot1d
    complex(kind=dfloat), intent(in), dimension(:)    :: z1, z2

    energy_2_tot1d = energy_tot1d (z1, p) + energy_tot1d (z2, p)

  end function energy_2_tot1d


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Return the total energy contained in one shell of both fields in
!   all planes
! Take into acount mass density, and separation between planes
!   (inherited from energy_tot1d)
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function energy_2_tot1dk (z1, z2, p)
!-
    implicit none
    type   (params),      intent(in)                  :: p
    real   (kind=dfloat)                              :: energy_2_tot1dk
    complex(kind=dfloat), intent(in), dimension(:)    :: z1, z2

    energy_2_tot1dk = energy_tot1dk (z1, p) + energy_tot1dk (z2, p)

  end function energy_2_tot1dk


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Return the total energy contained in shells of both fields
!   (whole box, all processors)
! Take into acount mass density, and separation between planes
!   (inherited from energy_tot)
! MPI: OK (inherited from energy_tot)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function energy_2_tot (z1, z2, p)
!-
    implicit none
    type   (params),      intent(in)                    :: p
    real   (kind=dfloat)                                :: energy_2_tot
    complex(kind=dfloat), intent(in), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)           :: z1, z2

    energy_2_tot = energy_tot (z1, p) + energy_tot (z2, p)

  end function energy_2_tot

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Return the energy corresponding to a shell value
! As we don't know the density in this section or its position,
!   we can't take it into account right here
! MPI: OK (no need)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function energy (zval)
!-
    implicit none
    real   (kind=dfloat)             :: energy
    complex(kind=dfloat), intent(in) :: zval
    energy = real (zval * conjg (zval), kind=dfloat) / 4._dfloat

  end function energy


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Return the cross-helicity H_C (whole box, all processors)
! Take into acount mass density, and separation between planes
!   (inherited from energy_tot)
! MPI: OK (inherited from energy_tot)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function cross_helicity (zp, zm, p)
!-
    implicit none
    type   (params),      intent(in)                   :: p
    real   (kind=dfloat)                               :: cross_helicity
    complex(kind=dfloat), intent(in), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)          :: zp, zm

    cross_helicity = energy_tot (zp, p) - energy_tot (zm, p)

  end function cross_helicity


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Return third invariant (=magnetic helicity in 3D)
!   of a loop cross-section
! As we don't know the density in this section or its position,
!   we can't take it into account right here
! *** in development ***
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function magn_helicity1d (zp, zm, k, p)
!-
    implicit none
    type   (params),      intent(in)                       :: p
    complex(kind=dfloat), intent(in), dimension(p%nmin:p%nmax) :: zp, zm
    complex(kind=dfloat), intent(in), dimension(p%nmin:p%nmax) :: k
    real   (kind=dfloat)                                   :: magn_helicity1d

    integer                 :: i
    real   (kind=dfloat)    :: s, alpha
    logical, save           :: firsttime = .TRUE.
    ! alpha is set here, but it should correspond to the values
    ! of eps and epsm
    alpha = 2

    if (firsttime) then
       print*, 'Warning: call of magn_helicity, which is in development'
       print*, '   (only first call will be reported)'
       firsttime = .FALSE.
    end if

    if (p%eps > 1._dfloat) then
       s = 1._dfloat
    else if (p%eps < 1._dfloat) then
       s = -1._dfloat
    else
       s = 0._dfloat
    end if
    magn_helicity1d = real ( &
         sum (((zp - zm) * conjg (zp - zm)) / k ** alpha), &
         kind=dfloat) / 4._dfloat

  end function magn_helicity1d


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Return third invariant (=magnetic helicity in 3D)
! Take into acount mass density, and separation between planes
! *** in development ***
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function magn_helicity (zp, zm, k, p)
!-
    implicit none
    type   (params),      intent(in)          :: p
    complex(kind=dfloat), intent(in), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)   :: zp, zm
    complex(kind=dfloat), intent(in), dimension(p%nmin:p%nmax) :: k
    real   (kind=dfloat)                      :: magn_helicity

    integer                 :: iz
    real   (kind=dfloat)    :: s, alpha
    logical, save           :: firsttime = .TRUE.
    ! alpha is set here, but it should correspond to the values
    ! of eps and epsm
    alpha = 2

    if (firsttime) then
       print*, 'Warning: call of magn_helicity, which is in development'
       print*, '  and not parallelized (only first call will be reported)'
       firsttime = .FALSE.
    end if

    if (p%eps > 1._dfloat) then
       s = 1._dfloat
    else if (p%eps < 1._dfloat) then
       s = -1._dfloat
    else
       s = 0._dfloat
    end if

    magn_helicity = 0._dfloat
    do iz = 0, p%nz - 1
       magn_helicity = magn_helicity + real ( &
            sum (((zp(:,iz) - zm(:,iz)) * conjg (zp(:,iz) - zm(:,iz))) &
            / k ** alpha), kind=dfloat) / 4._dfloat * rho(iz)
    end do
    ! see in energy_tot remarks about the same operation
    magn_helicity = magn_helicity * p%dz
  end function magn_helicity



!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Return the total dissipation for a field u or b (whole
!   box, all processors)
! Argument field is u or b
! Take into acount mass density, and separation between planes
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  function diss_tot (z, k, p, disscoeff)
!-
    implicit none
    type   (params),      intent(in)           :: p
    complex(kind=dfloat), intent(in), dimension(p%nmin:p%nmax) :: k
    real   (kind=dfloat), intent(in)           :: disscoeff

    real   (kind=dfloat)                       :: diss_tot, tmpd
    complex(kind=dfloat), intent(in), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)    :: z

    integer :: i

    complex(kind=dfloat), dimension(p%nmin:p%nmax) :: vk
    vk = k ** (visc / 2)  ! visc must be even

    ! compute total dissipation contained in shells of a field
    ! (all planes in one processor)
    tmpd = 0._dfloat
    do i = 0, p%nz - 1
       ! take into account effect of boundary conditions (reflexion)
       ! see also energy_tot
       if (((i .eq. 0     ) .and. (p%mpi%me .eq. 0         )) .or. &
            &((i .eq. p%nz-1) .and. (p%mpi%me .eq. p%mpi%np-1))) then
          tmpd = tmpd + energy_tot1d (z(:, i) * vk, p) * rho(i) * 2._dfloat
       else
          tmpd = tmpd + energy_tot1d (z(:, i) * vk, p) * rho(i) * 4._dfloat
       end if
    end do

    ! mpi_allreduce is overkill if we need to know the total energy only
    ! in the root processor, but we don't know if it is the case
    ! (unless we write another procedure for this case)
    call mpi_allreduce (tmpd, diss_tot, 1, mpi_double_precision, &
         mpi_sum, mpi_comm_world, ierr)
    
    ! convert this volumic energy to an energy
    diss_tot = disscoeff * diss_tot * p%dz * pi ** 3 / p%k0 ** 2
  end function diss_tot



!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Transform (u, b) to (zp, zm) for a loop section
! MPI: OK (no need)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine ub_to_zpm1d (u, b, zp, zm, p)
!-
    implicit none
    type(params),         intent(in)                            :: p
    complex(kind=dfloat), intent(in),  dimension(p%nmin:p%nmax) :: u, b
    complex(kind=dfloat), intent(out), dimension(p%nmin:p%nmax) :: zp, zm

    zp = u + b
    zm = u - b

  end subroutine ub_to_zpm1d

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Transform (u, b) to (zp, zm) for the planes of one processor
! MPI: OK (no need)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine ub_to_zpm (u, b, zp, zm, p)
!-
    implicit none
    type(params),         intent(in)                            :: p
    complex(kind=dfloat), intent(in),  &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                   :: u, b
    complex(kind=dfloat), intent(out), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                   :: zp, zm

    zp = u + b
    zm = u - b

  end subroutine ub_to_zpm


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Transform (zp, zm) to (u, b) for a loop section
! MPI: OK (no need)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine zpm_to_ub1d (zp, zm, u, b, p)
!-
    implicit none
    type(params),         intent(in)                            :: p
    complex(kind=dfloat), intent(in),  dimension(p%nmin:p%nmax) :: zp, zm
    complex(kind=dfloat), intent(out), dimension(p%nmin:p%nmax) :: u, b

    u = (zp + zm) / 2._dfloat
    b = (zp - zm) / 2._dfloat

  end subroutine zpm_to_ub1d


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Transform (zp, zm) to (u, b) for the planes of one processor
! MPI: OK (no need)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine zpm_to_ub (zp, zm, u, b, p)
!-
    implicit none
    type(params),         intent(in)                            :: p
    complex(kind=dfloat), intent(in),  &
         dimension(p%nmin:p%nmax, 0:p%nz-1) :: zp, zm
    complex(kind=dfloat), intent(out), &
         dimension(p%nmin:p%nmax, 0:p%nz-1) :: u, b

    u = (zp + zm) / 2._dfloat
    b = (zp - zm) / 2._dfloat

  end subroutine zpm_to_ub


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Compute Reynolds numbers Re and Rm (average over first
! quarter of wavenumbers for a loop section)
! TODO: this should be updated to a better definition of Re/Rm
! MPI: OK (no need)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine reynolds_numbers1d (zp, zm, k, re, rm, p)
!-
    implicit none
    type(params),         intent(in)                           :: p
    complex(kind=dfloat), intent(in), dimension(p%nmin:p%nmax) :: zp, zm
    complex(kind=dfloat), intent(in), dimension(p%nmin:p%nmax) :: k
    real   (kind=dfloat), intent(out)                          :: re, rm

    complex(kind=dfloat), dimension(p%nmin:p%nmax) :: u, b
    integer nb, i

    call zpm_to_ub1d (zp, zm, u, b, p)
    nb = (p%nmax - p%nmin) / 4 + 1
    re = 0._dfloat
    rm = 0._dfloat
    do i = p%nmin, p%nmin + nb - 1
       re = re + abs (u(i) / k(i))
       rm = rm + abs (b(i) / k(i))
    end do
    re = re / (real(nb, kind=dfloat) * p%nu )
    rm = rm / (real(nb, kind=dfloat) * p%eta)

  end subroutine reynolds_numbers1d


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Return the energy flux of a wave in a given loop section 
! Energy_tot1d gives values in unit of Cs^2
! VVa is unit of Cs^2
! Den is the anormalisation factor needed by energi_tot1d
! MPI: OK (no need)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  function zflux (z, den, VVa, p) 
!    implicit none
!    type   (params),      intent(in)                           :: p
!    real   (kind=dfloat)                                       :: Zflux
!    complex(kind=dfloat), intent(in), dimension(p%nmin:p%nmax) :: z
!    ! (density normalization factor)^-1 and Alfven speed
!    real(kind=dfloat), intent(in)                              :: Den, VVa
!
!    ! multiply for 1/2 if time averaged 
!    zflux = 0.5_dfloat * energy_tot1d (z, den, p) * VVa 
!  end function zflux


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! Return the value of the transmission coefficient (top boundary)
!! Only linear case!!
!! MPI: TODO (in collaboration with caller subroutine)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  function transm (Zpb, Zpt, denb, dent, Vab, Vat, rb, rt, p)
!    implicit none
!    type   (params),      intent(in)                 :: p
!    ! (density normalization factor)^-1 and Alfven speed
!    real(kind=dfloat), intent(in)                    :: Denb, Dent, Vab, Vat
!    real(kind=dfloat), intent(in)    :: rb, rt
!    complex(kind=dfloat), intent(in), dimension(p%nmin:p%nmax)  :: zpb, zpt
!    real(kind=dfloat)                :: transm
!
!    transm = rt * rt * Zflux(zpt, dent, Vat, p) &
!         / (rb * rb * Zflux(zpb, denb, Vab, p)) &
!         * (denb / dent) ** 4
!    !      transm = denb ** 4 * rt * Vat / (dent ** 4 * rb * Vab) &
!    !               * real(dot_product(zpt, zpt)) / real(dot_product(zpb, zpb))
!
!  end function transm


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Check whether time scales are shorter than time step
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine check_time_scales (zp, zm, k, p, ti, t, tscales)
    use atmosphere
    use nonlin
!-
    implicit none
    type(params),         intent(inout)                        :: p
    complex(kind=dfloat), intent(in), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                    :: zp, zm
    complex(kind=dfloat), intent(in), dimension(p%nmin:p%nmax) :: k
    integer             , intent(in)                           :: ti
    real(kind=dfloat)   , intent(in)                           :: t
    type(timescales)    , intent(inout)                        :: tscales

    ! for computation of perpendicular Alfven time step
    real(kind=dfloat) :: tauaperp
    real(kind=dfloat), dimension(0:p%nz-1) :: bb0

    real(kind=dfloat) :: tmp

    integer :: iz, ik
    integer, save :: ic = 0

    ! compute smallest non-linear time scale
    tmp = 0
!     if (p%mpi%me .eq. 0) then
!        print*, maxval (abs (zp)), maxval (abs (zm))
!     end if
    do iz = 0, p%nz - 1
       tmp = max (tmp, &
            kaexp(iz) * maxval (abs (k * zp(:,iz) )), &
            kaexp(iz) * maxval (abs (k * zm(:,iz) )))
    end do
    tscales%taunl = 1._dfloat / tmp
    call mpi_allreduce (tscales%taunl, tmp, 1, mpi_double_precision, &
         mpi_min, mpi_comm_world, ierr)
    tscales%taunl = tmp  ! not useful on other processors than root

    ! compute smallest relevant (not in dissipation zone) perpendicular Alfven time
    if (module_nonlin .eq. "goyb" .or. module_nonlin .eq. "sabrab") then
       bb0 = 0._dfloat
       tauaperp = 0._dfloat   ! is actually local 1/tauperp at the moment
       do ik = max (p%nmin + 1, 3), p%nmax
          bb0 = bb0 + biskampa * real (zp(ik-1, :) - zm(ik-1, :), kind=dfloat)
          tmp = k(ik) * maxval (abs (bb0 * kaexp))
          ! find larger 1/tauperp, so that tauaperp is still larger than taunu
          if (tmp .gt. tauaperp .and. tmp .lt. max (p%nu, p%eta) * (abs (k(ik)) * maxval (kaexp)) ** visc) then
             tauaperp = tmp
          end if
       end do
       call mpi_allreduce (tauaperp, tmp, 1, mpi_double_precision, mpi_max, mpi_comm_world, ierr)
       tauaperp = 1._dfloat / tmp
    end if

    if (ic .eq. 0) then
       ! compute smallest dissipation time scale
       tmp = (k(p%nmax) * maxval (kaexp)) ** visc
       tmp = min (1._dfloat / p%nu , 1._dfloat / p%eta) / tmp
       call mpi_allreduce (tmp, tscales%taunu, 1, mpi_double_precision, &
            mpi_min, mpi_comm_world, ierr)

       ! compute smallest Alfven time scale
       tmp = p%dz / maxval (va)
       call mpi_allreduce (tmp, tscales%taual, 1, mpi_double_precision, &
            mpi_min, mpi_comm_world, ierr)

       ! compute crossing time
       tmp = 0._dfloat
       do iz = 0, p%nz-1
          tmp = tmp + p%dz / va(iz)
       end do
       call mpi_allreduce (tmp, tscales%taucr, 1, mpi_double_precision, &
            mpi_sum, mpi_comm_world, ierr)
    end if

    if (p%mpi%me .eq. p%mpi%root) then

       ! Update time step (adaptative time step)
       ! CFL condition with security factor
       ! Less constraint from taunu as dissipation is implicit
       if (module_nonlin .eq. "none") then
           p%delta_t = min (tscales%taunu * 1.e12_dfloat, tscales%taual) / 5._dfloat
       else if (module_nonlin .eq. "goyb" .or. module_nonlin .eq. "sabrab") then
          p%delta_t = min (tscales%taunl, tscales%taunu * 1.e12_dfloat, tscales%taual, tauaperp) / 5._dfloat
       else
         p%delta_t = min (tscales%taunl, tscales%taunu * 1.e12_dfloat, tscales%taual) / 5._dfloat
       endif

       ! Time step and scales information
100    format ('ti:', i10, '  t:', ES12.5, '  dt:', ES10.3, '  tNL:', ES10.3)
1001   format ('ti:', i10, '  t:', ES12.5, '  dt:', ES10.3, '  tNL:', ES10.3, '  tAp:', ES10.3)
101    format ('         tcr:', ES10.3, '  tnu:', ES10.3, '  tAl:', ES10.3)


       if (mod (ic, 100) == 0) then
          if (module_nonlin .eq. "goyb" .or. module_nonlin .eq. "sabrab") then
             print 1001, ti, t, p%delta_t, tscales%taunl, tauaperp
          else
             print 100, ti, t, p%delta_t, tscales%taunl
          end if
          if (mod (ic, 1000) == 0) print 101, tscales%taucr, tscales%taunu, tscales%taual
          call flush (6)
       end if
    endif
    call mpi_bcast (p%delta_t, 1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)

    ic = ic + 1

  end subroutine check_time_scales

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Initialize or print wallclock time needed for simulation
! Prints information on efficiency
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine wallclock (p, ti, init)
!-
    type(params), intent(in)           :: p
    integer,      intent(in)           :: ti
    logical,      intent(in), optional :: init
    real(kind=dfloat), save            :: clock0, clock1
    real(kind=dfloat)                  :: cd

    if (p%mpi%me .eq. p%mpi%root) then
       if (present (init) .and. init) then
          call cpu_time (clock0)
       else
          call cpu_time (clock1)
          cd = clock1 - clock0
          print '(A,ES10.3,A,ES10.3,A)', 'Simulation duration: ', cd, &
               ' s   (',  cd / ti / p%tnz * p%mpi%np, ' s/nt/nz*np )'
          ! (don't write     / (ti * p%nz) to avoid integer overflows)
       end if
    end if
  end subroutine wallclock

end module diagnostics
