!+
!***********************************************************
! Module unit-open
!***********************************************************
! Units (CGS) for variables for open problem
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  30 Aug 05: AV/EB: created
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!-

module unit
  use types
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_unit = "open"

!+
  !                                     length           time       mass
!  type(units), parameter :: un = units (6.961e10_dfloat, 1._dfloat, 1._dfloat)
  type(units), parameter :: un = units (1._dfloat, 1._dfloat, 1._dfloat)
!-

  ! surface gravity acceleration
  real(kind=dfloat), parameter :: gg = 27389.9226 / un%length * un%time ** 2
  real(kind=dfloat), parameter :: Rsun = 6.961e+10 / un%length ! ----->  cm
  ! proton mass 
  ! the factro is to obtain an Alfven speed in km/s once used to get Rho
  ! in g/cm/km/km
  ! real(kind=dfloat), parameter :: mp = 1.67262158d-24 ! ---->  g


  !  real(kind=dfloat), parameter :: Msun = 1.9889e+33 ! --->  g
  !  real(kind=dfloat), parameter :: G = 6.673e-23  ! ------>  km^3 g^-1 s^-2


end module unit
