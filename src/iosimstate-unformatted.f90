!+
!***********************************************************
! Module iosimstate
!***********************************************************
! Input and output of simulation state, unformatted data
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  22 Oct 05: EB Created (moved from shell-atm)
!  18 Nov 05: AV/EB Version for unformatted output (module alternative)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module iosimstate
  use mpi
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_iosimstate = "unformatted"

contains

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Read state from which the simulation should continue
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine read_simstate (zp, zm, p, t)
!-
    implicit none
    type(params), intent(inout) :: p
    complex(kind=dfloat), intent(out), &
         dimension(p%nmin:p%nmax, 0:p%nz-1) :: zp, zm
    real(kind=dfloat), intent(out) :: t
    type(params)      :: oldp
    integer           :: i, j, ip, zlen
    real(kind=dfloat) :: fval1, fval2
    real(kind=dfloat) :: dummy
    character (len=*), parameter :: formi = "(a, i12, a, i12)"
    character (len=*), parameter :: formf = "(a, es12.5e2, a, es12.5e2)"
    integer, dimension(0:p%mpi%np-1) :: allnz
    integer           :: status(mpi_status_size)
    complex(kind=dfloat), allocatable, dimension(:,:) :: ztmp

    call mpi_gather (p%nz, 1, mpi_integer, allnz, 1, mpi_integer, &
         p%mpi%root, mpi_comm_world, ierr)
    if (p%mpi%me .eq. p%mpi%root) then
       print*, "Reading simulation state"

       open (91, file='simstate.dat', status='old', form='unformatted')

       ! check parameters
       read(91) oldp%nmin;   read(91) oldp%nmax
       read(91) oldp%tnz
       read(91) oldp%k0;     read(91) oldp%lambda
       read(91) oldp%nu;     read(91) oldp%eta
       read(91) oldp%eps;    read(91) oldp%epsm
       read(91) oldp%b0;     read(91) oldp%dz
       read(91) oldp%nt;     read(91) oldp%delta_t
       if (p%nmin /= oldp%nmin) then
          print formi, "Warning: parameter nmin has changed.    Old: ", &
               oldp%nmin   , " New: ", p%nmin
       end if
       if (p%nmax /= oldp%nmax) then
          print formi, "Warning: parameter nmax has changed.    Old: ", &
               oldp%nmax   , " New: ", p%nmax
       end if
       if (p%nmax - p%nmin /= oldp%nmax - oldp%nmin) then
          print *, "Error: number of modes has changed."
          stop
       end if
       if (p%tnz /= oldp%tnz) then
          print *, "Error: number of loop sections has changed."
          stop
       end if
       if (p%k0 /= oldp%k0) then
          print formf, "Warning: parameter k0 has changed.      Old: ", &
               oldp%k0     , " New: ", p%k0
       end if
       if (p%lambda /= oldp%lambda) then
          print formf, "Warning: parameter lambda has changed.  Old: ", &
               oldp%lambda , " New: ", p%lambda
       end if
       if (p%eta /= oldp%eta) then
          print formf, "Warning: parameter eta has changed.     Old: ", &
               oldp%eta    , " New: ", p%eta
       end if
       if (p%nu /= oldp%nu) then
          print formf, "Warning: parameter nu has changed.      Old: ", &
               oldp%nu     , " New: ", p%nu
       end if
       if (p%eps /= oldp%eps) then
          print formf, "Warning: parameter eps has changed.     Old: ", &
               oldp%eps    , " New: ", p%eps
       end if
       if (p%epsm /= oldp%epsm) then
          print formf, "Warning: parameter epsm has changed.    Old: ", &
               oldp%epsm   , " New: ", p%epsm
       end if
       if (p%epsm /= oldp%b0) then
          print formf, "Warning: parameter b0 has changed.      Old: ", &
               oldp%b0     , " New: ", p%b0
       end if
       if (p%dz /= oldp%dz) then
          print formf, "Warning: parameter dz has changed.      Old: ", &
               oldp%dz     , " New: ", p%dz
       end if

       ! number of output records (not useful for restarting; only for IDL
       ! data processing)
       read(91) dummy
       read(91) dummy
       read(91) dummy
       allocate (ztmp(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
    end if

    ! get shell values
    do ip=0, p%mpi%np-1
       if (p%mpi%me .eq. p%mpi%root) then
          do j = 0, allnz(ip) - 1
             do i = p%nmin, p%nmax
                read(91) fval1
                read(91) fval2
                ztmp(i, j) = cmplx (fval1, fval2, kind=dfloat)
             end do
          end do
          if (ip .eq. p%mpi%root) then
             zp = ztmp
          else  ! send data (only if to a different processor than root)
             zlen = (p%nmax - p%nmin + 1) * allnz (ip)
             call mpi_send (ztmp, zlen, mpi_double_complex, &
                  ip, 1, mpi_comm_world, ierr)
          end if
       end if
    end do
    if (p%mpi%me .ne. p%mpi%root) then
       zlen = (p%nmax - p%nmin + 1) * p%nz
       call mpi_recv (zp, zlen, mpi_double_complex, &
            p%mpi%root,  1, mpi_comm_world, status, ierr)
    end if

    do ip=0, p%mpi%np-1
       if (p%mpi%me .eq. p%mpi%root) then
          do j = 0, allnz(ip) - 1
             do i = p%nmin, p%nmax
                read(91) fval1
                read(91) fval2
                ztmp(i, j) = cmplx (fval1, fval2, kind=dfloat)
             end do
          end do
          if (ip .eq. p%mpi%root) then
             zm = ztmp
          else  ! send data (only if to a different processor than root)
             zlen = (p%nmax - p%nmin + 1) * allnz (ip)
             call mpi_send (ztmp, zlen, mpi_double_complex, &
                  ip, 2, mpi_comm_world, ierr)
          end if
       end if
    end do
    if (p%mpi%me .ne. p%mpi%root) then
       zlen = (p%nmax - p%nmin + 1) * p%nz
       call mpi_recv (zm, zlen, mpi_double_complex, &
            p%mpi%root,  2, mpi_comm_world, status, ierr)
    end if

    if (p%mpi%me .eq. p%mpi%root) then
       ! read current time
       read(91) t
       ! Check correlation time of forcing
       read(91) oldp%tstar
       if (p%tstar /= oldp%tstar) then
          print formf, "Warning: parameter tstar has changed.      Old: ", &
               oldp%tstar     , " New: ", p%tstar
       end if
       read(91) dummy
       if (nkforce /= dummy) then
          print formf, "Error: number of forced modes has changed. Old: ", &
               dummy     , " New: ", nkforce
          stop
       end if
    end if

    ! is broadcast useful?
    call mpi_bcast (t, 1, mpi_double_precision, &
         p%mpi%root, mpi_comm_world, ierr)

    ! Read random coefficients of forcing
    if (p%mpi%me .eq. p%mpi%root) then
       do i=0,nkforce-1   ! p%nmin, p%nmax   if allocated on all shells
          read(91) fval1;   read(91) fval2
          p%fc%a1(i) = cmplx (fval1, fval2, kind=dfloat)
          read(91) fval1;   read(91) fval2
          p%fc%b1(i) = cmplx (fval1, fval2, kind=dfloat)
       end do
       do i=0,nkforce-1   ! p%nmin, p%nmax   if allocated on all shells
          read(91) fval1;   read(91) fval2
          p%fc%a2(i) = cmplx (fval1, fval2, kind=dfloat)
          read(91) fval1;   read(91) fval2
          p%fc%b2(i) = cmplx (fval1, fval2, kind=dfloat)
       end do
       ! Send coefficients for second boundary to last processor if needed
       if (p%mpi%np - 1 .ne. p%mpi%root) then
          zlen = nkforce   ! p%nmax - p%nmin + 1   if allocated on all shells
          call mpi_send (p%fc%a2, zlen, mpi_double_complex, &
               p%mpi%np-1, 12, mpi_comm_world, ierr)
          call mpi_send (p%fc%b2, zlen, mpi_double_complex, &
               p%mpi%np-1, 13, mpi_comm_world, ierr)
       end if
    end if
    if (p%mpi%me .eq. p%mpi%np-1 .and. p%mpi%np - 1 .ne. p%mpi%root) then
       zlen = nkforce   ! p%nmax - p%nmin + 1   if allocated on all shells
       call mpi_recv (p%fc%a2, zlen, mpi_double_complex, &
            p%mpi%root, 12, mpi_comm_world, status, ierr)
       call mpi_recv (p%fc%b2, zlen, mpi_double_complex, &
            p%mpi%root, 13, mpi_comm_world, status, ierr)
    end if

    if (p%mpi%me .eq. p%mpi%root) then
       ! Check new parameters for atmosphere
       read(91) oldp%rho0
       read(91) oldp%h
       if (p%rho0 /= oldp%rho0) then
          print formi, "Warning: parameter rho0 has changed.    Old: ", &
               oldp%rho0   , " New: ", p%rho0
       end if
       if (p%h /= oldp%h) then
          print formi, "Warning: parameter h has changed.    Old: ", &
               oldp%h   , " New: ", p%h
       end if
       close (91)
    end if

  end subroutine read_simstate


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Write state from which the simulation could continue
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine write_simstate (zp, zm, p, outflags, outrecords, t, ti, &
       simstatefile)
!-
    implicit none
    type(params),      intent(inout) :: p
    type(saverecords), intent(in) :: outrecords
    type(saveflags),   intent(in) :: outflags
    complex(kind=dfloat), intent(in), &
         dimension(p%nmin:p%nmax, 0:p%nz-1) :: zp, zm
    real(kind=dfloat), intent(in) :: t
    integer, intent(in)           :: ti
    logical, optional, intent(in) :: simstatefile
    ! if true, write in simstate.dat
    ! if false (default), write in out_simstate_00000000ti
    
    integer                       :: i, iztot, j, ip, zlen, realoutnz, zstep

    integer, dimension(0:p%mpi%np-1) :: allnz
    integer           :: status(mpi_status_size)
    complex(kind=dfloat), allocatable, dimension(:,:) :: ztmp
    character*23 :: filename

    call mpi_gather (p%nz, 1, mpi_integer, allnz, 1, mpi_integer, &
         p%mpi%root, mpi_comm_world, ierr)

    if (p%mpi%me .eq. p%mpi%root) then
       if (present (simstatefile) .and. simstatefile) then
          filename = 'simstate.dat'
       else
          write (fmt='(i10.10)', unit=filename) ti
          write(102,'(i10.10)') ti
          filename = 'out_simstate_' // filename
       end if
       print*, "Writing simulation state to ", filename
       open (91, file=filename, status='replace', form='unformatted')

       ! step for output of planes
       zstep = p%tnz / outflags%outnz
       ! number of planes that are output (may be more than outnz!)
       realoutnz = (p%tnz - 1) / zstep + 1 

       ! write parameters
       write(91) p%nmin;    write(91) p%nmax
       ! number of planes we write
       if (present (simstatefile) .and. simstatefile) then
          write(91) p%tnz
       else
          write(91) realoutnz
       end if
       write(91) p%k0;      write(91) p%lambda
       write(91) p%nu;      write(91) p%eta
       write(91) p%eps;     write(91) p%epsm
       write(91) p%b0;
       ! Distance between output planes
       if (present (simstatefile) .and. simstatefile) then
          write(91) p%dz
       else
          write(91) p%dz * zstep !(integer division)
       end if
       ! number of time steps really done (not p%nt!)
       write(91) ti;
       write(91) p%delta_t   ! not taken into account by read_simstate

       ! write number of output records (only for IDL data processing)
       write(91) outrecords%energy
       write(91) outrecords%sfun
       write(91) outrecords%spec

       allocate (ztmp(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
    end if

    ! get shell values from all processors and write them
    ! More communications than needed if outflags%outnz .lt. p%tnz...
    if (p%mpi%me .eq. p%mpi%root) then
       iztot = 0
       do ip=0, p%mpi%np-1
          if (ip .eq. p%mpi%root) then
             ztmp = zp
          else
             zlen = (p%nmax - p%nmin + 1) * allnz (ip)
             call mpi_recv (ztmp, zlen, mpi_double_complex, &
                  ip, 3, mpi_comm_world, status, ierr)
          end if
          do j = 0, allnz(ip) - 1
             if (mod (iztot, zstep) .eq. 0 &
                  .or. (present (simstatefile) .and. simstatefile)) then
                do i = p%nmin, p%nmax
                   write(91)  real  (ztmp(i, j), kind=dfloat)
                   write(91)  aimag (ztmp(i, j))
                end do
             end if
             iztot = iztot + 1
          end do
       end do
    else
       zlen = (p%nmax - p%nmin + 1) * p%nz
       call mpi_send (zp, zlen, mpi_double_complex, &
            p%mpi%root,  3, mpi_comm_world, ierr)
    end if

    if (p%mpi%me .eq. p%mpi%root) then
       iztot = 0
       do ip=0, p%mpi%np-1
          if (ip .eq. p%mpi%root) then
             ztmp = zm
          else
             zlen = (p%nmax - p%nmin + 1) * allnz (ip)
             call mpi_recv (ztmp, zlen, mpi_double_complex, &
                  ip, 4, mpi_comm_world, status, ierr)
          end if
          do j = 0, allnz(ip) - 1
             if (mod (iztot, zstep) .eq. 0 &
                  .or. (present (simstatefile) .and. simstatefile)) then
                do i = p%nmin, p%nmax
                   write(91)  real  (ztmp(i, j), kind=dfloat)
                   write(91)  aimag (ztmp(i, j))
                end do
             end if
             iztot = iztot + 1
          end do
       end do
    else
       zlen = (p%nmax - p%nmin + 1) * p%nz
       call mpi_send (zm, zlen, mpi_double_complex, &
            p%mpi%root,  4, mpi_comm_world, ierr)
    end if

    if (p%mpi%me .eq. p%mpi%root) then
       ! Write current time
       write(91) t
       ! Write correlation time of forcing
       write(91) p%tstar
       ! Write number of modes on which forcing is made
       write(91) nkforce
    end if

    ! Write random coefficients of forcing
    if (p%mpi%me .eq. p%mpi%root) then
       do i=0,nkforce-1   ! p%nmin, p%nmax   if allocated on all shells
          write(91)  real (p%fc%a1(i), kind=dfloat)
          write(91) aimag (p%fc%a1(i))
          write(91)  real (p%fc%b1(i), kind=dfloat)
          write(91) aimag (p%fc%b1(i))
       end do
       ! Receive coefficients for second boundary from last processor
       if (p%mpi%root .ne. p%mpi%np - 1) then
          zlen = nkforce   ! p%nmax - p%nmin + 1   if allocated on all shells
          call mpi_recv (p%fc%a2, zlen, mpi_double_complex, &
               p%mpi%np-1, 14, mpi_comm_world, status, ierr)
          call mpi_recv (p%fc%b2, zlen, mpi_double_complex, &
               p%mpi%np-1, 15, mpi_comm_world, status, ierr)
       end if
       do i=0,nkforce-1   ! p%nmin, p%nmax   if allocated on all shells
          write(91)  real (p%fc%a2(i), kind=dfloat)
          write(91) aimag (p%fc%a2(i))
          write(91)  real (p%fc%b2(i), kind=dfloat)
          write(91) aimag (p%fc%b2(i))
       end do
    end if
    if (p%mpi%me .eq. p%mpi%np-1 .and. p%mpi%me .ne. p%mpi%root) then
       zlen = nkforce   ! p%nmax - p%nmin + 1   if allocated on all shells
       call mpi_send (p%fc%a2, zlen, mpi_double_complex, &
            p%mpi%root, 14, mpi_comm_world, ierr)
       call mpi_send (p%fc%b2, zlen, mpi_double_complex, &
            p%mpi%root, 15, mpi_comm_world, ierr)
    end if

    if (p%mpi%me .eq. p%mpi%root) then
       ! Write parameters of atmosphere
       write(91) p%rho0
       write(91) p%h
       close (91)
    end if

    if (p%mpi%me .eq. p%mpi%root) then
       deallocate (ztmp, stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
    end if

  end subroutine write_simstate

end module iosimstate
