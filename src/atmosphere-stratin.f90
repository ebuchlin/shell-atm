!+
!***********************************************************
! Module atmosphere-stratin 
!***********************************************************
! Variables and routines for a stratified cartesian atmosphere where
! the profiles for density and Alfven speed are read in files
! (in_rho and in_va)
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  22 Mar 2006: EB Created
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module atmosphere
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_atmosphere = "stratin"

  ! Atmosphere
  real(kind=dfloat), dimension(:), allocatable :: &
       va, va_m, vad, rho, r, aexp, kaexp

  ! normalisation value for propagation: unorm = 1/rho^1/4
  real(kind=dfloat),    dimension(:),   allocatable :: unorm
  ! normalisation value: sound speed
  real(kind=dfloat)                            :: CsN

contains 

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Allocate variables for the atmosphere
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine atmosphere_allocate(p)
!-
    type(params), intent(in)      :: p

    allocate (va(0:p%nz-1))  
    allocate (va_m(0:p%nz-1))  
    allocate (vad(0:p%nz-1))  
    allocate (rho(0:p%nz-1))  
    allocate (r(0:p%nz-1))  
    allocate (unorm(0:p%nz-1))
    allocate (aexp(0:p%nz-1))
    allocate (kaexp(0:p%nz-1))

  end subroutine atmosphere_allocate


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Deallocate variables for the atmosphere
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine atmosphere_deallocate
!-
    deallocate (va)
    deallocate (va_m)
    deallocate (vad)
    deallocate (rho)
    deallocate (r)
    deallocate (unorm)
    deallocate (aexp)
    deallocate (kaexp)

  end subroutine atmosphere_deallocate

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Assign values for Alfven speed, Alfven speed gradient and 
! mass density to the grid points.
! These values are computed by the xxx_det routines
!    (different alternatives according to geometry)
! MPI: need to check
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine grid_assign (p)
    use mpi
!-
    implicit none
    type(params), intent(in)      :: p
    integer                       :: i, ip
    real(kind=dfloat)             :: ir
    real(kind=dfloat), allocatable, dimension(:) :: &
         allva, allvad, allvam, allrho
    integer, dimension(0:p%mpi%np-1) :: allnz0, allnz
    integer           :: status(mpi_status_size)
    real(kind=dfloat) :: aexpnorm, b

    
    call mpi_gather (p%nz0, 1, mpi_integer, allnz0, 1, mpi_integer, &
         p%mpi%root, mpi_comm_world, ierr)
    call mpi_gather (p%nz , 1, mpi_integer, allnz , 1, mpi_integer, &
         p%mpi%root, mpi_comm_world, ierr)

    ! Assign grid values for r
    ! (would be by r_det, but soon replaced by grid_det)
    do i = 0, p%nz-1
       r(i) = (i + p%nz0) * p%dz
    end do

    ! Read values for va and rho in files, on root processor
    ! and deduce profiles of vad and va_m
    if (p%mpi%me .eq. p%mpi%root) then
       allocate (allva  (0:p%tnz-1))
       allocate (allvad (0:p%tnz-1))
       allocate (allvam (0:p%tnz-1))
       allocate (allrho (0:p%tnz-1))

       open (99, file='in_va', status='old')
       do i = 0, p%tnz - 1
          read (99,*) allva(i)
       end do
       close (99)
       do i = 0, p%tnz - 2
          ! Alfven speed at next mid-point
          ! may need to be adapted if non-uniform grid
          allvam (i) = (allva(i+1) + allva(i)) / 2._dfloat
          ! Alfven speed derivative
          ! Ready for non-uniform grid (but 1st-order)
          allvad (i) = (allva(i+1) - allva(i)) / p%dz
       end do
       ! We replicate the last value (even if it shouldn't be used)
       allvam(p%tnz-1) = allvam(p%tnz-2)
       allvad(p%tnz-1) = allvad(p%tnz-2)

       open (99, file='in_rho', status='old')
       do i = 0, p%tnz - 1
          read (99,*) allrho(i)
       end do
       close (99)
    end if

    ! scatter these profiles on the processors
    call mpi_scatterv (allva , allnz, allnz0, mpi_double_precision, &
         va  , p%nz, mpi_double_precision, p%mpi%root, mpi_comm_world, ierr)
    call mpi_scatterv (allvad, allnz, allnz0, mpi_double_precision, &
         vad , p%nz, mpi_double_precision, p%mpi%root, mpi_comm_world, ierr)
    call mpi_scatterv (allvam, allnz, allnz0, mpi_double_precision, &
         va_m, p%nz, mpi_double_precision, p%mpi%root, mpi_comm_world, ierr)
    call mpi_scatterv (allrho, allnz, allnz0, mpi_double_precision, &
         rho, p%nz, mpi_double_precision, p%mpi%root, mpi_comm_world, ierr)

    if (p%mpi%me .eq. p%mpi%root) then
       ! deduce values for aexp
       ! (normalize by loop-top value: width of loop is taken at loop-top; 
       !  this should depend on the model)
       ! As it is a normalization factor we don't care about 4\pi or \mu_0
       aexpnorm = allva(p%tnz/2) * sqrt (allrho(p%tnz/2))
       
       deallocate (allva )
       deallocate (allvad)
       deallocate (allvam)
       deallocate (allrho)
    end if

    call mpi_bcast (aexpnorm, 1, mpi_double_precision, p%mpi%root, &
         mpi_comm_world, ierr)
    ! deduce normalized values for aexp (fexpand is not needed)
    do i=0, p%nz-1
       aexp(i) = aexpnorm / (va(i) * sqrt (rho(i)))
    end do
    ! expansion factor expressed in units of inverse length (for use with k)
    kaexp = 1._dfloat / sqrt(aexp)

    ! normalization factor for zp and zm
    unorm = rho ** (-.25_dfloat)
    ! normalization factor for velocities (not needed)
    CsN = 0._dfloat

  end subroutine grid_assign



!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Print parameters specific to atmosphere
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine atmosphere_print_parameters(p)
!-
    implicit none
    type(params), intent(in)     :: p

    if (p%mpi%me .eq. p%mpi%root) then
       print*, '*** Characteristics of atmosphere ***'
       print*, '  Stratified, read from files.'
       print*, '  Minimum, average and maximum values:'
    end if

  end subroutine atmosphere_print_parameters


end module atmosphere
