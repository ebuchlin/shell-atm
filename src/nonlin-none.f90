!+
!***********************************************************
! Module nonlin-none
!***********************************************************
! No non-linear interactions
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  24 Oct 05: EB Created
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module nonlin
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_nonlin = "none"

contains 

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Empty routine (don't do integration of non-linear terms)
! MPI: OK (no need)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine integrate_step_nonlin (zp, zm, k, p)
!-
    implicit none
    type(params),         intent(in)                              :: p
    complex(kind=dfloat), intent(inout), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                     :: zp, zm
    complex(kind=dfloat), intent(in),    dimension(p%nmin:p%nmax) :: k

  end subroutine integrate_step_nonlin

end module nonlin
