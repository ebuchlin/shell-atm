!+
!***********************************************************
! Module nonlin-goyb
!***********************************************************
! Integration of non-linear terms through GOY shell-models
! with "Biskamp" term
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  24 Oct 05: EB Created (moved from shell-atm)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module nonlin
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_nonlin = "goyb"

contains 

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Perform one step of 3rd order Runge-Kutta method integration
! Tesi di laurea, Marco Ghiglione, p 38
! Version for shell-loop: deals with 2D fields, so that Alfven-wave
! propagation can be directly handled by the routine computing the
! right handside of the differential equation (derivs2d)
! MPI: OK (no need)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine integrate_step_nonlin (zp, zm, k, p)
!-
    implicit none
    type(params),         intent(in)                              :: p
    complex(kind=dfloat), intent(inout), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                     :: zp, zm
    complex(kind=dfloat), intent(in),    dimension(p%nmin:p%nmax) :: k

    ! temporary arrays for numerical scheme
    complex(kind=dfloat), dimension(:,:), allocatable, save :: vp, vm, &
         gp, gm, tp, tm
    ! are these temporary arrays already allocated?
    logical, save :: isallocated = .FALSE.

    if (.NOT. isallocated) then
       allocate (vp(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (vm(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (gp(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (gm(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (tp(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (tm(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       isallocated = .TRUE.
    end if

    vp = zp
    vm = zm
    call derivs2d (gp, vp, vm, k, p, +1)
    call derivs2d (gm, vm, vp, k, p, -1)
    vp = vp + (p%delta_t / 3._dfloat) * gp
    vm = vm + (p%delta_t / 3._dfloat) * gm
    call derivs2d (tp, vp, vm, k, p, +1)
    call derivs2d (tm, vm, vp, k, p, -1)
    gp = tp - (5._dfloat / 9._dfloat) * gp
    gm = tm - (5._dfloat / 9._dfloat) * gm
    vp = vp + (15._dfloat / 16._dfloat * p%delta_t) * gp
    vm = vm + (15._dfloat / 16._dfloat * p%delta_t) * gm
    call derivs2d (tp, vp, vm, k, p, +1)
    call derivs2d (tm, vm, vp, k, p, -1)
    gp = tp - (153._dfloat / 128._dfloat) * gp
    gm = tm - (153._dfloat / 128._dfloat) * gm
    zp = vp + (8._dfloat / 15._dfloat * p%delta_t) * gp
    zm = vm + (8._dfloat / 15._dfloat * p%delta_t) * gm

  end subroutine integrate_step_nonlin



!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Right-hand-side of ODEs (=dzs/dt)
! dzsdt: derivative (RHS of ODE) corresponding to zps
! zps, zms: Elsasser-like spectra
! dir: +1 if zps is z+ and zms z-, -1 if zps is z- and zms is z+
! Version in 2D
! MPI: OK (no need, as long as no propagation is done here)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine derivs2d (dzsdt, zps, zms, k, p, dir)
    use atmosphere
!-
    implicit none
    type(params),      intent(in) :: p
    ! dzsdt is the numerically computed right-handside of the partial 
    ! differential equation of the model, for zp (if dir==1) or for
    ! zm (if dir==-1)
    complex(kind=dfloat), intent(out), &
         dimension(p%nmin:p%nmax, 0:p%nz-1) :: dzsdt
    ! zps is zp if dir==1, zm if dir==-1
    ! zms is zm if dir==1, zp if dir==-1
    complex(kind=dfloat), intent(in),  &
         dimension(p%nmin:p%nmax, 0:p%nz-1) :: zps, zms
    complex(kind=dfloat), intent(in),  &
         dimension(p%nmin:p%nmax) :: k
    ! direction flag:
    ! 1 if computing d zp / dt, -1 if computing d zm / dt
    integer, intent(in) :: dir

    ! plane and wavenumber indices
    integer :: iz, ik
    ! current value of the "Biskamp" B_0^(n)(z,t) coefficient
    real(kind=dfloat), dimension(:), allocatable, save :: bb0

!!!for debug
!    complex(kind=dfloat), dimension(p%nmin:p%nmax, 0:p%nz-1) :: ddzsdt

    ! dissipation coefficient along z
    ! real (kind=dfloat) :: nuz

    ! are temporary arrays already allocated?
    logical, save :: isallocated = .FALSE.

    if (.NOT. isallocated) then
       allocate (bb0(0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       isallocated = .TRUE.
    end if

!!! GOY non-linear term
    ! using default dim=1 argument for eoshift (implicit for clarity):
    !  the arrays are shifted in the perp direction so that there are
    !  non-linear interactions with the neighboring shells
    dzsdt = eoshift(zps,+1) * eoshift(zms,+2) * (p%eps + p%epsm)
    dzsdt = dzsdt + eoshift(zms,+1) * eoshift(zps,+2) * (2 - p%eps - p%epsm)
    dzsdt = dzsdt + eoshift(zps,+1) * eoshift(zms,-1) * (p%epsm - p%eps)     / p%lambda
    dzsdt = dzsdt - eoshift(zms,+1) * eoshift(zps,-1) * (p%epsm + p%eps)     / p%lambda
    dzsdt = dzsdt - eoshift(zps,-1) * eoshift(zms,-2) * (p%epsm - p%eps)     / p%lambda ** 2
    dzsdt = dzsdt - eoshift(zms,-1) * eoshift(zps,-2) * (2 - p%eps - p%epsm) / p%lambda ** 2
    dzsdt = conjg (dzsdt) / 2._dfloat

    !!! for debug
!    ddzsdt = dzsdt

!!! "Biskamp" term for Alfvén effect
! take B into account from ik=2, gives a non-zero term from ik=3
    bb0(:) = 0._dfloat
    do ik = max (p%nmin + 1, 3), p%nmax
       bb0 = bb0 + biskampa * dir * real (zps(ik-1, :) - zms(ik-1, :), kind=dfloat)
       ! print*, ik, bb0(:)
       dzsdt(ik, :) = dzsdt(ik, :) + zps(ik, :) * bb0
    end do
    !!! debug: compute min dt corresponding to Biskamp term
!    ddzsdt = dzsdt - ddzsdt
!    do iz=0, p%nz-1
!       ddzsdt(:,iz) = (ddzsdt(:,iz) * k * kaexp(iz)) * (0._dfloat, 1._dfloat)
!    end do
!    print*, 'dt_Biskamp = ', minval (abs (zps / ddzsdt))


    ! multiply by i and by the k array in each plane
    ! TODO: find a more efficient way to do it (intrisic function?)
    do iz=0, p%nz-1
       dzsdt(:,iz) = (dzsdt(:,iz) * k * kaexp(iz)) * (0._dfloat, 1._dfloat)
    end do

  end subroutine derivs2d

end module nonlin
