!+
!***********************************************************
! Module iodata
!***********************************************************
! Output of data in files during the simulation,
! opening and closing the files
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  22 Oct 05: EB Created (moved from shell-atm)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module iodata
  use mpi
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_iodata = "formatted"

contains

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Open files useful for output
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! use error codes?
  subroutine output_open_files (p, outflags)
!-
    implicit none
    type(params),    intent(in) :: p
    type(saveflags), intent(in) :: outflags

    if (p%mpi%me .eq. p%mpi%root) then
       if (outflags%time) &
            open (10, file='out_time',     position='append', form='formatted')
       if (outflags%taunl) &
            open (101, file='out_taunl',   position='append', form='formatted')
       if (outflags%rawzp) &
            open (102, file='out_time2i',  position='append', form='formatted')
       if (outflags%rawzp .or. outflags%specu .or. outflags%specb) &
            open (103, file='out_time2',   position='append', form='formatted')
       if (outflags%enmode02) &
            open (11, file='out_ent02',    position='append', form='formatted')
       if (outflags%enmode08) &
            open (12, file='out_ent08',    position='append', form='formatted')
       if (outflags%enmode14) &
            open (13, file='out_ent14',    position='append', form='formatted')
       if (outflags%enmode20) &
            open (14, file='out_ent20',    position='append', form='formatted')
       if (outflags%entotu) &
            open (20, file='out_enu',      position='append', form='formatted')
       if (outflags%entotb) &
            open (21, file='out_enb',      position='append', form='formatted')
       if (outflags%dissu) &
            open (22, file='out_dissu',    position='append', form='formatted')
       if (outflags%dissb) &
            open (23, file='out_dissb',    position='append', form='formatted')
       if (outflags%forcp) &
            open (24, file='out_forcp',    position='append', form='formatted')
       if (outflags%crosshel) &
            open (26, file='out_crosshel', position='append', form='formatted')
       if (outflags%maghel) &
            open (27, file='out_maghel',   position='append', form='formatted')
       if (outflags%specu) &
            open (30, file='out_specavu',  position='append', form='formatted')
       if (outflags%specb) &
            open (31, file='out_specavb',  position='append', form='formatted')
       if (outflags%sfunu) &
            open (35, file='out_sfunu',    status='new', form='formatted')
       if (outflags%sfunu) &
            open (36, file='out_sfunb',    status='new', form='formatted')
    end if
  end subroutine output_open_files

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Output data in files for integrated quantities
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine output_data_a (zp, zm, k, p, tscales, outflags, outrecords, &
       t, ti, lasttime)
    use diagnostics
    use iosimstate
!-
    implicit none
    type(params),         intent(in)                           :: p
    type(timescales),     intent(in)                           :: tscales
    complex(kind=dfloat), intent(in), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                    :: zp, zm
    complex(kind=dfloat), intent(in), dimension(p%nmin:p%nmax) :: k
    type(saveflags),      intent(in)                           :: outflags
    type(saverecords),    intent(inout)                        :: outrecords
    real(kind=dfloat),    intent(in)                           :: t
    integer,              intent(in)                           :: ti
    logical, optional,    intent(in)                           :: lasttime
    
    ! u and b ('save' is for better performance only)
    real   (kind=dfloat)                                       :: tmp
    complex(kind=dfloat), save, allocatable, dimension(:,:)    :: u  , b

    ! are the saved arrays already allocated?
    logical, save :: isallocated = .FALSE.

153 format (ES23.15E3)

    if (.NOT. isallocated) then
       allocate (   u(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (   b(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       isallocated = .TRUE.
       outrecords%energy = 0
    end if

    if ((outflags%period > 0 .and. mod (ti, outflags%period ) == 0) .or. &
        (outflags%period < 0 .and. mod (t,  outflags%periodt) < p%delta_t)) then
       if (p%mpi%me .eq. p%mpi%root) then
          ! time corresponding to output of integrated quantities
          if (outflags%time)   write (10,153)  t
          ! last computed non-linear time scale
          if (outflags%taunl)  write (101,153) tscales%taunl
       end if
       
       ! check if this is necessary? (usually yes... so no need to check)
       call zpm_to_ub (zp, zm, u, b, p)

       ! Total energy in some modes
       if (outflags%enmode02) then
          tmp = energy_2_tot1dk (zp( 2, :), zm( 2, :), p)
          if (p%mpi%me .eq. p%mpi%root) write (11,153) tmp
       end if
       if (outflags%enmode08) then
          tmp = energy_2_tot1dk (zp( 8, :), zm( 8, :), p)
          if (p%mpi%me .eq. p%mpi%root) write (12,153) tmp
       end if
       if (outflags%enmode14) then
          tmp = energy_2_tot1dk (zp(14, :), zm(14, :), p)
          if (p%mpi%me .eq. p%mpi%root) write (13,153) tmp
       end if
       if (outflags%enmode20) then
          tmp = energy_2_tot1dk (zp(20, :), zm(20, :), p)
          if (p%mpi%me .eq. p%mpi%root) write (14,153) tmp
       end if

       ! Kinetic and magnetic energies in whole box
       ! factor 2., because we compute the total kinetic and magnetic 
       ! energies from u and b, with a function designed for z...
       if (outflags%entotu) then
          tmp = energy_tot (u, p)
          if (p%mpi%me .eq. p%mpi%root) write (20,153) 2._dfloat * tmp
       end if
       if (outflags%entotb) then
          tmp = energy_tot (b, p)
          if (p%mpi%me .eq. p%mpi%root) write (21,153) 2._dfloat * tmp
       end if

       ! instantaneous dissipations in whole box
       if (outflags%dissu) then
          tmp = diss_tot (u, k, p, p%nu)
          if (p%mpi%me .eq. p%mpi%root) write (22,153) tmp
       end if
       if (outflags%dissb) then
          tmp = diss_tot (b, k, p, p%eta)
          if (p%mpi%me .eq. p%mpi%root) write (23,153) tmp
       end if

       ! power of forcing at boundaries, as computed by last call of
       ! propagate_and_force
       if (outflags%forcp .and. p%mpi%me .eq. p%mpi%root) &
            write (24,153) p%forcp

       ! helicities in whole box
       if (outflags%crosshel) then
          tmp = cross_helicity (zp, zm, p)
          if (p%mpi%me .eq. p%mpi%root) write (26,153) tmp
       end if
       if (outflags%maghel) then
          tmp = magn_helicity (zp, zm, k, p)
          if (p%mpi%me .eq. p%mpi%root) write (27,153) tmp
       end if

       outrecords%energy = outrecords%energy + 1
    end if

  end subroutine output_data_a


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Output data in files for non-integrated quantities
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine output_data_b (zp, zm, k, p, tscales, outflags, outrecords, &
       t, ti, lasttime)
    use diagnostics
    use iosimstate
!-
    implicit none
    type(params),         intent(inout)                        :: p
    type(timescales),     intent(in)                           :: tscales
    complex(kind=dfloat), intent(in), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                  :: zp, zm
    complex(kind=dfloat), intent(in), dimension(p%nmin:p%nmax) :: k
    type(saveflags),      intent(in)                           :: outflags
    type(saverecords),    intent(inout)                        :: outrecords
    real(kind=dfloat),    intent(in)                           :: t
    integer,              intent(in)                           :: ti
    logical, optional,    intent(in)                           :: lasttime

    integer                                                   :: i, j, q
    ! "local" time steps for remembering how many spectra we summed to
    ! compute average
    integer, save                                             :: tloc = 0

    ! u and b ('save' is for better performance only)
    real   (kind=dfloat)                                      :: tmp
    complex(kind=dfloat), save, allocatable, dimension(:,:)   :: u  , b
    ! averages of spectra
    real   (kind=dfloat), save, allocatable, dimension(:)     :: spavu, spavb

    ! structure functions
    real   (kind=dfloat), save, allocatable, dimension(:,:,:) :: sfu, sfb
    ! used to compute average
    integer, save :: nsfun = 0

    ! average profile of heating
    real (kind=dfloat), save, allocatable, dimension(:) :: profheat
    ! used to compute average
    integer, save :: nprofheat = 0

    ! are these saved arrays already allocated?
    logical, save :: isallocated = .FALSE.
    logical, save :: isallocatedsfun = .FALSE.
    ! is the conversion to u and b already done?
    logical, save :: isubdone

    ! variables used for communication of fields to write
    integer                       :: ip, zlen
    integer                       :: status(mpi_status_size)
    real(kind=dfloat),    allocatable, save, dimension(:)   :: rtmp
    real(kind=dfloat),    allocatable, save, dimension(:,:) :: stmp
    ! number of planes in each processor
    integer, save, dimension(:), allocatable :: allnz
    ! period at which use spectra for averaging
    integer, save :: specsubperiod

153 format (ES23.15E3)

    if (.NOT. isallocated) then
       allocate (allnz (0:p%mpi%np-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       call mpi_gather (p%nz, 1, mpi_integer, allnz, 1, mpi_integer, &
            p%mpi%root, mpi_comm_world, ierr)
       allocate (   u(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (   b(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate ( spavu(p%nmin:p%nmax), stat=aerr)
       spavu = 0
       if (aerr .ne. 0) stop 'Allocation error'
       allocate ( spavb(p%nmin:p%nmax), stat=aerr)
       spavb = 0
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (rtmp(               0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (stmp(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (profheat(           0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       profheat = 0

       isallocated = .TRUE.
       outrecords%sfun = 0
       outrecords%spec = 0
       if (outflags%specperiod > 0) then
          specsubperiod = outflags%specperiod / 10
          specsubperiod = max (specsubperiod, 1)   ! avoid division by 0
       else
          specsubperiod = 1  ! shouldn't be used, but still avoid 0
       end if
    end if
    isubdone = .FALSE.
    
    !! Prepare output of spectra (averaged over time) and structure functions
    ! Try to use 10 different times per specperiod to compute average
    ! (see also definition of specsubperiod above) 
    if ((outflags%specperiod > 0 .and. &
           mod (ti, specsubperiod) == 0) .or. &
        (outflags%specperiod < 0 .and. &
           mod (t,  outflags%specperiodt / 10._dfloat) < p%delta_t)) then

       call zpm_to_ub (zp, zm, u, b, p)
       isubdone = .TRUE.
       
       ! Spectra
       ! In the end, the coefficients should be consistent with those
       ! of energy_tot1dk
       ! The sum of all values of the spectra should give the total energy
       if (outflags%specu) then
          do j = 0, p%nz - 1
             spavu(:) = spavu(:) + real (u(:, j) * conjg (u(:, j)), &
                  kind=dfloat) * rho(j) * aexp(j)
          end do
       end if

       if (outflags%specb) then
          do j = 0, p%nz - 1
             spavb(:) = spavb(:) + real (b(:, j) * conjg (b(:, j)), &
                  kind=dfloat) * rho(j) * aexp(j) 
          end do
       end if

       ! structure functions
       ! Depend on z, so taking into account rho, k0 and aexp is left
       ! for the analysis (but this may change)
       if (outflags%sfunu .or. outflags%sfunb) then
          if (.not. isallocatedsfun) then
             allocate (sfu(p%nmin:p%nmax, 0:p%nz-1, 1:outflags%sfunnb), &
                  stat=aerr)
             if (aerr .ne. 0) stop 'Allocation error'
             allocate (sfb(p%nmin:p%nmax, 0:p%nz-1, 1:outflags%sfunnb), &
                  stat=aerr)
             if (aerr .ne. 0) stop 'Allocation error'
             isallocatedsfun = .TRUE.
             sfu = 0._dfloat
             sfb = 0._dfloat
          end if
          do q = 1, outflags%sfunnb
             do j = 0, p%nz - 1
                do i = p%nmin, p%nmax
                   if (outflags%sfunu) then
                      sfu(i, j, q) = sfu(i, j, q) + abs (u(i, j)) ** q
                   end if
                   if (outflags%sfunb) then
                      sfb(i, j, q) = sfb(i, j, q) + abs (b(i, j)) ** q
                   end if
                end do
             end do
          end do
          nsfun = nsfun + 1
       end if

       tloc = tloc + 1
    end if

    ! Prepare output of heating profiles (averaged over all time steps)
    if (outflags%profheat) then 
       if (.not. isubdone) then
          call zpm_to_ub (zp, zm, u, b, p)
          isubdone = .TRUE.
       end if
       do i = 0, p%nz - 1 
          do j = p%nmin, p%nmax
             profheat(i) = profheat(i) + k(j) ** visc * &
                  (abs (u(j, i)) ** 2 * p%nu + abs (b(j, i)) ** 2 * p%eta )
             ! all this will be multiplied by a factor pi ** 3 * rho(i)
             ! in the end, and divided by nprofheat and p%k0**2
          end do
       end do
       nprofheat = nprofheat + 1
    end if

    !! Output data (spectra, raw fields (simstate))
    if ((outflags%specperiod > 0 .and. &
            mod (ti, outflags%specperiod) == 0) .or. &
        (outflags%specperiod < 0 .and. &
            mod (t, outflags%specperiodt) < p%delta_t)) then

       ! perpendicular spectra, summed on all planes
       if (outflags%specu) then
          spavu = spavu / real (tloc, kind=dfloat)
          ! sum spectra from all processors, and write them
          do i = p%nmin, p%nmax
             call mpi_reduce (spavu(i), tmp, 1, mpi_double_precision, &
                  mpi_sum, p%mpi%root, mpi_comm_world, ierr)
             if (p%mpi%me .eq. p%mpi%root) then
                write (30,153) tmp * .5_dfloat * p%dz * pi ** 3 / p%k0 ** 2
             end if
          end do
       end if
       if (outflags%specb) then
          spavb = spavb / real (tloc, kind=dfloat)
          ! sum spectra from all processors, and write them
          do i = p%nmin, p%nmax
             call mpi_reduce (spavb(i), tmp, 1, mpi_double_precision, &
                  mpi_sum, p%mpi%root, mpi_comm_world, ierr)
             if (p%mpi%me .eq. p%mpi%root) then
                write (31,153) tmp * .5_dfloat * p%dz * pi ** 3 / p%k0 ** 2
             end if
          end do
       end if

       ! Raw fields (analyze later in IDL; this makes output procedures
       ! less clumsy and time-consuming, while allowing broader analyzis
       ! possibilities), split in one file per output.
       ! Replaces also movu and movb, which are deprecated
       ! Replaces old specu and specb (which were not avergaed along z)
       if (outflags%rawzp) &
            call write_simstate (zp, zm, p, outflags, outrecords, t, ti)

       ! Time of output of simulation state or spectra
       if (p%mpi%me .eq. p%mpi%root) then
          if (outflags%rawzp .or. outflags%specu .or. outflags%specb) &
               write (103,153) t
       end if

       outrecords%spec = outrecords%spec + 1
       tloc = 0
       spavu = 0
       spavb = 0
    end if

    ! In the end: output averages and structure functions
    if (present (lasttime) .and. lasttime) then
       if (p%mpi%me .eq. p%mpi%root) &
            print*, "Writing averages and structure functions"

       ! Output velocity structure function
       if (outflags%sfunu) then
          sfu = sfu / real (nsfun, kind=dfloat)
          ! root processor gets structure functions from all processors,
          ! and write them
          do q = 1, outflags%sfunnb
             if (p%mpi%me .eq. p%mpi%root) then
                do ip=0, p%mpi%np-1
                   if (ip .eq. p%mpi%root) then
                      stmp = sfu(:,:,q)
                   else
                      zlen = (p%nmax - p%nmin + 1) * allnz (ip)
                      call mpi_recv (stmp, zlen, mpi_double_precision, &
                           ip, 7, mpi_comm_world, status, ierr)
                   end if
                   write (35,153) stmp(:,0:allnz(ip)-1)
                end do
             else
                zlen = (p%nmax - p%nmin + 1) * p%nz
                call mpi_send (sfu(0,0,q), zlen, mpi_double_precision, &
                     p%mpi%root,  7, mpi_comm_world, ierr)
             end if
          end do
       end if

       ! Output magnetic field structure function
       if (outflags%sfunb) then
          sfb = sfb / real (nsfun, kind=dfloat)
          ! root processor gets structure functions from all processors,
          ! and write them
          do q = 1, outflags%sfunnb
             if (p%mpi%me .eq. p%mpi%root) then
                do ip=0, p%mpi%np-1
                   if (ip .eq. p%mpi%root) then
                      stmp = sfb(:,:,q)
                   else
                      zlen = (p%nmax - p%nmin + 1) * allnz (ip)
                      call mpi_recv (stmp, zlen, mpi_double_precision, &
                           ip, 8, mpi_comm_world, status, ierr)
                   end if
                   write (36,153) stmp(:,0:allnz(ip)-1)
                end do
             else
                zlen = (p%nmax - p%nmin + 1) * p%nz
                call mpi_send (sfb(0,0,q), zlen, mpi_double_precision, &
                     p%mpi%root,  8, mpi_comm_world, ierr)
             end if
          end do
       end if

       ! Output average of heating profile
       if (outflags%profheat) then 
          do i = 0, p%nz - 1 
             profheat(i) = profheat(i) * rho(i) * pi ** 3 / (p%k0 ** 2  * nprofheat)
          end do
          ! root processor gets profiles from all processors, and write them
          if (p%mpi%me .eq. p%mpi%root) then
             ! open file (replace existing file)
             open (55, file='out_profheat', form='formatted')
                do ip=0, p%mpi%np-1
                if (ip .eq. p%mpi%root) then
                   rtmp = profheat
                else
                   zlen = allnz (ip)
                   call mpi_recv (rtmp, zlen, mpi_double_precision, &
                        ip, 8, mpi_comm_world, status, ierr)
                end if
                write (55,153) rtmp(0:allnz(ip)-1)
             end do
             close (55)
          else
             zlen = p%nz
             call mpi_send (rtmp, zlen, mpi_double_precision, &
                     p%mpi%root,  8, mpi_comm_world, ierr)
          end if
          
       end if

       if (isallocatedsfun) then
          deallocate (sfu, stat=aerr)
          if (aerr .ne. 0) stop 'Allocation error'
          deallocate (sfb, stat=aerr)
          if (aerr .ne. 0) stop 'Allocation error'
          isallocatedsfun = .FALSE.
       end if
       outrecords%sfun = outflags%sfunnb
    end if

  end subroutine output_data_b



!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Close files used for output
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine output_close_files (p, outflags)
!-
    implicit none
    type(params),    intent(in) :: p
    type(saveflags), intent(in) :: outflags

    if (p%mpi%me .eq. p%mpi%root) then
       if (outflags%time)     close (10)
       if (outflags%taunl)    close (101)
       if (outflags%rawzp)    close (102)
       if (outflags%rawzp .or. outflags%specu .or. outflags%specb) &
            close (103)
       if (outflags%enmode02) close (11)
       if (outflags%enmode08) close (12)
       if (outflags%enmode14) close (13)
       if (outflags%enmode20) close (14)
       if (outflags%entotu)   close (20)
       if (outflags%entotb)   close (21)
       if (outflags%dissu)    close (22)
       if (outflags%dissb)    close (23)
       if (outflags%forcp)    close (24)
       if (outflags%crosshel) close (26)
       if (outflags%maghel)   close (27)
       if (outflags%specu)    close (30)
       if (outflags%specb)    close (31)
       if (outflags%sfunu)    close (35)
       if (outflags%sfunb)    close (36)
    end if

  end subroutine output_close_files

end module iodata
