!+
!***********************************************************
! Module nonlin-nlsabra
!***********************************************************
! Integration of non-linear terms through 2D non-local shell-models
! (adapted from the 3D non-local shell-models)
!
! The non-locality exponent alpha is hard-coded in derivs2d()
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  03 Oct 12: EB Created (forked from nonlin-nlshell)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module nonlin
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_nonlin = "nlsabra"

contains 

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Perform one step of 3rd order Runge-Kutta method integration
! Tesi di laurea, Marco Ghiglione, p 38
! Version for shell-loop: deals with 2D fields, so that Alfven-wave
! propagation can be directly handled by the routine computing the
! right handside of the differential equation (derivs2d)
! MPI: OK (no need)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine integrate_step_nonlin (zp, zm, k, p)
!-
    implicit none
    type(params),         intent(in)                              :: p
    complex(kind=dfloat), intent(inout), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                     :: zp, zm
    complex(kind=dfloat), intent(in),    dimension(p%nmin:p%nmax) :: k

    ! temporary arrays for numerical scheme
    complex(kind=dfloat), dimension(:,:), allocatable, save :: vp, vm, &
         gp, gm, tp, tm
    ! are these temporary arrays already allocated?
    logical, save :: isallocated = .FALSE.

    if (.NOT. isallocated) then
       allocate (vp(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (vm(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (gp(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (gm(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (tp(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (tm(p%nmin:p%nmax, 0:p%nz-1), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       isallocated = .TRUE.
    end if

    vp = zp
    vm = zm
    call derivs2d (gp, vp, vm, k, p, +1)
    call derivs2d (gm, vm, vp, k, p, -1)
    vp = vp + (p%delta_t / 3._dfloat) * gp
    vm = vm + (p%delta_t / 3._dfloat) * gm
    call derivs2d (tp, vp, vm, k, p, +1)
    call derivs2d (tm, vm, vp, k, p, -1)
    gp = tp - (5._dfloat / 9._dfloat) * gp
    gm = tm - (5._dfloat / 9._dfloat) * gm
    vp = vp + (15._dfloat / 16._dfloat * p%delta_t) * gp
    vm = vm + (15._dfloat / 16._dfloat * p%delta_t) * gm
    call derivs2d (tp, vp, vm, k, p, +1)
    call derivs2d (tm, vm, vp, k, p, -1)
    gp = tp - (153._dfloat / 128._dfloat) * gp
    gm = tm - (153._dfloat / 128._dfloat) * gm
    zp = vp + (8._dfloat / 15._dfloat * p%delta_t) * gp
    zm = vm + (8._dfloat / 15._dfloat * p%delta_t) * gm

  end subroutine integrate_step_nonlin



!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Right-hand-side of ODEs (=dzs/dt)
! dzsdt: derivative (RHS of ODE) corresponding to zps
! zps, zms: Elsasser-like spectra
! dir: +1 if zps is z+ and zms z-, -1 if zps is z- and zms is z+
! Version in 2D
! MPI: OK (no need, as long as no propagation is done here)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine derivs2d (dzsdt, zps, zms, k, p, dir)
    use atmosphere
!-
    implicit none
    type(params),      intent(in) :: p
    ! dzsdt is the numerically computed right-handside of the partial 
    ! differential equation of the model, for zp (if dir==1) or for
    ! zm (if dir==-1)
    complex(kind=dfloat), intent(out), &
         dimension(p%nmin:p%nmax, 0:p%nz-1) :: dzsdt
    ! zps is zp if dir==1, zm if dir==-1
    ! zms is zm if dir==1, zp if dir==-1
    complex(kind=dfloat), intent(in),  &
         dimension(p%nmin:p%nmax, 0:p%nz-1) :: zps, zms
    complex(kind=dfloat), intent(in),  &
         dimension(p%nmin:p%nmax) :: k
    ! direction flag:
    ! 1 if computing d zp / dt, -1 if computing d zm / dt
    integer, intent(in) :: dir
    real(kind=dfloat), parameter :: alpha = -2.5_dfloat ! non-locality exponent

    ! plane index
    integer :: iz
    ! nonlocal separation index
    integer              :: m
    ! values of coefficients and local copy p%lambda
    real   (kind=dfloat) :: a1, b1, a2, b2, a3, b3, l
    l = p%lambda

    ! dissipation coefficient along z
    ! real (kind=dfloat) :: nuz

    dzsdt = 0
    !!! Non-linear term
    ! using default dim=1 argument for eoshift (implicit for clarity):
    !  the arrays are shifted in the perp direction so that there are
    !  non-linear interactions with the neighboring shells

    !!! Non-local Sabra
    do m = 1, p%nmax - p%nmin

       a1 = l ** m * (l + 1._dfloat)
       a2 = (l ** (-2 * m) - l ** 2) / (l - 1._dfloat)
       a3 = (l ** (-2 * m) - 1._dfloat) / (l * (l - 1._dfloat))

       b1 = l ** (-m) / (l - 1._dfloat)
       b2 = -1._dfloat / (l - 1._dfloat)
       b3 = -l / (l - 1._dfloat)

      dzsdt  = dzsdt                                                 &
      & +(((p%k0 * l **(m-1)) ** alpha) / (l * (l + 1))) * (       &
      & + conjg (eoshift (zps,  m)) * eoshift (zms, m+1) * (a1-b1)   &
      & + conjg (eoshift (zms,  m)) * eoshift (zps, m+1) * (a1+b1)   &
      & + conjg (eoshift (zps, -m)) * eoshift (zms, 1)   * (a2-b2)   &
      & + conjg (eoshift (zms, -m)) * eoshift (zps, 1)   * (a2+b2)   &
      & + eoshift (zps, -m - 1)     * eoshift (zms, -1)  * (a3-b3)   &
      & + eoshift (zms, -m - 1)     * eoshift (zps, -1)  * (a3+b3)   )
    end do

    ! multiply by i k / 2  in each plane
    ! TODO: find a more efficient way to do it (intrisic function?)
    do iz=0, p%nz - 1
       dzsdt(:,iz) = (dzsdt(:,iz) * k * kaexp(iz)) * (0._dfloat, .5_dfloat)
    end do

  end subroutine derivs2d

end module nonlin
