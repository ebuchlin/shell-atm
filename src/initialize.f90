!+
!***********************************************************
! Module initialize
!***********************************************************
! Initialization of fields, of atmosphere, of wavenumbers and
! of random numbers
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  22 Oct 05: EB Created (moved from shell-atm)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module initialize
  use mpi
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_initialize = "default"

contains

!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Initialization of shells values
! MPI: OK (no need, but be careful when changing initial field)
! May be deprecated if we can write the initial fields in simstate.dat
! by an external (IDL?) program
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine init_shells (zp, zm, k, p)
    use diagnostics
!-
    implicit none
    type(params),         intent(in)                            :: p
    complex(kind=dfloat), intent(out), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                   :: zp, zm
    complex(kind=dfloat), intent(in),  dimension(p%nmin:p%nmax) :: k

    complex(kind=dfloat), dimension(p%nmin:p%nmax, 0:p%nz-1) :: u, b
    real   (kind=dfloat), dimension(p%nmin:p%nmax, 0:p%nz-1) :: tmp

    real   (kind=dfloat), dimension(p%nmin:p%nmax)           :: tmp1d

    integer :: iz

    u  = 0._dfloat
    b  = 0._dfloat
    zp = 0._dfloat
    zm = 0._dfloat

!!! Choose one of these initial spectra
 
    if (.TRUE.) then
!!! power-law with random phase ("well-developped turbulence" (?))
!!! (phase could be more random) in whole box
       call random_number (harvest=tmp)
       u =     (tmp - 0.5_dfloat) * (1._dfloat, 0._dfloat)
       call random_number (harvest=tmp)
       u = u + (tmp - 0.5_dfloat) * (0._dfloat, 1._dfloat)
       call random_number (harvest=tmp)
       b =     (tmp - 0.5_dfloat) * (1._dfloat, 0._dfloat)
       call random_number (harvest=tmp)
       b = b + (tmp - 0.5_dfloat) * (0._dfloat, 1._dfloat)
       do iz = 0, p%nz - 1
          u(:, iz) = u(:, iz) / (abs (u(:, iz)) + 1e-30_dfloat) * &
               (k * kaexp(iz)) ** p%initslope * p%initamp
          b(:, iz) = b(:, iz) / (abs (b(:, iz)) + 1e-30_dfloat) * &
               (k * kaexp(iz)) ** p%initslope * p%initamp
       end do
!!! power-law with random phase ("well-developped turbulence" (?))
!!! in photospheric planes
!!! Caution: not yet parallelized (the boundary of each processor domain
!!! is currently considered as a boundary plane)
!!! Power-law spectrum for u with random phase
    !       call random_number (harvest=tmp1d)
    !       u(:, 0) =           (tmp1d - 0.5_dfloat) * (1._dfloat, 0._dfloat)
    !       call random_number (harvest=tmp1d)
    !       u(:, 0) = u(:, 0) + (tmp1d - 0.5_dfloat) * (0._dfloat, 1._dfloat)
    !       u(:, 0) = u(:, 0) / abs (u(:, 0)) * &
    !                    (k * kaexp(iz)) ** p%initslope * p%initamp
    !       call random_number (harvest=tmp1d)
    !       u(:, p%nz-1) =     (tmp1d - 0.5_dfloat) * (1._dfloat, 0._dfloat)
    !       call random_number (harvest=tmp1d)
    !       u(:, p%nz-1) = u(:, p%nz-1) + (tmp1d - 0.5_dfloat) * (0._dfloat, 1._dfloat)
    !       u(:, p%nz-1) = u(:, p%nz-1) / abs (u(:, p%nz-1)) * &
    !                    (k * kaexp(iz)) ** p%initslope * p%initamp


!!! b deduced from u for "zero" initial cross helicity
    !      call random_number (harvest=tmp)
    !      b =     (tmp - 0.5_dfloat) * (1._dfloat, 0._dfloat)
    !      call random_number (harvest=tmp)
    !      b = b + (tmp - 0.5_dfloat) * (0._dfloat, 1._dfloat)
    !      b = b / (abs (b) * k2d ** (5. / 6.) * 10000._dfloat)

!!! b deduced from u for intermediate initial cross helicity
    !       b = u / abs (u)
    !       call random_number (harvest=tmp)
    !       b = b + (tmp - 0.5_dfloat) * (1e-2_dfloat, 0._dfloat)
    !       call random_number (harvest=tmp)
    !       b = b + (tmp - 0.5_dfloat) * (0._dfloat, 1e-2_dfloat)
    !       b = b / abs (b) * k2d ** p%initslope * p%initamp / 2._dfloat

!!! Power-law spectrum for b with random phase (independent from u) 
    !        b(:, 0) = u(:, 0) / abs (u(:, 0))
    !        call random_number (harvest=tmp1d)
    !        b(:, 0) = b(:, 0) + (tmp1d - 0.5_dfloat) * (1e-2_dfloat, 0._dfloat)
    !        call random_number (harvest=tmp1d)
    !        b(:, 0) = b(:, 0) + (tmp1d - 0.5_dfloat) * (0._dfloat, 1e-2_dfloat)
    !        b(:, 0) = b(:, 0) / abs (b(:, 0)) * &
    !                     (k * kaexp(iz)) ** p%initslope * p%initamp / 2._dfloat
    !        b(:, p%nz-1) = u(:, p%nz-1) / abs (u(:, p%nz-1))
    !        call random_number (harvest=tmp1d)
    !        b(:, p%nz-1) = b(:, p%nz-1) + (tmp1d - 0.5_dfloat) * (1e-2_dfloat, 0._dfloat)
    !        call random_number (harvest=tmp1d)
    !        b(:, p%nz-1) = b(:, p%nz-1) + (tmp1d - 0.5_dfloat) * (0._dfloat, 1e-2_dfloat)
    !        b(:, p%nz-1) = b(:, p%nz-1) / abs (b(:, p%nz-1)) * &
    !                     (k * kaexp(iz)) ** p%initslope * p%initamp / 2._dfloat

    !   print*, minval (abs(b)), maxval (abs(b))
    !   print*, minval (abs(u)), maxval (abs(u))
       call ub_to_zpm (u, b, zp, zm, p)
    endif


!!! for tests only
!!! a few well separated (non-interacting) modes close to the boundary planes (of each processor domain)
    if (.FALSE.) then
        zp (2 , 1:5       ) = p%initamp * (1._dfloat, 0._dfloat)
        zp (10, 1:5       ) = p%initamp * (1._dfloat, 0._dfloat)
        zp (15, 1:5       ) = p%initamp * (1._dfloat, 0._dfloat)
        zp (20, 1:5       ) = p%initamp * (1._dfloat, 0._dfloat)
        zm (5 , p%nz-6:p%nz - 2) = p%initamp * (0._dfloat, 1._dfloat)
        zm (10, p%nz-6:p%nz - 2) = p%initamp * (0._dfloat, 1._dfloat)
        zm (15, p%nz-6:p%nz - 2) = p%initamp * (0._dfloat, 1._dfloat)
        zm (20, p%nz-6:p%nz - 2) = p%initamp * (0._dfloat, 1._dfloat)
    endif


!!! constant phase zp and zm, with spectrum slope (for deterministic tests)
    !      do iz = 0, p%nz - 1
    !         zp(:,iz) = (1._dfloat, 0._dfloat) * p%initamp * &
    !                         (k * kaexp(iz)) ** p%initslope
    !         zm(:,iz) = (0._dfloat, 1._dfloat) * p%initamp * &
    !                         (k * kaexp(iz)) ** p%initslope
    !      end do

    !!! to check initialization for debug
    !if (p%mpi%me .lt. 4) then
    !   do i = 0, p%nz-1
    !      write(110+p%mpi%me,*) zp(0, i), zm(0, i)
    !   enddo
    !end if
  end subroutine init_shells



!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Return in array k the values of the wavenumbers for each
! index value in the perpendicular direction
! Initialize random number generator
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine get_wavenumbers (k, p)
!-
    implicit none
    type(params),         intent(in)                            :: p
    complex(kind=dfloat), intent(out), dimension(p%nmin:p%nmax) :: k 

    integer                                                     :: i, n, now(8)

    integer, dimension(:), allocatable :: randseed

    ! get size of seed and current seed
    call random_seed (size=n)
    allocate (randseed (n))
    call random_seed (get=randseed)

    ! alter seed, depending on current time and processor (try to avoid
    ! having the same seed for both boundaries when on a different processor)
    ! and use it to reinitialize random number generator
    call date_and_time (values=now)
    randseed = randseed * (now(8) - 500 + p%mpi%me)
    call random_seed (put=randseed)

    ! Initialize array of wavenumbers corresponding to each shell
    k(p%nmin) = (p%k0 * (p%lambda ** p%nmin)) * (1._dfloat, 0._dfloat)
    do i = p%nmin + 1, p%nmax
       k(i) = k(i - 1) * p%lambda
    end do

  end subroutine get_wavenumbers

end module initialize
