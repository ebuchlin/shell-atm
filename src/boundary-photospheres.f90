!+
!***********************************************************
! Module boundary-photospheres
!***********************************************************
! Variables and routines for boundary conditions and forcing
! For a photosphere on both sides of the box
!
! Authors:
!  AV   Andrea Verdini
!  EB   Eric Buchlin
!
! Modifications history:
!  07 Nov 05 EB Created
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module boundary
  use types
!-
  implicit none

  ! Identification of module alternative
  character (len=*), parameter :: module_boundary = "photospheres"

contains


!+
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Boundary conditions (reflection and forcing)
! A velocity is imposed, which leads to partial reflection
! MPI: OK (done)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  subroutine force (zp, zm, k, p, t)
    use mpi
    use atmosphere
    use diagnostics
!-
    implicit none
    type(params),         intent(inout)                           :: p
    complex(kind=dfloat), intent(inout), &
         dimension(p%nmin:p%nmax, 0:p%nz-1)                     :: zp, zm
    complex(kind=dfloat), intent(in),    dimension(p%nmin:p%nmax) :: k
    real(kind=dfloat), intent(in) :: t

    ! boundary conditions on velocity (forcing)
    complex(kind=dfloat), dimension(:), save, allocatable :: u1, u2

    ! amplitudes of forcing velocity for the 3 forced modes
!     complex(kind=dfloat), dimension(3), save      :: a1, b1, a2, b2
    real(kind=dfloat), dimension(0:nkforce-1)               :: tmp1
    real(kind=dfloat)                             :: tmp
    ! are u1 and u2 already allocated?
    logical, save :: isallocated = .FALSE.
    ! MPI variables
    integer           :: status(mpi_status_size)

    if (.NOT. isallocated) then
       allocate (u1(p%nmin:p%nmax), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       allocate (u2(p%nmin:p%nmax), stat=aerr)
       if (aerr .ne. 0) stop 'Allocation error'
       u1 = 0; u2 = 0
       isallocated = .TRUE.
    end if

    if (p%mpi%me .eq. 0 .or. p%mpi%me .eq. p%mpi%np-1) then
       ! p%tstar is the time after which the phase of one component
       ! of forcing should be changed
       if (modulo (t, p%tstar) .lt. p%delta_t) then
          ! change complex phase of 1st component, for both boundaries
          if (p%mpi%me .eq. p%mpi%root) then
             call random_number (harvest=tmp1)
             ! indices must match the size of tmp1
             p%fc%a1 = exp (cmplx (0._dfloat, tmp1 * pi * 2._dfloat))
          end if
          if (p%mpi%me .eq. p%mpi%np-1) then
             call random_number (harvest=tmp1)
             p%fc%a2 = exp (cmplx (0._dfloat, tmp1 * pi * 2._dfloat))
          end if
       else if (modulo (t + p%tstar/2._dfloat, p%tstar) .lt. p%delta_t) then
          ! change complex phase of 2nd component, for both boundaries
          if (p%mpi%me .eq. p%mpi%root) then
             call random_number (harvest=tmp1)
             p%fc%b1 = exp (cmplx (0._dfloat, tmp1 * pi * 2._dfloat))
          end if
          if (p%mpi%me .eq. p%mpi%np-1) then
             call random_number (harvest=tmp1)
             p%fc%b2 = exp (cmplx (0._dfloat, tmp1 * pi * 2._dfloat))
          end if
       end if

       ! set forcing (velocity field at boundaries)
       u1(2:2+nkforce-1) = (sin (pi * t / p%tstar) ** 2 * p%fc%a1 + &
            sin (pi * (t / p%tstar + .5_dfloat)) ** 2 * p%fc%b1) * p%forcamp
       ! same kind of forcing on 2nd boundary
       u2(2:2+nkforce-1) = (sin (pi * t / p%tstar) ** 2 * p%fc%a2 + &
            sin (pi * (t / p%tstar + .5_dfloat)) ** 2 * p%fc%b2) * p%forcamp

       ! apply this forcing by imposing the velocity field,
       ! and compute power of forcing
       if (p%mpi%me .eq. 0) then
          zp(:, 0)      = -zm(:, 0)      + 2._dfloat * u1
          p%forcp = energy_tot1d (zp(:,0), p) - &
               energy_tot1d (zm(:,0), p)
          ! Use also aexp?
          p%forcp = p%forcp * rho(0) * va(0)
          if (p%mpi%root .ne. 0) then
             call mpi_send (p%forcp, 1, mpi_double_precision, &
                  p%mpi%root, 10, mpi_comm_world, ierr)
          end if
       end if
       if (p%mpi%me .eq. p%mpi%np-1) then
          zm(:, p%nz-1) = -zp(:, p%nz-1) + 2._dfloat * u2
          tmp = energy_tot1d (zm(:,p%nz-1), p) - &
               energy_tot1d (zp(:,p%nz-1), p)
          ! Use also aexp?
          tmp = tmp * rho(p%nz-1) * va(p%nz-1)
          if (p%mpi%root .ne. p%mpi%np-1) then
             call mpi_send (tmp, 1, mpi_double_precision, &
                  p%mpi%root, 11, mpi_comm_world, ierr)
          end if
       end if
    end if
    if (p%mpi%me .eq. p%mpi%root) then ! does not assume that root is 0
       if (p%mpi%root .ne. 0) then
          call mpi_recv (p%forcp, 1, mpi_double_precision, &
               0,          10, mpi_comm_world, status, ierr)
       end if
       if (p%mpi%root .ne. p%mpi%np-1) then
          call mpi_recv (tmp,     1, mpi_double_precision, &
               p%mpi%np-1, 11, mpi_comm_world, status, ierr)
       end if
       p%forcp = (p%forcp + tmp) * pi ** 3 / p%k0 ** 2
    end if

  end subroutine force

end module boundary
