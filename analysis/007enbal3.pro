; verify energy balance for 007enbal-nodiss-nonl2, using debug output
; in fort.990 and fort.991

if n_elements(do_ps) eq 0 then do_ps = 0
if do_ps then begin
    set_plot, 'PS'
    device, filename='007enbal3.eps', encapsulated=1, xsize=15, ysize=12
endif

pushd, buchlin_basedir + $
  'berenice/numerics/shell-atm/runs/007enbal-nodiss-nonl2'
sa_plot_energy, /tot
p = sa_params_get ()
readarray, 'fort.990', p.nrecen, entot
readarray, 'fort.991', p.nrecen, entot2
readarray, 'out_time', p.nrecen, t
readarray, 'out_forcp', p.nrecen, f
ft = total (f, /cumulative) * (t[1]-t[0])
popd
pause
oplot, t, entot, color=255
pause
oplot, t, entot2, color=255L*256
pause
oplot, t, ft+entot[0], color=255L*65536L

if do_ps then begin
    device, /close
    set_plot, 'X'
endif
end
