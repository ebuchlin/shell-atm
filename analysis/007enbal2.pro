; verify energy balance, computing energy by different means (eb+eu,
; or energy contained in fields of out_simstate_nnn)

if n_elements(do_ps) eq 0 then do_ps = 0
if do_ps then begin
    set_plot, 'PS'
    device, filename='007enbal2.eps', encapsulated=1, xsize=15, ysize=12
endif

goto, dontread
pushd, buchlin_basedir + $
  'berenice/numerics/shell-atm/runs/007enbal-nodiss-nonl2'
p = sa_params_get ()

; first method: eu + eb (and get also other useful arrays)
readarray, 'out_time', p.nrecen, t
readarray, 'out_enu', p.nrecen, eu
readarray, 'out_enb', p.nrecen, eb
readarray, 'out_dissu', p.nrecen, du
readarray, 'out_dissb', p.nrecen, db
readarray, 'out_forcp', p.nrecen, df
dt = (shift (t, -1) - t)[0:p.nrecen-3]
e1 = (eu + eb)[0:p.nrecen-3]
d  = (du + db)[0:p.nrecen-3]
f  =        df[1:p.nrecen-2]
dedt1 = (shift (eu+eb, -1) - (eu+eb))[0:p.nrecen-3] / dt

if p.nrecen ne p.nrecsp then message, 'This comparison assumes nrecen=nrecsp'
; other methods: from out_simstate_nnn
; assumes rho=1, aexp=1
readarray_s, 'out_time2i', p.nrecsp, t2i
pp = replicate (p, p.nrecsp)
e2 = dblarr (p.nrecsp)
e3 = dblarr (p.nrecsp)
e4 = dblarr (p.nrecsp)
e5 = dblarr (p.nrecsp)
e6 = dblarr (p.nrecsp)
f2 = dblarr (p.nrecsp)
for i=0, p.nrecsp-1 do begin
    pp[i] = sa_params_get ('out_simstate_' + t2i[i], shells=z)
    ; energy of zp + energy of zm
    e2[i] = total (abs (z[*, *, *]) ^ 2)
    ; or: with removing zm(0) and zp(nz-1)
    e3[i] = e2[i] - $
      (total (abs (z[*, 0, 1]) ^ 2) + total (abs (z[*, p.nz-1, 0]) ^ 2))
    ; or: counting boundaries for half
    e4[i] = e2[i] - .5d * total (abs( z[*, [0, p.nz], *]) ^ 2)
    ; or: get u and b and their energies
    u = sa_shells_to_fields (z, /uu)
    b = sa_shells_to_fields (z, /bb)
    e5[i] = total (abs (u) ^ 2) + total (abs (b) ^ 2)
    ; or: count u and b energy half at boundaries (should be the same than e1)
    e6[i] = e5[i] - .5d * (total (abs (u[*, [0, p.nz-1]]) ^ 2) + $
                           total (abs (b[*, [0, p.nz-1]]) ^ 2))
    e2[i] *= .25d * !pi^3 / p.k0^2 * p.dz
    e3[i] *= .25d * !pi^3 / p.k0^2 * p.dz
    e4[i] *= .25d * !pi^3 / p.k0^2 * p.dz
    e5[i] *=  .5d * !pi^3 / p.k0^2 * p.dz
    e6[i] *=  .5d * !pi^3 / p.k0^2 * p.dz
    ; power of forcing, computed from out_simstate_nnn (assume dt
    ; constant, va=1, and no forcing at 2nd boundary)
    f2[i] = total (abs (z[*, 0, 0]) ^ 2 - abs (z[*, 0, 1]) ^ 2)
    f2[i] *= .25d * !pi^3 / p.k0^2 * 1.d
endfor
dedt2 = (shift (e2, -1) - e2)[0:p.nrecen-3] / dt
dedt3 = (shift (e3, -1) - e3)[0:p.nrecen-3] / dt
dedt4 = (shift (e4, -1) - e4)[0:p.nrecen-3] / dt
dedt5 = (shift (e5, -1) - e5)[0:p.nrecen-3] / dt
dedt6 = (shift (e6, -1) - e6)[0:p.nrecen-3] / dt
e2 = e2[0:p.nrecen-3]
e3 = e3[0:p.nrecen-3]
e4 = e4[0:p.nrecen-3]
e5 = e5[0:p.nrecen-3]
e6 = e6[0:p.nrecen-3]
popd
dontread:

tt = t - t[0]
; e1a = alog10 (abs (e1-e1[0]))
; e2a = alog10 (abs (e2-e1[0]))
; e3a = alog10 (abs (e3-e1[0]))
; e4a = alog10 (abs (e4-e1[0]))
; e5a = alog10 (abs (e5-e1[0]))
; e6a = alog10 (abs (e6-e1[0]))

e1a = e1-e1[0]
e2a = e2-e1[0]
e3a = e3-e1[0]
e4a = e4-e1[0]
e5a = e5-e1[0]
e6a = e6-e1[0]

plot, tt, e1a , xstyle=1, ystyle=3, yrange=1e-8*[-3, 1]
pause
oplot, tt, e2a, color=128
pause
oplot, tt, e3a, color=255
pause
oplot, tt, e4a, color=128*256L
pause
oplot, tt, e5a, color=255*256L
pause
oplot, tt, e6a, color=128L*65536L
pause

; plot,  tt, e1 - e1[0], xstyle=1, ystyle=3, yrange=1e-8*[-1, 1]
; pause
; oplot, tt, e4 - e1[0], color=128*256L
; pause
; oplot, tt, e6 - e6[0], color=128L*65536L
; pause

; plot,  tt, e2 - e2[0], xstyle=1, ystyle=3, yrange=1e-8*[-1, 1]
; pause
; oplot, tt, e5 - e2[0], color=128*256L
; pause

;     plot, t, dedt, yrange=yrange[*, id], xstyle=1, ystyle=3
;     oplot, t, -d, color=255L
;     oplot, t, f / 10, color=255L*256L
;     pause

if do_ps then begin
    device, /close
    set_plot, 'X'
endif
end
