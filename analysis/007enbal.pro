; verify energy balance

if n_elements(do_ps) eq 0 then do_ps = 0
if do_ps then begin
    set_plot, 'PS'
    device, filename='007enbal.eps', encapsulated=1, xsize=15, ysize=12
endif

dir = ['-nodiss', '-noforc', '-nodiss-noforc', '-nodiss-nonl']
yrange = [[1e-6 * [-5, .5]], $
          [1e-6 * [-5, .5]], $
          [1e-6 * [-5, .5]], $
          [1e-6 * [-5, .5]], $
          [1e-6 * [-5, .5]]]
nd = n_elements(dir) 

for id=0, nd-1 do begin
    pushd, buchlin_basedir + 'berenice/numerics/shell-atm/runs/007enbal' $
      + dir[id]
    print, dir[id]
    p = sa_params_get ()
    readarray, 'out_time', p.nrecen, t
    readarray, 'out_enu', p.nrecen, eu
    readarray, 'out_enb', p.nrecen, eb
    readarray, 'out_dissu', p.nrecen, du
    readarray, 'out_dissb', p.nrecen, db
    readarray, 'out_forcp', p.nrecen, df
    dt = (shift (t, -1) - t)[0:p.nrecen-3]
    e = (eu + eb)[0:p.nrecen-3]
    d = (du + db)[0:p.nrecen-3]
    f = df[1:p.nrecen-2]
    dedt = (shift (eu+eb, -1) - (eu+eb))[0:p.nrecen-3] / dt
    popd

    plot, t, dedt, yrange=yrange[*, id], xstyle=1, ystyle=3
    oplot, t, -d, color=255L
    oplot, t, f / 10, color=255L*256L
    pause
endfor

if do_ps then begin
    device, /close
    set_plot, 'X'
endif
end
