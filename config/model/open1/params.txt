# nmin
0
# nmax
17
# nz
1000
# k0
62.831853
# lambda
2
# nu
1.e-10
# eta
1.e-10
# eps
1.25
# epsm
-.3333333333333333333
# b0
1.
# dz distance between planes
.001
# nt
-.02
# initial delta_t
0.00001
# continue from saved state
0
# initial amplitude
1e-3
# initial slope
-.3333333333333
# forcing amplitude
1e-3
# tstar correlation time of forcing
200.3124
# rho0 density at base
1.e-19
# h scale height
2.
