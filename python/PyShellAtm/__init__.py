# -*- coding: utf-8 -*-
'''
Analysis tools for Shell-Atm simulations results

Licence: GPLv3
Author: Eric Buchlin, eric.buchlin@ias.u-psud.fr

@package PyShellAtm
'''

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import os

class params (dict):
  '''Shell-Atm parameters'''

  def __init__ (self, fileName='simstate.dat', dir='.'):
    '''Initialize parameters
    @param fileName: simulation state file
    '''
    
    if fileName is not None:
      p, f = read_simstate (fileName, dir=dir)
      self = p # TODO does not seem to work
    else: return
    self['nk']   = self['nmax'] - self['nmin'] + 1
    self['kmin'] = self['k0'] * self['lambda'] ** self['nmin']
    self['kmax'] = self['k0'] * self['lambda'] ** self['nmax']

  def logkperp (self):
    '''@return Array of kperp values corresponding to field array indices (first dimension)'''
    return np.log10 (self['k0']) + np.log10 (self['lambda']) * np.arange (self['nmin'], self['nmax'] + 1)

  def z (self):
    ''''@return Array of position values corresponding to field array indices (second dimension)'''
    raise RuntimeError('z should be computed from simstate')

  def __print__ (self):
    print 'ik: ', self['nmin'], ' - ', self['nmax'], '   k0: ', self['k0'], '   lam: ', self['lambda']
    print 'nz: ', self['nz'], '   dz: ', self['dz'], '   b0: ', self['b0']
    print 'nu: ', self['nu'], 'eta: ', self['eta']
    print 'eps: ', self['eps'], 'epsm: ', self['epsm']
    print 'nt: ', self['nt'], '   nrecen: ', self['nrecen'], '   nrecsf: ', self['nrecsf'], '   nrecsp: ', self['nrecsp']




class fields (object):
  '''Shell-Atm fields'''

  def __init__ (self, fileName='simstate.dat', dir='.'):
    '''Read fields from a simulation state file
    @param fileName: simulation state file
    '''
    if fileName is not None:
      p, self = read_simstate (fileName, dir)
    else: return

  def u(self):
    '''@return Velocity field'''
    return (self.zp + self.zm) / 2.

  def b(self):
    '''@return Magnetic field'''
    return (self.zp - self.zm) / 2.
  
  def ep (self):
    '''@return Energy of z+ field'''
    return np.abs (self.zp) ** 2 / 2.

  def em (self):
    '''@return Energy of z- field'''
    return np.abs (self.zm) ** 2 / 2.

  def eu (self):
    '''@return Kinetic energy field'''
    return np.abs (self.zp + self.zm) ** 2 / 4.

  def eb (self):
    '''@return Magnetic energy field'''
    return np.abs (self.zp - self.zm) ** 2 / 4.

  def ch (self):
    '''@return Cross-helicity field'''
    ep = self.ep()
    em = self.em()
    return (ep - em) / (ep + em)

  def ef (self):
    '''@return Energy flux (assumes unity density and Alfvén speed)'''
    return self.ep() - self.em()

  def b1 (self):
    '''@return Brms (used to compute Biskamp term)'''
    b1 = np.zeros_like (self.zp, dtype=np.float64)
    nk = b1.shape[0]
    for ik in range (3, nk):
      b1[ik,:] = b1[ik-1,:] + np.real (self.zp[ik-1,:] - self.zm[ik-1,:])
    return b1

class simstate (object):
  '''Shell-Atm simulation state: parameters and fields'''
  
  def __init__ (self, fileName, dir='.', p=None, f=None):
    '''@brief Initialize simulation state
       @param fileName: file name to read (without directory); if None, use p and f
       @param dir: directory where to read file
       @param p: parameters
       @param f: fields
      '''
    self.dir = dir
    if fileName is not None:
      (self.p, self.f) = read_simstate (dir + '/' + fileName)
    else:
      self.p = p
      self.f = f

  def z (self):
    '''@return Array of positions (corresponding to second index of f)
    
    The positions file is read on request. Please use simstates.z if you need the positions at several times
    '''
    z2 = np.loadtxt(dir + '/out_rad')
    nz2 = len (z2)
    nz = self.p['nz']
    return z2[(nz2 // nz) * np.arange(nz)]



def read_simstate (fileName, dir='.'):
  '''Read parameters and simulation state from a file
  @param fileName Name of simulation state file (includes parameters; without directory name)
  @param dir Directory where to read file
  @return A (parameters, fields) pair
  '''
  with open (dir + '/' + fileName) as fp:
    p = params(fileName=None, dir=dir)
    f = fields(fileName=None, dir=dir)
    
    p['nmin']    = int   (fp.readline())
    p['nmax']    = int   (fp.readline())
    p['nz']      = int   (fp.readline())
    p['k0']      = float (fp.readline())
    p['lambda']  = float (fp.readline())
    p['nu']      = float (fp.readline())
    p['eta']     = float (fp.readline())
    p['eps']     = float (fp.readline())
    p['epsm']    = float (fp.readline())
    p['b0']      = float (fp.readline())
    p['dz']      = float (fp.readline())
    p['nt']      = int   (fp.readline())
    fp.readline() # skip delta t
    p['nrecen']  = int   (fp.readline())
    p['nrecsf']  = int   (fp.readline())
    p['nrecsp']  = int   (fp.readline())

    p['nk'] = p['nmax'] - p['nmin'] + 1
    data = np.fromfile (fp, count=4 * p['nk'] * p['nz'], dtype=float, sep=' ')
    data.shape = (2, p['nz'], p['nk'], 2)
    data = data.transpose()
    f.zp = data[0,:,:,0] + (0+1j) * data[1,:,:,0]
    f.zm = data[0,:,:,1] + (0+1j) * data[1,:,:,1]

    p['time']    = float (fp.readline())
    p['tstar']   = float (fp.readline())
    nkforce = int  (fp.readline())
    # skip forcing coefficients
    for i in range (8 * nkforce): fp.readline()
    p['rho0']    = float (fp.readline())
    p['h0']      = float (fp.readline())

    return p, f


def read_out_plane (fileName, p, dir='.', it=None):
  '''@return Fields in one output plane, as a function of time
    @param fileName: file name
    @param dir: Directory where to read file
    @param it: select time indices window [it[0], it[1][
  '''
  f = fields (fileName=None, dir=dir)
  with open (dir + '/' + fileName) as fp:
    data = np.fromfile (fp, dtype=float, sep=' ')
    nt = len (data) / (4 * p['nk'])
    data.shape = (nt, 2, p['nk'], 2)
    if it is not None:      # select time interval
      it1 = np.max ([it[0], 0])
      it2 = np.min ([it[1], nt])
      #print 'it corr time:', it1, it2
      data = data[it1:it2,:,:,:]
      nt = it2 - it1
    data = data.transpose()
    f.zp = data[0,:,0,:] + (0+1j) * data[1,:,0,:]
    f.zm = data[0,:,1,:] + (0+1j) * data[1,:,1,:]
  return f


class simstates (object):
  '''Shell-Atm simulation states and time series, at all output times of a run
    . t: time axis, for ch, enu, enb, forcp, taunl
    . t2: time axis, for spectra and simulation states (third dimension of zp and zm)
    . t2i: time step indices corresponding to t2 (used to label simulation state file names)
    . z: position axis (second dimension of zp and zm)
    . endp, endf: parameters and fields at end of simulation
    . p: parameters for first time-dependent field. p.logkperp() gives first dimension of zp and zm.
    . zp, zm: fields (dimensions: kperp, z, t)
  '''
  
  def __init__ (self, dir='.'):
    self.dir = dir
    # main (final) parameters and simulation state
    self.endp, self.endf = read_simstate(dir + '/simstate.dat')
    
    # time information and time series
    self.t = np.loadtxt (dir + '/out_time')
    self.ch = np.loadtxt (dir + '/out_crosshel')
    self.enu = np.loadtxt (dir + '/out_enu')
    self.enb = np.loadtxt (dir + '/out_enb')
    self.forcp = np.loadtxt (dir + '/out_forcp')
    self.taunl = np.loadtxt (dir + '/out_taunl')
    # time axis (and time indices) for spectra and simulation states
    self.t2 = np.loadtxt (dir + '/out_time2')
    with open (dir + '/out_time2i') as fp: t2i = [el.strip() for el in fp.readlines()]
    # read longitudinal axis for final simulation state
    self.endz = np.loadtxt (dir + '/out_rad')
    if len (self.endz) != self.endp['nz']:
      raise AssertionError('Length of out_rad inconsistant with simstate.dat')
    if len (self.t2) != self.endp['nrecsp']:
      raise AssertionError('Length of out_time2 inconsistant with simstate.dat')
    # read all out_simstate files and get fields
    # (nz can be different in simstate.dat, but we assume it to be the same in all out_simstate_*)
    self.p, f = read_simstate (dir + '/out_simstate_' + t2i[0])
    self.z = self.endz[(self.endp['nz'] // self.p['nz']) * np.arange (self.p['nz'])]
    self.zp = np.zeros ((self.p['nk'], self.p['nz'], self.endp['nrecsp']))
    self.zm = np.zeros ((self.p['nk'], self.p['nz'], self.endp['nrecsp']))
    for i in range(self.endp['nrecsp']):
      t2ii = t2i[i]
      pt, ft = read_simstate (fileName=dir + '/out_simstate_' + t2ii)
      if pt['nz'] != self.p['nz']:
        raise AssertionError('nz should be the same in different out_simstate_ files')
      self.zp[:, :, i] = ft.zp
      self.zm[:, :, i] = ft.zm


def plot_energy (dir='.', xlog=False, ylog=False):
  '''Plot energy time series'''
  t = np.loadtxt (dir + '/out_time')
  eu = np.loadtxt (dir + '/out_enu')
  eb = np.loadtxt (dir + '/out_enb')
  if len (t) != len (eu) or len (t) != len (eb):
    raise AssertionError('Time and energies arrays should be the same size')
  plt.plot (t, eu, color='black')
  plt.plot (t, eb, color='blue')
  plt.plot (t, eu + eb, color='red')
  plt.xlabel ('Time')
  plt.ylabel ('Energy')
  if xlog: plt.xscale ('log')
  if ylog: plt.yscale ('log')
  plt.show()


def plot_de (dir='.', xlog=False, ylog=False):
  '''Plot dissipation power time series'''
  t = np.loadtxt (dir + '/out_time')
  eu = np.loadtxt (dir + '/out_dissu')
  eb = np.loadtxt (dir + '/out_dissb')
  if len (t) != len (eu) or len (t) != len (eb):
    raise AssertionError('Time and dissipation arrays should be the same size')
  plt.plot (t, eu, color='black')
  plt.plot (t, eb, color='blue')
  plt.plot (t, eu + eb, color='red')
  plt.xlabel ('Time')
  plt.ylabel ('Dissipation power')
  if xlog: plt.xscale ('log')
  if ylog: plt.yscale ('log')
  plt.show()


def plot_dt (file='simstate.dat', it2=None, dir='.', k=True, hyper=2, biskampa=None, saveFile=None):
  '''Plot time scales as a function of time or wavenumber
  @param file: simulation state file for parameters and fields
  @param it2: index of output time (supersedes 'file' parameter)
  @param dir: directory where to read files
  @param k: time scales as a function of kperp instead of t
  @param hyper: hyperviscosity exponent (2 for usual viscosity)
  @param biskampa: coefficient for "Biskamp" perpendicular Alfvén term (for GOY-B model)
  @param outfile: output file for saving plot

  Warning: assumes that Alfven speed (read in out_va) does not depend on time.
  '''
  if k: plotk = True
  else: plotk = False

  if it2 is not None:
    t2i = np.loadtxt(dir + '/out_time2i', dtype='S')[it2]
    file = 'out_simstate_' + t2i

  # parameters are read in simstate.dat even if another simulation state file name is provided
  psim, fsim = read_simstate ('simstate.dat', dir=dir) # psim = params(file) doesn't work

  if file == 'simstate.dat': p, f = psim, fsim
  else: p, f = read_simstate (file, dir=dir)

  if it2 is None:  # simstate.dat
    it2 = p['nrecsp'] - 1

  logk = p.logkperp()
  k = 10. ** logk
  t = np.loadtxt (dir + '/out_time')
  va = np.loadtxt (dir + '/out_va')  # but va sometimes depends on time...
  aexp = np.loadtxt (dir + '/out_aexp')

  # times that are independent of (k,z,t)
  tauAlmin = np.log10 (psim['dz'] / np.max (va))
  tauAlmax = np.log10 (np.sum (psim['dz'] / va))
  tauForc  = np.log10 (psim['tstar'])

  # in va and aexp, select planes that are really part of output
  zstep = psim['nz'] / p['nz']
  iz = zstep * np.arange (p['nz'])
  va = va[iz]
  aexp = aexp[iz]

  if plotk:  # plot time scales as a function of k
    # percentile values for plotting bands corresponding to distributions of values:
    # min, p20, p40, median, p60, p80, max
    pc = [0, 5, 20, 50, 80, 95, 100]
    pcl = len (pc)
    pcl2 = pcl // 2
    tauNL = np.zeros ((p['nk'], pcl))
    bb0 = np.zeros (p['nz'])
    tauAp = np.zeros ((p['nk'], pcl))
    tauNu = np.zeros (p['nk'])

    for ik in range(p['nk']):
      # non-linear term
      alltauNL = -np.log10 (k[ik] * np.maximum (np.abs (f.zp[ik,:]), np.abs (f.zm[ik,:]))) + .5 * np.log10 (aexp)
      tauNL[ik,:] = np.percentile (alltauNL, pc)
      # Biskamp term
      if biskampa is not None:
        if ik > 2:
          bb0 += np.real (f.zp[ik-1,:] - f.zm[ik-1,:])
          alltauAp = -np.log10 (k[ik] * biskampa * np.abs (bb0 / np.sqrt(aexp)))
          tauAp[ik,:] = np.percentile (alltauAp, pc)
        else:
          tauAp[ik,:] = np.nan
      # Dissipation term
      tauNu[ik] = -np.log10 (max (p['nu'], p['eta']) * k[ik] ** hyper / np.max (aexp))

    plt.plot (logk[[0, -1]], [tauAlmin] * 2, color='black', label=r'$\tau_{A,\parallel}$')

    plt.plot (logk[[0, -1]], [tauAlmax] * 2, color='black')

    plt.plot (logk[[0, -1]], [tauForc]  * 2, color='green', label=r'$\tau_f$')

    plt.plot (logk, tauNL[:, 0], ':', color='b')
    plt.plot (logk, tauNL[:,-1], ':', color='b')
    plt.plot (logk, tauNL[:, pcl2], color='b', label=r'$\tau_{NL}$')
    for i in range (1, pcl2):
      plt.fill_between (logk, tauNL[:, i], tauNL[:,-i-1], color='b', alpha=.1)

    if biskampa is not None:
      plt.plot (logk, tauAp[:, 0], ':', color='m')
      plt.plot (logk, tauAp[:,-1], ':', color='m')
      plt.plot (logk, tauAp[:, pcl2], color='m', label=r'$\tau_{A,\perp}$')
      for i in range (1, pcl2):
        plt.fill_between (logk, tauAp[:, i], tauAp[:,-i-1], color='m', alpha=.1)

    plt.plot (logk, tauNu, color='r', label=r'$\tau_\nu$')

    # TODO experimental: plot time associated to correlation length
    if True:
      logk2, lp = get_corr_length ('zp', param=p, field=f.zp, plot=False)
      assert (np.max (np.abs (logk2 - logk)) < 1e-4)
      logk2, lm = get_corr_length ('zm', param=p, field=f.zm, plot=False)
      assert (np.max (np.abs (logk2 - logk)) < 1e-4)
      plt.plot (logk2, np.log10 ((lp + lm) / p['b0'] / 2.), color='#7f7f7f', label=r'$L_{corr}/b_0$')

    # TODO: experimental: plot correlation time over time interval around current time
    # dt for output of out_plane is hardcoded
    if False:
      dt = 1.e-5  # from types.f90
      t2 = np.loadtxt(dir + '/out_time2')
      dt2 = np.median (t2 - np.roll(t2, 1))
      it20 = (p['time'] - t2[0] - dt2) / dt
      it21 = (p['time'] - t2[0] + dt2) / dt   # there is some overlap with previous/next time interval
      logk2, lp = get_corr_time ('zp', dt, param=p, it=(it20,it21), pc=pc, plot=False)
      assert (np.max (np.abs (logk2 - logk)) < 1e-4)
      plt.plot (logk, np.log10 (lp[:,0]), ':', color='y')
      plt.plot (logk, np.log10 (lp[:,-1]), ':', color='y')
      plt.plot (logk, np.log10 (lp[:, pcl2]), color='y', label=r'$\tau_{corr}$')
      for i in range (1, pcl2):
        plt.fill_between (logk, np.log10 (lp[:, i]), np.log10 (lp[:,-i-1]), color='y', alpha=.1)

    plt.xlabel (r'$\log k_\perp$')
    plt.ylabel (r'$\log \tau$')
    plt.title (r'$t={:.2f}$'.format(p['time']))
    plt.legend()
    if saveFile is None:
      plt.show ()
    else:
      plt.ylim ((-10,10))
      plt.savefig (saveFile, bbox_inches='tight')
  else:  # plot time scales as a function of t
    tauNL = np.loadtxt (dir + '/out_taunl')

    plt.plot (t[[0, -1]], [tauAlmin] * 2, color='k', label=r'$\tau_{A,\parallel}$')
    plt.plot (t[[0, -1]], [tauAlmax] * 2, color='k')
    plt.plot (t[[0, -1]], [tauForc]  * 2, color='g', label=r'$\tau_f$')
    plt.plot (t, tauNL, color='b', label=r'$\tau_{NL}$')
    plt.yscale ('log')
    plt.xlabel (r'$t$')
    plt.ylabel (r'$\tau$')
    plt.title ('$t={:.2f}$'.format(p['time']))
    plt.legend()
    if saveFile is None:
      plt.show ()
    else:
      plt.savefig (saveFile, bbox_inches='tight')
  plt.close()


def plot_cutt (f, it2, dir='.', saveFile=None, vlog=None, vmin=None, vmax=None, compexp=0.):
  '''Plot cut of field at some given time as a function of wavenumber and position
  @param f: field (string): u, b, zp, zm (amplitudes), eu, eb, ep, em (energies), ch (cross helicity) 
  @param it2: time index of out_simstate files output
  @param dir: directory where to read files
  @param vlog: use a logarithmic scale for field values to display
  @param vmin: minimum field value to display
  @param vmax: maximum field value to display
  @param compexp: compensate spectra by k^compexp
'''
  t2i = np.loadtxt(dir + '/out_time2i', dtype='S')[it2]
  t2  = np.loadtxt(dir + '/out_time2')[it2]
  p, g = read_simstate('out_simstate_' + t2i, dir=dir)
  # get field to plot (and plot title)
  if   f == 'u' : h, ft = np.abs (g.u()), 'u'
  elif f == 'b' : h, ft = np.abs (g.b()), 'b'
  elif f == 'zp': h, ft = np.abs (g.zp) , 'z^+'
  elif f == 'zm': h, ft = np.abs (g.zm) , 'z^-'
  elif f == 'eu': h, ft = g.em()        , 'E_u'
  elif f == 'eb': h, ft = g.eb()        , 'E_b'
  elif f == 'ep': h, ft = g.ep()        , 'E^+'
  elif f == 'em': h, ft = g.em()        , 'E^-'
  elif f == 'ch': h, ft = g.ch()        , 'H_c'
  elif f == 'ef': h, ft = g.ef()        , 'E^+ - E^-'
  else: raise NameError ('Unknown field requested: ' + f)
  # axes
  logk = p.logkperp()
  z2 = np.loadtxt(dir + '/out_rad')
  z = z2[(len (z2) // p['nz']) * np.arange(p['nz'])]
  # compensate spectra by some power-law
  h *= 10. ** (compexp * logk[:, np.newaxis])
  # default vlog and color scale depend on field; make color scale symmetric for linear non-positive scale
  cmap = cm.gnuplot2
  if (f in ['ch', 'ef']):
    if (vlog is None): vlog = False
    if (vlog == False):
      cmap = cm.seismic
      if (vmin is None and vmax is None):
        vminmax = np.nanmax (np.abs (h))
      else:
        if   (vmin is None): vmin = 0.
        elif (vmax is None): vmax = 0.
        vminmax = max (np.abs (vmin), np.abs (vmax))
      vmin, vmax = -vminmax, vminmax
  else:
    if (vlog is None): vlog = True
  # plot image
  h = h.transpose()
  if (vlog):
    im = np.log10 (h)
    if vmin is not None: vmin = np.log10 (vmin)
    if vmax is not None: vmax = np.log10 (vmax)
    # replace -Inf by very small value (bug of imshow: displays color at maximum of color scale for -Inf)
    im = np.maximum (im, -300)
  else:
    im = h
  plt.imshow(im, aspect='auto', origin='lower', interpolation='none', \
    extent=[logk[0], logk[-1], z[0], z[-1]], cmap=cmap, vmin=vmin, vmax=vmax)
  plt.xlabel('$\log k_\perp$')
  plt.ylabel('$z$')
  if (vlog):
    if (f == 'ef'): ft = '(' + ft + ')'
    ft = '\log ' + ft
  plt.title('$' + ft + '$, $t=' + str(t2) + '$')
  cb = plt.colorbar()
  if saveFile is not None:
    plt.savefig(saveFile, bbox_inches='tight')
  else:
    plt.show()
  plt.close()



def plot_cutk (f, ik, dir='.', saveFile=None, vlog=None, vmin=None, vmax=None):
  '''Plot cut of field at some given wavenumber, as a function of position and time
  @param f: field (string): u, b, zp, zm (amplitudes), eu, eb, ep, em (energies), ch (cross helicity) 
  @param ik: wavenumber index
  @param dir: directory where to read files
  @param vlog: use a logarithmic scale for field values to display
  @param vmin: minimum field value to display
  @param vmax: maximum field value to display
'''
  allt2i = np.loadtxt(dir + '/out_time2i', dtype='S')
  allt2  = np.loadtxt(dir + '/out_time2')
  nt2 = len (allt2)
  
  p, g = read_simstate('out_simstate_' + allt2i[0], dir=dir)
  # axes
  logk = p.logkperp()
  z2 = np.loadtxt(dir + '/out_rad')
  z = z2[(len (z2) // p['nz']) * np.arange(p['nz'])]
  
  h = np.empty ((p['nz'], len (allt2)))
  for t2i, t2, i in zip (allt2i, allt2, range (nt2)):
    p, g = read_simstate('out_simstate_' + t2i, dir=dir)
    # get field to plot (and plot title)
    if   f == 'u' : hh, ft = np.abs (g.u()), 'u'
    elif f == 'b' : hh, ft = np.abs (g.b()), 'b'
    elif f == 'zp': hh, ft = np.abs (g.zp) , 'z^+'
    elif f == 'zm': hh, ft = np.abs (g.zm) , 'z^-'
    elif f == 'eu': hh, ft = g.em()        , 'E_u'
    elif f == 'eb': hh, ft = g.eb()        , 'E_b'
    elif f == 'ep': hh, ft = g.ep()        , 'E^+'
    elif f == 'em': hh, ft = g.em()        , 'E^-'
    elif f == 'ch': hh, ft = g.ch()        , 'H_c'
    elif f == 'ef': hh, ft = g.ef()        , 'E^+ - E^-'
    else: raise NameError ('Unknown field requested: ' + f)
    h[:, i] = hh[ik, :]

  # default vlog and color scale depend on field; make color scale symmetric for linear non-positive scale
  cmap = cm.gnuplot2
  if (f in ['ch', 'ef']):
    if (vlog is None): vlog = False
    if (vlog == False):
      cmap = cm.seismic
      if (vmin is None and vmax is None):
        vminmax = np.nanmax (np.abs (h))
      else:
        if   (vmin is None): vmin = 0.
        elif (vmax is None): vmax = 0.
        vminmax = max (np.abs (vmin), np.abs (vmax))
      vmin, vmax = -vminmax, vminmax
  else:
    if (vlog is None): vlog = True

  # plot image
  if (vlog):
    im = np.log10 (h)
    if vmin is not None: vmin = np.log10 (vmin)
    if vmax is not None: vmax = np.log10 (vmax)
    # replace -Inf by very small value (bug of imshow: displays color at maximum of color scale for -Inf)
    im = np.maximum (im, -300)
  else:
    im = h
  plt.imshow(im, aspect='auto', origin='lower', interpolation='none', \
    extent=[allt2[0], allt2[-1], z[0], z[-1]], cmap=cmap, vmin=vmin, vmax=vmax)
  plt.xlabel('$t$')
  plt.ylabel('$z$')
  if (vlog):
    if (f == 'ef'): ft = '(' + ft + ')'
    ft = '\log ' + ft
  plt.title('$' + ft + '$, $k=' + str(10. ** logk[ik]) + '$')
  cb = plt.colorbar()
  if saveFile is not None:
    plt.savefig(saveFile, bbox_inches='tight')
  else:
    plt.show()
  plt.close()


def plot_spec2d (f, it2, dir='.', saveFile=None, hann=False):
  '''Plot 2D spectrum at some given time
  @param f: field (string): u, b, zp, zm (amplitudes), eu, eb, ep, em (energies), ch (cross helicity) 
  @param it2: time index of out_simstate files output (or last time if None)
  @param dir: directory where to read files
  @param hann: use Hann ("Hanning") window on position axis

  Have to assume uniform position grid.
  '''
  t2i = np.loadtxt(dir + '/out_time2i', dtype='S')[it2]
  t2  = np.loadtxt(dir + '/out_time2')[it2]
  p, g = read_simstate('out_simstate_' + t2i, dir=dir)
  # select field
  if   f == 'u' : h, ft = g.u(), '$u$'
  elif f == 'b' : h, ft = g.b(), '$b$'
  elif f == 'zp': h, ft = g.zp , '$z^+$'
  elif f == 'zm': h, ft = g.zm , '$z^-$'
  # axes
  logk = p.logkperp()
  z = p['dz'] * np.arange(p['nz']) # uniform grid
  # Hann window on z axis (for each index on k axis)
  if (hann): h = h * np.hanning (p['nz'])
  # Fourier spectrum and frequencies
  tt = np.fft.fft (h, axis=1)
  tt = np.abs (tt) ** 2
  zf = np.fft.fftfreq (p['nz'], d=p['dz'])
  # sum spectrum for positive and negative frequencies
  pass  # TODO


def field_corr (f, dx, maxshift=None, plot=False, window=False):
  '''@return 1. Auto-correlation function of a field along second (t or z) axis, 2. shift array corresponding to auto-correlation array, 3. correlation length or time (half width at half maximum of auto-correlation function)
    @param f: field (array of axes (kperp, z or t))
    @param dx: position or time increment for field array
    @param maxshift: number of points to consider for auto-correlation shifts
    @param plot: plot the computed auto-correlation function
    @param window: apply Hann windowing function
  '''
  nk, nx = f.shape
  # shifts/lags
  if maxshift is None: maxshift = nx // 2
  sh = np.arange (0, maxshift)
  
  if window: fw = f * np.hanning (nx)
  else: fw = f

  #auto-correlations for these shifts: FFT method, faster
  Ff = np.fft.fft (fw - np.mean (fw, axis=1, keepdims=True), axis=1, n=2*nx)
  corr = np.real (np.fft.ifft (Ff * np.conjugate (Ff), axis=1)) [:, :maxshift]
  corr /= np.var (fw, axis=1, keepdims=True) * nx

  sh = sh * dx

  if plot:
    plt.plot (sh, corr.transpose())
    plt.xlabel (r'$\Delta z$')
    plt.ylabel ('Correlation')
    plt.show()

  # computed correlation lengths or times
  l = np.zeros (nk)
  for ik in range (nk):
    w = np.where (corr[ik,:] <= .5)[0]
    if len (w) == 0: l[ik] = np.nan
    else:
      i0 = w[0] - 1
      i1 = w[0] + 1
      if i0 < 0: raise RuntimeError ('Index w[0] expected to be >0') 
      l[ik] = np.interp (.5, corr[ik,i0:i1][::-1], sh[i0:i1][::-1])
      #print ik, corr[ik,i0:i1], sh[i0:i1], l[ik]
      if plot: plt.plot ([l[ik]], .5, 'o', color='k')

  return sh, corr, l


def get_corr_length (f, file='simstate.dat', it2=None, dir='.', param=None, field=None, plot=False):
  '''Get correlation length for a given time (or simulation state file), as a function of perpendicular wavenumber
  @param f: field ('u', 'b', 'zp' or 'zm')
  @param file: simulation state file for parameters and fields
  @param it2: index of output time (supersedes 'file' parameter)
  @param dir: directory where to read files
  @param param: simulation parameters
  @param field: field data (if param and field are not None, they are used instead of f, file, it2, dir: the simulation state has not to be read here)
  @param plot: plot the computed auto-correlation function
  @return: log wavenumbers, correlation lengths
  '''

  fts = {'u': 'u', 'b': 'b', 'zp': 'z^+', 'zm': 'z^-'}
  if param is None or field is None:
    if it2 is not None:
      t2i = np.loadtxt(dir + '/out_time2i', dtype='S')[it2]
      file = 'out_simstate_' + t2i
    p, g = read_simstate (file, dir=dir)
    # get field to use
    if   f == 'u' : h = g.u()
    elif f == 'b' : h = g.b()
    elif f == 'zp': h = g.zp
    elif f == 'zm': h = g.zm
    else: raise NameError ('Unknown field requested: ' + f)
  else:
    p, h = param, field
  if f is not None: ft = fts[f]
  else: ft = ''

  logk = p.logkperp()

  sh, corr, l = field_corr (h, p['dz'], plot=False)
  
  if plot:
    plt.plot (logk, l)
    plt.yscale ('log')
    plt.xlabel (r'$\log k_\perp$')
    plt.ylabel (r'Correlation length for $' + ft + '$')
    plt.title ('$t={:.2f}$'.format(p['time']))
    plt.show()

  return logk, l


def get_corr_time (f, dt, dir='.', param=None, it=None, pc=None, plot=False):
  '''Get correlation time as a function of perpendicular wavenumber
  @param f: field ('u', 'b', 'zp' or 'zm')
  @param dt: time interval for field output
  @param dir: directory where to read files
  @param param: simulation parameters (if param is not None, it is used instead of f, dir: the final simulation state has not to be read here)
  @param it: select time indices window [it[0], it[1][
  @param pc: percentiles of correlation time distribution (if None: output mean correlation time)
  @param plot: plot the computed auto-correlation functions
  @return: log wavenumbers, correlation times or percentiles of correlations times in output planes (dimensions (nk) or (nk, npc))
  '''
  if param is None: p, g = read_simstate ('simstate.dat', dir=dir)
  else: p = param

  logk = p.logkperp()
  alll = []

  # read out_plane_* files and get correlation times for each
  for file in sorted (os.listdir (dir)):
    if file[:10] != 'out_plane_': continue
    print file
    g = read_out_plane (file, p, dir=dir, it=it)
    
    # get field to use
    if   f == 'u' : h, ft = g.u(), 'u'
    elif f == 'b' : h, ft = g.b(), 'b'
    elif f == 'zp': h, ft = g.zp , 'z^+'
    elif f == 'zm': h, ft = g.zm , 'z^-'
    else: raise NameError ('Unknown field requested: ' + f)

    sh, corr, l = field_corr (h, dt, window=True)
    # accumulate values for l as a function of k in different planes
    alll += [l]
  
  # statistics on all correlation times as a function of k in different planes
  alll = np.array (alll)
  if pc is None: # return average
    return logk, np.mean (alll, axis=0)
  else:  # return percentiles
    return logk, np.percentile (alll, pc, axis=0).transpose()


# To have interactive motion in data cube, use something like
#import numpy as np
#from matplotlib import pyplot as plt
#from time import sleep
#plt.ion()
#nb_images = 10
#tableau = np.random.normal(10, 10, (nb_images, 10, 10))
#image = plt.imshow(tableau[0, :, :])

#for k in np.arange(nb_images):  # use keyboard input
    #print "image numero: %i"%i
    #image.set_data(tableau[k, :, :])
    #plt.draw()
    #sleep(0.1)
